-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 22, 2019 at 10:12 PM
-- Server version: 5.6.43-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_leadersapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `message` varchar(100) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `readed` tinyint(1) NOT NULL,
  `extra` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `subTitle` varchar(500) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `description` text,
  `points` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `name`, `subTitle`, `avatar`, `description`, `points`) VALUES
(1, 'Dr. Arun G Nair', 'Senior Dietitian @ KIMS', '61698qMiHNL__SL1000_.jpg', 'Arun is an experienced dietitian working in KIMS, Kerala for 8 years.', NULL),
(2, 'test name', 'new title', NULL, 'dfsdfdsfdsfsf', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_icon` varchar(60) DEFAULT NULL,
  `category` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `constituency_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_icon`, `category`, `description`, `constituency_id`) VALUES
(1, '0xe8f0', 'General', NULL, NULL),
(5, '0xe535', 'Transport', '', 0),
(6, '0xeb4c', 'Agriculture', NULL, NULL),
(7, '0xe0af', 'Business', NULL, NULL),
(9, '0xe548', 'Health', NULL, NULL),
(10, '0xe16e', 'Politics', '', 0),
(11, '0xe8b0', 'Schemes', 'Details about various Government schemes.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_icons`
--

CREATE TABLE `category_icons` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `icon_data` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_icons`
--

INSERT INTO `category_icons` (`id`, `name`, `icon_data`) VALUES
(2, '360', '0xe577'),
(3, '3d_rotation', '0xe84d'),
(4, '4k', '0xe072'),
(5, 'ac_unit', '0xeb3b'),
(6, 'access_alarm', '0xe190'),
(7, 'access_alarms', '0xe191'),
(8, 'access_time', '0xe192'),
(9, 'accessibility', '0xe84e'),
(10, 'accessibility_new', '0xe92c'),
(11, 'accessible', '0xe914'),
(12, 'accessible_forward', '0xe934'),
(13, 'account_balance', '0xe84f'),
(14, 'account_balance_wallet', '0xe850'),
(15, 'account_box', '0xe851'),
(16, 'account_circle', '0xe853'),
(17, 'adb', '0xe60e'),
(18, 'add', '0xe145'),
(19, 'add_a_photo', '0xe439'),
(20, 'add_alarm', '0xe193'),
(21, 'add_alert', '0xe003'),
(22, 'add_box', '0xe146'),
(23, 'add_call', '0xe0e8'),
(24, 'add_circle', '0xe147'),
(25, 'add_circle_outline', '0xe148'),
(26, 'add_comment', '0xe266'),
(27, 'add_location', '0xe567'),
(28, 'add_photo_alternate', '0xe43e'),
(29, 'add_shopping_cart', '0xe854'),
(30, 'add_to_home_screen', '0xe1fe'),
(31, 'add_to_photos', '0xe39d'),
(32, 'add_to_queue', '0xe05c'),
(33, 'adjust', '0xe39e'),
(34, 'airline_seat_flat', '0xe630'),
(35, 'airline_seat_flat_angled', '0xe631'),
(36, 'airline_seat_individual_suite', '0xe632'),
(37, 'airline_seat_legroom_extra', '0xe633'),
(38, 'airline_seat_legroom_normal', '0xe634'),
(39, 'airline_seat_legroom_reduced', '0xe635'),
(40, 'airline_seat_recline_extra', '0xe636'),
(41, 'airline_seat_recline_normal', '0xe637'),
(42, 'airplanemode_active', '0xe195'),
(43, 'airplanemode_inactive', '0xe194'),
(44, 'airplay', '0xe055'),
(45, 'airport_shuttle', '0xeb3c'),
(46, 'alarm', '0xe855'),
(47, 'alarm_add', '0xe856'),
(48, 'alarm_off', '0xe857'),
(49, 'alarm_on', '0xe858'),
(50, 'album', '0xe019'),
(51, 'all_inclusive', '0xeb3d'),
(52, 'all_out', '0xe90b'),
(53, 'alternate_email', '0xe0e6'),
(54, 'android', '0xe859'),
(55, 'announcement', '0xe85a'),
(56, 'apps', '0xe5c3'),
(57, 'archive', '0xe149'),
(58, 'arrow_back', '0xe5c4'),
(59, 'arrow_back_ios', '0xe5e0'),
(60, 'arrow_downward', '0xe5db'),
(61, 'arrow_drop_down', '0xe5c5'),
(62, 'arrow_drop_down_circle', '0xe5c6'),
(63, 'arrow_drop_up', '0xe5c7'),
(64, 'arrow_forward', '0xe5c8'),
(65, 'arrow_forward_ios', '0xe5e1'),
(66, 'arrow_left', '0xe5de'),
(67, 'arrow_right', '0xe5df'),
(68, 'arrow_upward', '0xe5d8'),
(69, 'art_track', '0xe060'),
(70, 'aspect_ratio', '0xe85b'),
(71, 'assessment', '0xe85c'),
(72, 'assignment', '0xe85d'),
(73, 'assignment_ind', '0xe85e'),
(74, 'assignment_late', '0xe85f'),
(75, 'assignment_return', '0xe860'),
(76, 'assignment_returned', '0xe861'),
(77, 'assignment_turned_in', '0xe862'),
(78, 'assistant', '0xe39f'),
(79, 'assistant_photo', '0xe3a0'),
(80, 'atm', '0xe573'),
(81, 'attach_file', '0xe226'),
(82, 'attach_money', '0xe227'),
(83, 'attachment', '0xe2bc'),
(84, 'audiotrack', '0xe3a1'),
(85, 'autorenew', '0xe863'),
(86, 'av_timer', '0xe01b'),
(87, 'backspace', '0xe14a'),
(88, 'backup', '0xe864'),
(89, 'battery_alert', '0xe19c'),
(90, 'battery_charging_full', '0xe1a3'),
(91, 'battery_full', '0xe1a4'),
(92, 'battery_std', '0xe1a5'),
(93, 'battery_unknown', '0xe1a6'),
(94, 'beach_access', '0xeb3e'),
(95, 'beenhere', '0xe52d'),
(96, 'block', '0xe14b'),
(97, 'bluetooth', '0xe1a7'),
(98, 'bluetooth_audio', '0xe60f'),
(99, 'bluetooth_connected', '0xe1a8'),
(100, 'bluetooth_disabled', '0xe1a9'),
(101, 'bluetooth_searching', '0xe1aa'),
(102, 'blur_circular', '0xe3a2'),
(103, 'blur_linear', '0xe3a3'),
(104, 'blur_off', '0xe3a4'),
(105, 'blur_on', '0xe3a5'),
(106, 'book', '0xe865'),
(107, 'bookmark', '0xe866'),
(108, 'bookmark_border', '0xe867'),
(109, 'border_all', '0xe228'),
(110, 'border_bottom', '0xe229'),
(111, 'border_clear', '0xe22a'),
(112, 'border_color', '0xe22b'),
(113, 'border_horizontal', '0xe22c'),
(114, 'border_inner', '0xe22d'),
(115, 'border_left', '0xe22e'),
(116, 'border_outer', '0xe22f'),
(117, 'border_right', '0xe230'),
(118, 'border_style', '0xe231'),
(119, 'border_top', '0xe232'),
(120, 'border_vertical', '0xe233'),
(121, 'branding_watermark', '0xe06b'),
(122, 'brightness_1', '0xe3a6'),
(123, 'brightness_2', '0xe3a7'),
(124, 'brightness_3', '0xe3a8'),
(125, 'brightness_4', '0xe3a9'),
(126, 'brightness_5', '0xe3aa'),
(127, 'brightness_6', '0xe3ab'),
(128, 'brightness_7', '0xe3ac'),
(129, 'brightness_auto', '0xe1ab'),
(130, 'brightness_high', '0xe1ac'),
(131, 'brightness_low', '0xe1ad'),
(132, 'brightness_medium', '0xe1ae'),
(133, 'broken_image', '0xe3ad'),
(134, 'brush', '0xe3ae'),
(135, 'bubble_chart', '0xe6dd'),
(136, 'bug_report', '0xe868'),
(137, 'build', '0xe869'),
(138, 'burst_mode', '0xe43c'),
(139, 'business', '0xe0af'),
(140, 'business_center', '0xeb3f'),
(141, 'cached', '0xe86a'),
(142, 'cake', '0xe7e9'),
(143, 'calendar_today', '0xe935'),
(144, 'calendar_view_day', '0xe936'),
(145, 'call', '0xe0b0'),
(146, 'call_end', '0xe0b1'),
(147, 'call_made', '0xe0b2'),
(148, 'call_merge', '0xe0b3'),
(149, 'call_missed', '0xe0b4'),
(150, 'call_missed_outgoing', '0xe0e4'),
(151, 'call_received', '0xe0b5'),
(152, 'call_split', '0xe0b6'),
(153, 'call_to_action', '0xe06c'),
(154, 'camera', '0xe3af'),
(155, 'camera_alt', '0xe3b0'),
(156, 'camera_enhance', '0xe8fc'),
(157, 'camera_front', '0xe3b1'),
(158, 'camera_rear', '0xe3b2'),
(159, 'camera_roll', '0xe3b3'),
(160, 'cancel', '0xe5c9'),
(161, 'card_giftcard', '0xe8f6'),
(162, 'card_membership', '0xe8f7'),
(163, 'card_travel', '0xe8f8'),
(164, 'casino', '0xeb40'),
(165, 'cast', '0xe307'),
(166, 'cast_connected', '0xe308'),
(167, 'category', '0xe574'),
(168, 'center_focus_strong', '0xe3b4'),
(169, 'center_focus_weak', '0xe3b5'),
(170, 'change_history', '0xe86b'),
(171, 'chat', '0xe0b7'),
(172, 'chat_bubble', '0xe0ca'),
(173, 'chat_bubble_outline', '0xe0cb'),
(174, 'check', '0xe5ca'),
(175, 'check_box', '0xe834'),
(176, 'check_box_outline_blank', '0xe835'),
(177, 'check_circle', '0xe86c'),
(178, 'check_circle_outline', '0xe92d'),
(179, 'chevron_left', '0xe5cb'),
(180, 'chevron_right', '0xe5cc'),
(181, 'child_care', '0xeb41'),
(182, 'child_friendly', '0xeb42'),
(183, 'chrome_reader_mode', '0xe86d'),
(184, 'class', '0xe86e'),
(185, 'clear', '0xe14c'),
(186, 'clear_all', '0xe0b8'),
(187, 'close', '0xe5cd'),
(188, 'closed_caption', '0xe01c'),
(189, 'cloud', '0xe2bd'),
(190, 'cloud_circle', '0xe2be'),
(191, 'cloud_done', '0xe2bf'),
(192, 'cloud_download', '0xe2c0'),
(193, 'cloud_off', '0xe2c1'),
(194, 'cloud_queue', '0xe2c2'),
(195, 'cloud_upload', '0xe2c3'),
(196, 'code', '0xe86f'),
(197, 'collections', '0xe3b6'),
(198, 'collections_bookmark', '0xe431'),
(199, 'color_lens', '0xe3b7'),
(200, 'colorize', '0xe3b8'),
(201, 'comment', '0xe0b9'),
(202, 'compare', '0xe3b9'),
(203, 'compare_arrows', '0xe915'),
(204, 'computer', '0xe30a'),
(205, 'confirmation_number', '0xe638'),
(206, 'contact_mail', '0xe0d0'),
(207, 'contact_phone', '0xe0cf'),
(208, 'contacts', '0xe0ba'),
(209, 'content_copy', '0xe14d'),
(210, 'content_cut', '0xe14e'),
(211, 'content_paste', '0xe14f'),
(212, 'control_point', '0xe3ba'),
(213, 'control_point_duplicate', '0xe3bb'),
(214, 'copyright', '0xe90c'),
(215, 'create', '0xe150'),
(216, 'create_new_folder', '0xe2cc'),
(217, 'credit_card', '0xe870'),
(218, 'crop', '0xe3be'),
(219, 'crop_16_9', '0xe3bc'),
(220, 'crop_3_2', '0xe3bd'),
(221, 'crop_5_4', '0xe3bf'),
(222, 'crop_7_5', '0xe3c0'),
(223, 'crop_din', '0xe3c1'),
(224, 'crop_free', '0xe3c2'),
(225, 'crop_landscape', '0xe3c3'),
(226, 'crop_original', '0xe3c4'),
(227, 'crop_portrait', '0xe3c5'),
(228, 'crop_rotate', '0xe437'),
(229, 'crop_square', '0xe3c6'),
(230, 'dashboard', '0xe871'),
(231, 'data_usage', '0xe1af'),
(232, 'date_range', '0xe916'),
(233, 'dehaze', '0xe3c7'),
(234, 'delete', '0xe872'),
(235, 'delete_forever', '0xe92b'),
(236, 'delete_outline', '0xe92e'),
(237, 'delete_sweep', '0xe16c'),
(238, 'departure_board', '0xe576'),
(239, 'description', '0xe873'),
(240, 'desktop_mac', '0xe30b'),
(241, 'desktop_windows', '0xe30c'),
(242, 'details', '0xe3c8'),
(243, 'developer_board', '0xe30d'),
(244, 'developer_mode', '0xe1b0'),
(245, 'device_hub', '0xe335'),
(246, 'device_unknown', '0xe339'),
(247, 'devices', '0xe1b1'),
(248, 'devices_other', '0xe337'),
(249, 'dialer_sip', '0xe0bb'),
(250, 'dialpad', '0xe0bc'),
(251, 'directions', '0xe52e'),
(252, 'directions_bike', '0xe52f'),
(253, 'directions_boat', '0xe532'),
(254, 'directions_bus', '0xe530'),
(255, 'directions_car', '0xe531'),
(256, 'directions_railway', '0xe534'),
(257, 'directions_run', '0xe566'),
(258, 'directions_subway', '0xe533'),
(259, 'directions_transit', '0xe535'),
(260, 'directions_walk', '0xe536'),
(261, 'disc_full', '0xe610'),
(262, 'dns', '0xe875'),
(263, 'do_not_disturb', '0xe612'),
(264, 'do_not_disturb_alt', '0xe611'),
(265, 'do_not_disturb_off', '0xe643'),
(266, 'do_not_disturb_on', '0xe644'),
(267, 'dock', '0xe30e'),
(268, 'domain', '0xe7ee'),
(269, 'done', '0xe876'),
(270, 'done_all', '0xe877'),
(271, 'done_outline', '0xe92f'),
(272, 'donut_large', '0xe917'),
(273, 'donut_small', '0xe918'),
(274, 'drafts', '0xe151'),
(275, 'drag_handle', '0xe25d'),
(276, 'drive_eta', '0xe613'),
(277, 'dvr', '0xe1b2'),
(278, 'edit', '0xe3c9'),
(279, 'edit_attributes', '0xe578'),
(280, 'edit_location', '0xe568'),
(281, 'eject', '0xe8fb'),
(282, 'email', '0xe0be'),
(283, 'enhanced_encryption', '0xe63f'),
(284, 'equalizer', '0xe01d'),
(285, 'error', '0xe000'),
(286, 'error_outline', '0xe001'),
(287, 'euro_symbol', '0xe926'),
(288, 'ev_station', '0xe56d'),
(289, 'event', '0xe878'),
(290, 'event_available', '0xe614'),
(291, 'event_busy', '0xe615'),
(292, 'event_note', '0xe616'),
(293, 'event_seat', '0xe903'),
(294, 'exit_to_app', '0xe879'),
(295, 'expand_less', '0xe5ce'),
(296, 'expand_more', '0xe5cf'),
(297, 'explicit', '0xe01e'),
(298, 'explore', '0xe87a'),
(299, 'exposure', '0xe3ca'),
(300, 'exposure_neg_1', '0xe3cb'),
(301, 'exposure_neg_2', '0xe3cc'),
(302, 'exposure_plus_1', '0xe3cd'),
(303, 'exposure_plus_2', '0xe3ce'),
(304, 'exposure_zero', '0xe3cf'),
(305, 'extension', '0xe87b'),
(306, 'face', '0xe87c'),
(307, 'fast_forward', '0xe01f'),
(308, 'fast_rewind', '0xe020'),
(309, 'fastfood', '0xe57a'),
(310, 'favorite', '0xe87d'),
(311, 'favorite_border', '0xe87e'),
(312, 'featured_play_list', '0xe06d'),
(313, 'featured_video', '0xe06e'),
(314, 'feedback', '0xe87f'),
(315, 'fiber_dvr', '0xe05d'),
(316, 'fiber_manual_record', '0xe061'),
(317, 'fiber_new', '0xe05e'),
(318, 'fiber_pin', '0xe06a'),
(319, 'fiber_smart_record', '0xe062'),
(320, 'file_download', '0xe2c4'),
(321, 'file_upload', '0xe2c6'),
(322, 'filter', '0xe3d3'),
(323, 'filter_1', '0xe3d0'),
(324, 'filter_2', '0xe3d1'),
(325, 'filter_3', '0xe3d2'),
(326, 'filter_4', '0xe3d4'),
(327, 'filter_5', '0xe3d5'),
(328, 'filter_6', '0xe3d6'),
(329, 'filter_7', '0xe3d7'),
(330, 'filter_8', '0xe3d8'),
(331, 'filter_9', '0xe3d9'),
(332, 'filter_9_plus', '0xe3da'),
(333, 'filter_b_and_w', '0xe3db'),
(334, 'filter_center_focus', '0xe3dc'),
(335, 'filter_drama', '0xe3dd'),
(336, 'filter_frames', '0xe3de'),
(337, 'filter_hdr', '0xe3df'),
(338, 'filter_list', '0xe152'),
(339, 'filter_none', '0xe3e0'),
(340, 'filter_tilt_shift', '0xe3e2'),
(341, 'filter_vintage', '0xe3e3'),
(342, 'find_in_page', '0xe880'),
(343, 'find_replace', '0xe881'),
(344, 'fingerprint', '0xe90d'),
(345, 'first_page', '0xe5dc'),
(346, 'fitness_center', '0xeb43'),
(347, 'flag', '0xe153'),
(348, 'flare', '0xe3e4'),
(349, 'flash_auto', '0xe3e5'),
(350, 'flash_off', '0xe3e6'),
(351, 'flash_on', '0xe3e7'),
(352, 'flight', '0xe539'),
(353, 'flight_land', '0xe904'),
(354, 'flight_takeoff', '0xe905'),
(355, 'flip', '0xe3e8'),
(356, 'flip_to_back', '0xe882'),
(357, 'flip_to_front', '0xe883'),
(358, 'folder', '0xe2c7'),
(359, 'folder_open', '0xe2c8'),
(360, 'folder_shared', '0xe2c9'),
(361, 'folder_special', '0xe617'),
(362, 'font_download', '0xe167'),
(363, 'format_align_center', '0xe234'),
(364, 'format_align_justify', '0xe235'),
(365, 'format_align_left', '0xe236'),
(366, 'format_align_right', '0xe237'),
(367, 'format_bold', '0xe238'),
(368, 'format_clear', '0xe239'),
(369, 'format_color_fill', '0xe23a'),
(370, 'format_color_reset', '0xe23b'),
(371, 'format_color_text', '0xe23c'),
(372, 'format_indent_decrease', '0xe23d'),
(373, 'format_indent_increase', '0xe23e'),
(374, 'format_italic', '0xe23f'),
(375, 'format_line_spacing', '0xe240'),
(376, 'format_list_bulleted', '0xe241'),
(377, 'format_list_numbered', '0xe242'),
(378, 'format_list_numbered_rtl', '0xe267'),
(379, 'format_paint', '0xe243'),
(380, 'format_quote', '0xe244'),
(381, 'format_shapes', '0xe25e'),
(382, 'format_size', '0xe245'),
(383, 'format_strikethrough', '0xe246'),
(384, 'format_textdirection_l_to_r', '0xe247'),
(385, 'format_textdirection_r_to_l', '0xe248'),
(386, 'format_underlined', '0xe249'),
(387, 'forum', '0xe0bf'),
(388, 'forward', '0xe154'),
(389, 'forward_10', '0xe056'),
(390, 'forward_30', '0xe057'),
(391, 'forward_5', '0xe058'),
(392, 'free_breakfast', '0xeb44'),
(393, 'fullscreen', '0xe5d0'),
(394, 'fullscreen_exit', '0xe5d1'),
(395, 'functions', '0xe24a'),
(396, 'g_translate', '0xe927'),
(397, 'gamepad', '0xe30f'),
(398, 'games', '0xe021'),
(399, 'gavel', '0xe90e'),
(400, 'gesture', '0xe155'),
(401, 'get_app', '0xe884'),
(402, 'gif', '0xe908'),
(403, 'golf_course', '0xeb45'),
(404, 'gps_fixed', '0xe1b3'),
(405, 'gps_not_fixed', '0xe1b4'),
(406, 'gps_off', '0xe1b5'),
(407, 'grade', '0xe885'),
(408, 'gradient', '0xe3e9'),
(409, 'grain', '0xe3ea'),
(410, 'graphic_eq', '0xe1b8'),
(411, 'grid_off', '0xe3eb'),
(412, 'grid_on', '0xe3ec'),
(413, 'group', '0xe7ef'),
(414, 'group_add', '0xe7f0'),
(415, 'group_work', '0xe886'),
(416, 'hd', '0xe052'),
(417, 'hdr_off', '0xe3ed'),
(418, 'hdr_on', '0xe3ee'),
(419, 'hdr_strong', '0xe3f1'),
(420, 'hdr_weak', '0xe3f2'),
(421, 'headset', '0xe310'),
(422, 'headset_mic', '0xe311'),
(423, 'headset_off', '0xe33a'),
(424, 'healing', '0xe3f3'),
(425, 'hearing', '0xe023'),
(426, 'help', '0xe887'),
(427, 'help_outline', '0xe8fd'),
(428, 'high_quality', '0xe024'),
(429, 'highlight', '0xe25f'),
(430, 'highlight_off', '0xe888'),
(431, 'history', '0xe889'),
(432, 'home', '0xe88a'),
(433, 'hot_tub', '0xeb46'),
(434, 'hotel', '0xe53a'),
(435, 'hourglass_empty', '0xe88b'),
(436, 'hourglass_full', '0xe88c'),
(437, 'http', '0xe902'),
(438, 'https', '0xe88d'),
(439, 'image', '0xe3f4'),
(440, 'image_aspect_ratio', '0xe3f5'),
(441, 'import_contacts', '0xe0e0'),
(442, 'import_export', '0xe0c3'),
(443, 'important_devices', '0xe912'),
(444, 'inbox', '0xe156'),
(445, 'indeterminate_check_box', '0xe909'),
(446, 'info', '0xe88e'),
(447, 'info_outline', '0xe88f'),
(448, 'input', '0xe890'),
(449, 'insert_chart', '0xe24b'),
(450, 'insert_comment', '0xe24c'),
(451, 'insert_drive_file', '0xe24d'),
(452, 'insert_emoticon', '0xe24e'),
(453, 'insert_invitation', '0xe24f'),
(454, 'insert_link', '0xe250'),
(455, 'insert_photo', '0xe251'),
(456, 'invert_colors', '0xe891'),
(457, 'invert_colors_off', '0xe0c4'),
(458, 'iso', '0xe3f6'),
(459, 'keyboard', '0xe312'),
(460, 'keyboard_arrow_down', '0xe313'),
(461, 'keyboard_arrow_left', '0xe314'),
(462, 'keyboard_arrow_right', '0xe315'),
(463, 'keyboard_arrow_up', '0xe316'),
(464, 'keyboard_backspace', '0xe317'),
(465, 'keyboard_capslock', '0xe318'),
(466, 'keyboard_hide', '0xe31a'),
(467, 'keyboard_return', '0xe31b'),
(468, 'keyboard_tab', '0xe31c'),
(469, 'keyboard_voice', '0xe31d'),
(470, 'kitchen', '0xeb47'),
(471, 'label', '0xe892'),
(472, 'label_important', '0xe937'),
(473, 'label_outline', '0xe893'),
(474, 'landscape', '0xe3f7'),
(475, 'language', '0xe894'),
(476, 'laptop', '0xe31e'),
(477, 'laptop_chromebook', '0xe31f'),
(478, 'laptop_mac', '0xe320'),
(479, 'laptop_windows', '0xe321'),
(480, 'last_page', '0xe5dd'),
(481, 'launch', '0xe895'),
(482, 'layers', '0xe53b'),
(483, 'layers_clear', '0xe53c'),
(484, 'leak_add', '0xe3f8'),
(485, 'leak_remove', '0xe3f9'),
(486, 'lens', '0xe3fa'),
(487, 'library_add', '0xe02e'),
(488, 'library_books', '0xe02f'),
(489, 'library_music', '0xe030'),
(490, 'lightbulb_outline', '0xe90f'),
(491, 'line_style', '0xe919'),
(492, 'line_weight', '0xe91a'),
(493, 'linear_scale', '0xe260'),
(494, 'link', '0xe157'),
(495, 'link_off', '0xe16f'),
(496, 'linked_camera', '0xe438'),
(497, 'list', '0xe896'),
(498, 'live_help', '0xe0c6'),
(499, 'live_tv', '0xe639'),
(500, 'local_activity', '0xe53f'),
(501, 'local_airport', '0xe53d'),
(502, 'local_atm', '0xe53e'),
(503, 'local_bar', '0xe540'),
(504, 'local_cafe', '0xe541'),
(505, 'local_car_wash', '0xe542'),
(506, 'local_convenience_store', '0xe543'),
(507, 'local_dining', '0xe556'),
(508, 'local_drink', '0xe544'),
(509, 'local_florist', '0xe545'),
(510, 'local_gas_station', '0xe546'),
(511, 'local_grocery_store', '0xe547'),
(512, 'local_hospital', '0xe548'),
(513, 'local_hotel', '0xe549'),
(514, 'local_laundry_service', '0xe54a'),
(515, 'local_library', '0xe54b'),
(516, 'local_mall', '0xe54c'),
(517, 'local_movies', '0xe54d'),
(518, 'local_offer', '0xe54e'),
(519, 'local_parking', '0xe54f'),
(520, 'local_pharmacy', '0xe550'),
(521, 'local_phone', '0xe551'),
(522, 'local_pizza', '0xe552'),
(523, 'local_play', '0xe553'),
(524, 'local_post_office', '0xe554'),
(525, 'local_printshop', '0xe555'),
(526, 'local_see', '0xe557'),
(527, 'local_shipping', '0xe558'),
(528, 'local_taxi', '0xe559'),
(529, 'location_city', '0xe7f1'),
(530, 'location_disabled', '0xe1b6'),
(531, 'location_off', '0xe0c7'),
(532, 'location_on', '0xe0c8'),
(533, 'location_searching', '0xe1b7'),
(534, 'lock', '0xe897'),
(535, 'lock_open', '0xe898'),
(536, 'lock_outline', '0xe899'),
(537, 'looks', '0xe3fc'),
(538, 'looks_3', '0xe3fb'),
(539, 'looks_4', '0xe3fd'),
(540, 'looks_5', '0xe3fe'),
(541, 'looks_6', '0xe3ff'),
(542, 'looks_one', '0xe400'),
(543, 'looks_two', '0xe401'),
(544, 'loop', '0xe028'),
(545, 'loupe', '0xe402'),
(546, 'low_priority', '0xe16d'),
(547, 'loyalty', '0xe89a'),
(548, 'mail', '0xe158'),
(549, 'mail_outline', '0xe0e1'),
(550, 'map', '0xe55b'),
(551, 'markunread', '0xe159'),
(552, 'markunread_mailbox', '0xe89b'),
(553, 'maximize', '0xe930'),
(554, 'memory', '0xe322'),
(555, 'menu', '0xe5d2'),
(556, 'merge_type', '0xe252'),
(557, 'message', '0xe0c9'),
(558, 'mic', '0xe029'),
(559, 'mic_none', '0xe02a'),
(560, 'mic_off', '0xe02b'),
(561, 'minimize', '0xe931'),
(562, 'missed_video_call', '0xe073'),
(563, 'mms', '0xe618'),
(564, 'mobile_screen_share', '0xe0e7'),
(565, 'mode_comment', '0xe253'),
(566, 'mode_edit', '0xe254'),
(567, 'monetization_on', '0xe263'),
(568, 'money_off', '0xe25c'),
(569, 'monochrome_photos', '0xe403'),
(570, 'mood', '0xe7f2'),
(571, 'mood_bad', '0xe7f3'),
(572, 'more', '0xe619'),
(573, 'more_horiz', '0xe5d3'),
(574, 'more_vert', '0xe5d4'),
(575, 'motorcycle', '0xe91b'),
(576, 'mouse', '0xe323'),
(577, 'move_to_inbox', '0xe168'),
(578, 'movie', '0xe02c'),
(579, 'movie_creation', '0xe404'),
(580, 'movie_filter', '0xe43a'),
(581, 'multiline_chart', '0xe6df'),
(582, 'music_note', '0xe405'),
(583, 'music_video', '0xe063'),
(584, 'my_location', '0xe55c'),
(585, 'nature', '0xe406'),
(586, 'nature_people', '0xe407'),
(587, 'navigate_before', '0xe408'),
(588, 'navigate_next', '0xe409'),
(589, 'navigation', '0xe55d'),
(590, 'near_me', '0xe569'),
(591, 'network_cell', '0xe1b9'),
(592, 'network_check', '0xe640'),
(593, 'network_locked', '0xe61a'),
(594, 'network_wifi', '0xe1ba'),
(595, 'new_releases', '0xe031'),
(596, 'next_week', '0xe16a'),
(597, 'nfc', '0xe1bb'),
(598, 'no_encryption', '0xe641'),
(599, 'no_sim', '0xe0cc'),
(600, 'not_interested', '0xe033'),
(601, 'not_listed_location', '0xe575'),
(602, 'note', '0xe06f'),
(603, 'note_add', '0xe89c'),
(604, 'notification_important', '0xe004'),
(605, 'notifications', '0xe7f4'),
(606, 'notifications_active', '0xe7f7'),
(607, 'notifications_none', '0xe7f5'),
(608, 'notifications_off', '0xe7f6'),
(609, 'notifications_paused', '0xe7f8'),
(610, 'offline_bolt', '0xe932'),
(611, 'offline_pin', '0xe90a'),
(612, 'ondemand_video', '0xe63a'),
(613, 'opacity', '0xe91c'),
(614, 'open_in_browser', '0xe89d'),
(615, 'open_in_new', '0xe89e'),
(616, 'open_with', '0xe89f'),
(617, 'outlined_flag', '0xe16e'),
(618, 'pages', '0xe7f9'),
(619, 'pageview', '0xe8a0'),
(620, 'palette', '0xe40a'),
(621, 'pan_tool', '0xe925'),
(622, 'panorama', '0xe40b'),
(623, 'panorama_fish_eye', '0xe40c'),
(624, 'panorama_horizontal', '0xe40d'),
(625, 'panorama_vertical', '0xe40e'),
(626, 'panorama_wide_angle', '0xe40f'),
(627, 'party_mode', '0xe7fa'),
(628, 'pause', '0xe034'),
(629, 'pause_circle_filled', '0xe035'),
(630, 'pause_circle_outline', '0xe036'),
(631, 'payment', '0xe8a1'),
(632, 'people', '0xe7fb'),
(633, 'people_outline', '0xe7fc'),
(634, 'perm_camera_mic', '0xe8a2'),
(635, 'perm_contact_calendar', '0xe8a3'),
(636, 'perm_data_setting', '0xe8a4'),
(637, 'perm_device_information', '0xe8a5'),
(638, 'perm_identity', '0xe8a6'),
(639, 'perm_media', '0xe8a7'),
(640, 'perm_phone_msg', '0xe8a8'),
(641, 'perm_scan_wifi', '0xe8a9'),
(642, 'person', '0xe7fd'),
(643, 'person_add', '0xe7fe'),
(644, 'person_outline', '0xe7ff'),
(645, 'person_pin', '0xe55a'),
(646, 'person_pin_circle', '0xe56a'),
(647, 'personal_video', '0xe63b'),
(648, 'pets', '0xe91d'),
(649, 'phone', '0xe0cd'),
(650, 'phone_android', '0xe324'),
(651, 'phone_bluetooth_speaker', '0xe61b'),
(652, 'phone_forwarded', '0xe61c'),
(653, 'phone_in_talk', '0xe61d'),
(654, 'phone_iphone', '0xe325'),
(655, 'phone_locked', '0xe61e'),
(656, 'phone_missed', '0xe61f'),
(657, 'phone_paused', '0xe620'),
(658, 'phonelink', '0xe326'),
(659, 'phonelink_erase', '0xe0db'),
(660, 'phonelink_lock', '0xe0dc'),
(661, 'phonelink_off', '0xe327'),
(662, 'phonelink_ring', '0xe0dd'),
(663, 'phonelink_setup', '0xe0de'),
(664, 'photo', '0xe410'),
(665, 'photo_album', '0xe411'),
(666, 'photo_camera', '0xe412'),
(667, 'photo_filter', '0xe43b'),
(668, 'photo_library', '0xe413'),
(669, 'photo_size_select_actual', '0xe432'),
(670, 'photo_size_select_large', '0xe433'),
(671, 'photo_size_select_small', '0xe434'),
(672, 'picture_as_pdf', '0xe415'),
(673, 'picture_in_picture', '0xe8aa'),
(674, 'picture_in_picture_alt', '0xe911'),
(675, 'pie_chart', '0xe6c4'),
(676, 'pie_chart_outlined', '0xe6c5'),
(677, 'pin_drop', '0xe55e'),
(678, 'place', '0xe55f'),
(679, 'play_arrow', '0xe037'),
(680, 'play_circle_filled', '0xe038'),
(681, 'play_circle_outline', '0xe039'),
(682, 'play_for_work', '0xe906'),
(683, 'playlist_add', '0xe03b'),
(684, 'playlist_add_check', '0xe065'),
(685, 'playlist_play', '0xe05f'),
(686, 'plus_one', '0xe800'),
(687, 'poll', '0xe801'),
(688, 'polymer', '0xe8ab'),
(689, 'pool', '0xeb48'),
(690, 'portable_wifi_off', '0xe0ce'),
(691, 'portrait', '0xe416'),
(692, 'power', '0xe63c'),
(693, 'power_input', '0xe336'),
(694, 'power_settings_new', '0xe8ac'),
(695, 'pregnant_woman', '0xe91e'),
(696, 'present_to_all', '0xe0df'),
(697, 'print', '0xe8ad'),
(698, 'priority_high', '0xe645'),
(699, 'public', '0xe80b'),
(700, 'publish', '0xe255'),
(701, 'query_builder', '0xe8ae'),
(702, 'question_answer', '0xe8af'),
(703, 'queue', '0xe03c'),
(704, 'queue_music', '0xe03d'),
(705, 'queue_play_next', '0xe066'),
(706, 'radio', '0xe03e'),
(707, 'radio_button_checked', '0xe837'),
(708, 'radio_button_unchecked', '0xe836'),
(709, 'rate_review', '0xe560'),
(710, 'receipt', '0xe8b0'),
(711, 'recent_actors', '0xe03f'),
(712, 'record_voice_over', '0xe91f'),
(713, 'redeem', '0xe8b1'),
(714, 'redo', '0xe15a'),
(715, 'refresh', '0xe5d5'),
(716, 'remove', '0xe15b'),
(717, 'remove_circle', '0xe15c'),
(718, 'remove_circle_outline', '0xe15d'),
(719, 'remove_from_queue', '0xe067'),
(720, 'remove_red_eye', '0xe417'),
(721, 'remove_shopping_cart', '0xe928'),
(722, 'reorder', '0xe8fe'),
(723, 'repeat', '0xe040'),
(724, 'repeat_one', '0xe041'),
(725, 'replay', '0xe042'),
(726, 'replay_10', '0xe059'),
(727, 'replay_30', '0xe05a'),
(728, 'replay_5', '0xe05b'),
(729, 'reply', '0xe15e'),
(730, 'reply_all', '0xe15f'),
(731, 'report', '0xe160'),
(732, 'report_off', '0xe170'),
(733, 'report_problem', '0xe8b2'),
(734, 'restaurant', '0xe56c'),
(735, 'restaurant_menu', '0xe561'),
(736, 'restore', '0xe8b3'),
(737, 'restore_from_trash', '0xe938'),
(738, 'restore_page', '0xe929'),
(739, 'ring_volume', '0xe0d1'),
(740, 'room', '0xe8b4'),
(741, 'room_service', '0xeb49'),
(742, 'rotate_90_degrees_ccw', '0xe418'),
(743, 'rotate_left', '0xe419'),
(744, 'rotate_right', '0xe41a'),
(745, 'rounded_corner', '0xe920'),
(746, 'router', '0xe328'),
(747, 'rowing', '0xe921'),
(748, 'rss_feed', '0xe0e5'),
(749, 'rv_hookup', '0xe642'),
(750, 'satellite', '0xe562'),
(751, 'save', '0xe161'),
(752, 'save_alt', '0xe171'),
(753, 'scanner', '0xe329'),
(754, 'scatter_plot', '0xe268'),
(755, 'schedule', '0xe8b5'),
(756, 'school', '0xe80c'),
(757, 'score', '0xe269'),
(758, 'screen_lock_landscape', '0xe1be'),
(759, 'screen_lock_portrait', '0xe1bf'),
(760, 'screen_lock_rotation', '0xe1c0'),
(761, 'screen_rotation', '0xe1c1'),
(762, 'screen_share', '0xe0e2'),
(763, 'sd_card', '0xe623'),
(764, 'sd_storage', '0xe1c2'),
(765, 'search', '0xe8b6'),
(766, 'security', '0xe32a'),
(767, 'select_all', '0xe162'),
(768, 'send', '0xe163'),
(769, 'sentiment_dissatisfied', '0xe811'),
(770, 'sentiment_neutral', '0xe812'),
(771, 'sentiment_satisfied', '0xe813'),
(772, 'sentiment_very_dissatisfied', '0xe814'),
(773, 'sentiment_very_satisfied', '0xe815'),
(774, 'settings', '0xe8b8'),
(775, 'settings_applications', '0xe8b9'),
(776, 'settings_backup_restore', '0xe8ba'),
(777, 'settings_bluetooth', '0xe8bb'),
(778, 'settings_brightness', '0xe8bd'),
(779, 'settings_cell', '0xe8bc'),
(780, 'settings_ethernet', '0xe8be'),
(781, 'settings_input_antenna', '0xe8bf'),
(782, 'settings_input_component', '0xe8c0'),
(783, 'settings_input_composite', '0xe8c1'),
(784, 'settings_input_hdmi', '0xe8c2'),
(785, 'settings_input_svideo', '0xe8c3'),
(786, 'settings_overscan', '0xe8c4'),
(787, 'settings_phone', '0xe8c5'),
(788, 'settings_power', '0xe8c6'),
(789, 'settings_remote', '0xe8c7'),
(790, 'settings_system_daydream', '0xe1c3'),
(791, 'settings_voice', '0xe8c8'),
(792, 'share', '0xe80d'),
(793, 'shop', '0xe8c9'),
(794, 'shop_two', '0xe8ca'),
(795, 'shopping_basket', '0xe8cb'),
(796, 'shopping_cart', '0xe8cc'),
(797, 'short_text', '0xe261'),
(798, 'show_chart', '0xe6e1'),
(799, 'shuffle', '0xe043'),
(800, 'shutter_speed', '0xe43d'),
(801, 'signal_cellular_4_bar', '0xe1c8'),
(802, 'signal_cellular_connected_no_internet_4_bar', '0xe1cd'),
(803, 'signal_cellular_no_sim', '0xe1ce'),
(804, 'signal_cellular_null', '0xe1cf'),
(805, 'signal_cellular_off', '0xe1d0'),
(806, 'signal_wifi_4_bar', '0xe1d8'),
(807, 'signal_wifi_4_bar_lock', '0xe1d9'),
(808, 'signal_wifi_off', '0xe1da'),
(809, 'sim_card', '0xe32b'),
(810, 'sim_card_alert', '0xe624'),
(811, 'skip_next', '0xe044'),
(812, 'skip_previous', '0xe045'),
(813, 'slideshow', '0xe41b'),
(814, 'slow_motion_video', '0xe068'),
(815, 'smartphone', '0xe32c'),
(816, 'smoke_free', '0xeb4a'),
(817, 'smoking_rooms', '0xeb4b'),
(818, 'sms', '0xe625'),
(819, 'sms_failed', '0xe626'),
(820, 'snooze', '0xe046'),
(821, 'sort', '0xe164'),
(822, 'sort_by_alpha', '0xe053'),
(823, 'spa', '0xeb4c'),
(824, 'space_bar', '0xe256'),
(825, 'speaker', '0xe32d'),
(826, 'speaker_group', '0xe32e'),
(827, 'speaker_notes', '0xe8cd'),
(828, 'speaker_notes_off', '0xe92a'),
(829, 'speaker_phone', '0xe0d2'),
(830, 'spellcheck', '0xe8ce'),
(831, 'star', '0xe838'),
(832, 'star_border', '0xe83a'),
(833, 'star_half', '0xe839'),
(834, 'stars', '0xe8d0'),
(835, 'stay_current_landscape', '0xe0d3'),
(836, 'stay_current_portrait', '0xe0d4'),
(837, 'stay_primary_landscape', '0xe0d5'),
(838, 'stay_primary_portrait', '0xe0d6'),
(839, 'stop', '0xe047'),
(840, 'stop_screen_share', '0xe0e3'),
(841, 'storage', '0xe1db'),
(842, 'store', '0xe8d1'),
(843, 'store_mall_directory', '0xe563'),
(844, 'straighten', '0xe41c'),
(845, 'streetview', '0xe56e'),
(846, 'strikethrough_s', '0xe257'),
(847, 'style', '0xe41d'),
(848, 'subdirectory_arrow_left', '0xe5d9'),
(849, 'subdirectory_arrow_right', '0xe5da'),
(850, 'subject', '0xe8d2'),
(851, 'subscriptions', '0xe064'),
(852, 'subtitles', '0xe048'),
(853, 'subway', '0xe56f'),
(854, 'supervised_user_circle', '0xe939'),
(855, 'supervisor_account', '0xe8d3'),
(856, 'surround_sound', '0xe049'),
(857, 'swap_calls', '0xe0d7'),
(858, 'swap_horiz', '0xe8d4'),
(859, 'swap_horizontal_circle', '0xe933'),
(860, 'swap_vert', '0xe8d5'),
(861, 'swap_vertical_circle', '0xe8d6'),
(862, 'switch_camera', '0xe41e'),
(863, 'switch_video', '0xe41f'),
(864, 'sync', '0xe627'),
(865, 'sync_disabled', '0xe628'),
(866, 'sync_problem', '0xe629'),
(867, 'system_update', '0xe62a'),
(868, 'system_update_alt', '0xe8d7'),
(869, 'tab', '0xe8d8'),
(870, 'tab_unselected', '0xe8d9'),
(871, 'table_chart', '0xe265'),
(872, 'tablet', '0xe32f'),
(873, 'tablet_android', '0xe330'),
(874, 'tablet_mac', '0xe331'),
(875, 'tag_faces', '0xe420'),
(876, 'tap_and_play', '0xe62b'),
(877, 'terrain', '0xe564'),
(878, 'text_fields', '0xe262'),
(879, 'text_format', '0xe165'),
(880, 'text_rotate_up', '0xe93a'),
(881, 'text_rotate_vertical', '0xe93b'),
(882, 'text_rotation_angledown', '0xe93c'),
(883, 'text_rotation_angleup', '0xe93d'),
(884, 'text_rotation_down', '0xe93e'),
(885, 'text_rotation_none', '0xe93f'),
(886, 'textsms', '0xe0d8'),
(887, 'texture', '0xe421'),
(888, 'theaters', '0xe8da'),
(889, 'thumb_down', '0xe8db'),
(890, 'thumb_up', '0xe8dc'),
(891, 'thumbs_up_down', '0xe8dd'),
(892, 'time_to_leave', '0xe62c'),
(893, 'timelapse', '0xe422'),
(894, 'timeline', '0xe922'),
(895, 'timer', '0xe425'),
(896, 'timer_10', '0xe423'),
(897, 'timer_3', '0xe424'),
(898, 'timer_off', '0xe426'),
(899, 'title', '0xe264'),
(900, 'toc', '0xe8de'),
(901, 'today', '0xe8df'),
(902, 'toll', '0xe8e0'),
(903, 'tonality', '0xe427'),
(904, 'touch_app', '0xe913'),
(905, 'toys', '0xe332'),
(906, 'track_changes', '0xe8e1'),
(907, 'traffic', '0xe565'),
(908, 'train', '0xe570'),
(909, 'tram', '0xe571'),
(910, 'transfer_within_a_station', '0xe572'),
(911, 'transform', '0xe428'),
(912, 'transit_enterexit', '0xe579'),
(913, 'translate', '0xe8e2'),
(914, 'trending_down', '0xe8e3'),
(915, 'trending_flat', '0xe8e4'),
(916, 'trending_up', '0xe8e5'),
(917, 'trip_origin', '0xe57b'),
(918, 'tune', '0xe429'),
(919, 'turned_in', '0xe8e6'),
(920, 'turned_in_not', '0xe8e7'),
(921, 'tv', '0xe333'),
(922, 'unarchive', '0xe169'),
(923, 'undo', '0xe166'),
(924, 'unfold_less', '0xe5d6'),
(925, 'unfold_more', '0xe5d7'),
(926, 'update', '0xe923'),
(927, 'usb', '0xe1e0'),
(928, 'verified_user', '0xe8e8'),
(929, 'vertical_align_bottom', '0xe258'),
(930, 'vertical_align_center', '0xe259'),
(931, 'vertical_align_top', '0xe25a'),
(932, 'vibration', '0xe62d'),
(933, 'video_call', '0xe070'),
(934, 'video_label', '0xe071'),
(935, 'video_library', '0xe04a'),
(936, 'videocam', '0xe04b'),
(937, 'videocam_off', '0xe04c'),
(938, 'videogame_asset', '0xe338'),
(939, 'view_agenda', '0xe8e9'),
(940, 'view_array', '0xe8ea'),
(941, 'view_carousel', '0xe8eb'),
(942, 'view_column', '0xe8ec'),
(943, 'view_comfy', '0xe42a'),
(944, 'view_compact', '0xe42b'),
(945, 'view_day', '0xe8ed'),
(946, 'view_headline', '0xe8ee'),
(947, 'view_list', '0xe8ef'),
(948, 'view_module', '0xe8f0'),
(949, 'view_quilt', '0xe8f1'),
(950, 'view_stream', '0xe8f2'),
(951, 'view_week', '0xe8f3'),
(952, 'vignette', '0xe435'),
(953, 'visibility', '0xe8f4'),
(954, 'visibility_off', '0xe8f5'),
(955, 'voice_chat', '0xe62e'),
(956, 'voicemail', '0xe0d9'),
(957, 'volume_down', '0xe04d'),
(958, 'volume_mute', '0xe04e'),
(959, 'volume_off', '0xe04f'),
(960, 'volume_up', '0xe050'),
(961, 'vpn_key', '0xe0da'),
(962, 'vpn_lock', '0xe62f'),
(963, 'wallpaper', '0xe1bc'),
(964, 'warning', '0xe002'),
(965, 'watch', '0xe334'),
(966, 'watch_later', '0xe924'),
(967, 'wb_auto', '0xe42c'),
(968, 'wb_cloudy', '0xe42d'),
(969, 'wb_incandescent', '0xe42e'),
(970, 'wb_iridescent', '0xe436'),
(971, 'wb_sunny', '0xe430'),
(972, 'wc', '0xe63d'),
(973, 'web', '0xe051'),
(974, 'web_asset', '0xe069'),
(975, 'weekend', '0xe16b'),
(976, 'whatshot', '0xe80e'),
(977, 'widgets', '0xe1bd'),
(978, 'wifi', '0xe63e'),
(979, 'wifi_lock', '0xe1e1'),
(980, 'wifi_tethering', '0xe1e2'),
(981, 'work', '0xe8f9'),
(982, 'wrap_text', '0xe25b'),
(983, 'youtube_searched_for', '0xe8fa'),
(984, 'zoom_in', '0xe8ff'),
(985, 'zoom_out', '0xe900'),
(1, 'zoom_out_map', '0xe56b');

-- --------------------------------------------------------

--
-- Table structure for table `constituency`
--

CREATE TABLE `constituency` (
  `id` int(11) NOT NULL,
  `constiuency` varchar(100) NOT NULL,
  `district_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constituency`
--

INSERT INTO `constituency` (`id`, `constiuency`, `district_id`) VALUES
(1, 'Manjeshwar', 5),
(2, 'Kasaragod', 5),
(3, 'Udma', 5),
(4, 'Kanhangad', 5),
(5, 'Trikaripur', 5),
(6, 'Payyannur', 4),
(7, 'Kalliasseri', 4),
(8, 'Taliparamba', 4),
(9, 'Irikkur', 4),
(10, 'Azhikode', 4),
(11, 'Kannur', 4),
(12, 'Dharmadam', 4),
(13, 'Thalassery', 4),
(14, 'Kuthuparamba', 4),
(15, 'Mattannur', 4),
(16, 'Peravoor', 4),
(17, 'Mananthavady', 14),
(18, 'Sulthanbathery', 14),
(19, 'Kalpetta', 14),
(20, 'Vatakara', 8),
(21, 'Kuttiady', 8),
(22, 'Nadapuram', 8),
(23, 'Koyilandy', 8),
(24, 'Perambra', 8),
(25, 'Balusseri', 8),
(26, 'Elathur', 8),
(27, 'Kozhikode North', 8),
(28, 'Kozhikode South', 8),
(29, 'Beypore', 8),
(30, 'Kunnamangalam', 8),
(31, 'Koduvally', 8),
(32, 'Thiruvambady', 8),
(33, 'Kondotty', 9),
(34, 'Ernad', 9),
(35, 'Nilambur', 9),
(36, 'Wandoor (SC)', 9),
(37, 'Manjeri', 9),
(38, 'Perinthalmanna', 9),
(39, 'Mankada', 9),
(40, 'Malappuram', 9),
(41, 'Vengara', 9),
(42, 'Vallikunnu', 9),
(43, 'Tirurangadi', 9),
(44, 'Tanur', 9),
(45, 'Tirur', 9),
(46, 'Kottakkal', 9),
(47, 'Thavanur', 9),
(48, 'Ponnani', 9),
(49, 'Thrithala', 10),
(50, 'Pattambi', 10),
(51, 'Shornur', 10),
(52, 'Ottappalam', 10),
(53, 'Kongad', 10),
(54, 'Mannarkkad', 10),
(55, 'Malampuzha', 10),
(56, 'Palakkad', 10),
(57, 'Tarur', 10),
(58, 'Chittur', 10),
(59, 'Nemmara', 10),
(60, 'Alathur', 10),
(61, 'Chelakkara', 13),
(62, 'Kunnamkulam', 13),
(63, 'Guruvayoor', 13),
(64, 'Manalur', 13),
(65, 'Wadakkanchery', 13),
(66, 'Ollur', 13),
(67, 'Thrissur', 13),
(68, 'Nattika', 13),
(69, 'Kaipamangalam', 13),
(70, 'Irinjalakuda', 13),
(71, 'Puthukkad', 13),
(72, 'Chalakudy', 13),
(73, 'Kodungallur', 13),
(74, 'Perumbavoor', 2),
(75, 'Angamaly', 2),
(76, 'Aluva', 2),
(77, 'Kalamassery', 2),
(78, 'Paravur', 2),
(79, 'Vypeen', 2),
(80, 'Kochi', 2),
(81, 'Thripunithura', 2),
(82, 'Ernakulam', 2),
(83, 'Thrikkakara', 2),
(84, 'Kunnathunad (SC)', 2),
(85, 'Piravom', 2),
(86, 'Muvattupuzha', 2),
(87, 'Kothamangalam', 2),
(88, 'Devikulam', 3),
(89, 'Udumbanchola', 3),
(90, 'Thodupuzha', 3),
(91, 'Idukki', 3),
(92, 'Peerumade', 3),
(93, 'Pala', 7),
(94, 'Kaduthuruthy', 7),
(95, 'Vaikom', 7),
(96, 'Ettumanoor', 7),
(97, 'Kottayam', 7),
(98, 'Puthuppally', 7),
(99, 'Changanassery', 7),
(100, 'Kanjirappally', 7),
(101, 'Poonjar', 7),
(102, 'Aroor', 1),
(103, 'Cherthala', 1),
(104, 'Alappuzha', 1),
(105, 'Ambalappuzha', 1),
(106, 'Kuttanad', 1),
(107, 'Haripad', 1),
(108, 'Kayamkulam', 1),
(109, 'Mavelikkara', 1),
(110, 'Chengannur', 1),
(111, 'Thiruvalla', 11),
(112, 'Ranni', 11),
(113, 'Aranmula', 11),
(114, 'Konni', 11),
(115, 'Adoor', 11),
(116, 'Karunagapally', 6),
(117, 'Chavara', 6),
(118, 'Kunnathur', 6),
(119, 'Kottarakkara', 6),
(120, 'Pathanapuram', 6),
(121, 'Punalur', 6),
(122, 'Chadayamangalam', 6),
(123, 'Kundara', 6),
(124, 'Kollam', 6),
(125, 'Eravipuram', 6),
(126, 'Chathannoor', 6),
(127, 'Varkala', 12),
(128, 'Attingal', 12),
(129, 'Chirayinkeezhu', 12),
(130, 'Nedumangad', 12),
(131, 'Vamanapuram', 12),
(132, 'Kazhakoottam', 12),
(133, 'Vattiyoorkavu', 12),
(134, 'Thiruvananthapuram', 12),
(135, 'Nemom', 12),
(136, 'Aruvikkara', 12),
(137, 'Parassala', 12),
(138, 'Kattakkada', 12),
(139, 'Kovalam', 12),
(140, 'Neyyattinkara', 12);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `district` varchar(100) NOT NULL,
  `state_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `district`, `state_id`) VALUES
(1, 'Alappuzha', 1),
(2, 'Ernakulam', 1),
(3, 'Idukki', 1),
(4, 'Kannur', 1),
(5, 'Kasaragod', 1),
(6, 'Kollam', 1),
(7, 'Kottayam', 1),
(8, 'Kozhikode', 1),
(9, 'Malappuram', 1),
(10, 'Palakkad', 1),
(11, 'Pathanamthitta', 1),
(12, 'Thiruvananthapuram', 1),
(13, 'Thrissur', 1),
(14, 'Wayanad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `description` text,
  `photos` varchar(500) DEFAULT NULL,
  `anonymous` tinyint(1) NOT NULL,
  `constituency_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `device_location` varchar(50) DEFAULT NULL,
  `active_status` char(1) NOT NULL DEFAULT 'a'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `user_id`, `title`, `description`, `photos`, `anonymous`, `constituency_id`, `type`, `category_id`, `location`, `device_location`, `active_status`) VALUES
(1, 6, 'Test', 'Description', 'IMG_01549284101.jpg', 0, 134, 'complaint', 1, NULL, '8.5603914, 76.8803268', 'a'),
(2, 6, 'Not responsible', 'Mukhyamantri Raji veykkanam.', 'IMG_01550140684.jpg', 1, 133, 'complaint', NULL, NULL, '8.560403, 76.8803167', 'a'),
(3, 17, 'accident ', 'kazhakkuttom bypass accident ', 'IMG_01550214647.jpg,IMG_11550214650.jpg', 0, 133, 'complaint', NULL, NULL, '8.5603919, 76.880325', 'a'),
(4, 6, 'hB', 'haha\n', '', 0, 133, 'feedback', NULL, NULL, '8.5603922, 76.8803492', 'a'),
(5, 6, 'Title', 'Description\n', 'IMG_01553068272.jpg,IMG_11553068272.jpg', 0, 57, 'complaint', NULL, NULL, NULL, 'a'),
(6, 6, 'gbs', 'bshs', '', 0, 133, 'feedback', NULL, NULL, '8.5603893, 76.8803138', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `constituency_id` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `html_content` text CHARACTER SET utf8,
  `points` int(11) NOT NULL DEFAULT '0',
  `party` varchar(20) DEFAULT NULL,
  `front` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `name`, `image`, `constituency_id`, `description`, `html_content`, `points`, `party`, `front`) VALUES
(1, 'P. B. Abdul Razak', 'image_1.jpg', 1, '', NULL, 0, 'IUML', 'UDF'),
(2, 'N. A. Nellikunnu', 'nanellikkunnu.jpg', 2, 'Son of Shri Abdul Khadir and Smt. Nabeesa; born at Nellikkunnu, Kasaragod. Social and Political Worker. Entered politics through Muslim Students\' Federation. Was Secretary, Muslim Students Federation (M.S.F.), Kasaragod Government College Unit, Secretary, Kasaragod Taluk Committee, Muslim Youth League, Kasaragod; Muslim League State Council Member; General Secretary, Muslim League Municipal Committee, Kasaragod; First Correspondent of Chandrika Daily in U.A.E.; Founder Secretary of Chandrika Readers Forum, Dubai. Now, Working Committee Member, I.U.M.L., State Committee; President, Agriculturist Welfare Co-operative Society, Kasaragod; Manager, Anvarul Uloom A.U.P. School, Nellikkunnu, Kasaragod. Previously elected to KLA in 2011.', '', 0, 'IUML', 'UDF'),
(3, 'K. Kunhiraman', '16454_730926.jpg', 3, 'Son of Shri Chandu Maniyani and Smt. Kunhamma Amma, born at Alakkode. S.S.L.C.; Agriculturist. Was Area Secretary, C.P.I.(M) Udma; Executive Director, Kerafed (17 years); President, Panayal State Co-operative Bank (20 years); District Treasurer, Kerala Karshaka Sangham, Kasaragod; President, Bekal Tourism Co-operative Society ; Winner of Best Co-operator in South India. Now District Committee Member, C.P.I.(M), Kasaragod; State Committee Member, Kerala Karshaka Sangham. Previously elected to KLA in 2011. ', '', 0, 'CPI(M)', 'LDF'),
(4, 'E. Chandrasekharan', '16454_730571.jpg', 4, 'Minister for Revenue and Housing Son of Shri P. Kunhiraman Nair and Smt. Edayillyam Parvathi Amma; born at Perumbala. Social Worker. Was Taluk Secretary, A.I.Y.F., Kasaragod (1970); District Secretary A.I.Y.F. (1975) and District Secretariat Member, C.P.I. of undivided Kannur; Member, C.P.I., State Council (1976); State Joint Secretary, A.I.Y.F. (1979); Member, C.P.I. Kasaragod Taluk Committee; Member, Chemmanadu Grama Panchayat (1979-84); Assistant District Secretary, C.P.I, Kasaragod (1984); District Secretary, C.P.I. (1987); State Executive Member, C.P.I. (1998); Member, C.P.I. State Secretariat; Member, Kerala State Rural Development Board (1987-91); Director Board Member, Kerala Agro Machineries Corporation (1991-96); Member, State Land Reforms Review Committee (2008-2010); Advisory Committee Member, BSNL, Kannur S.S.A. (since 2009). Now, Member and Treasurer, C.P.I. State Executive; Leader, C.P.I. Parliamentary Party. Previously elected to KLA in 2011.', '', 0, 'CPI', 'LDF'),
(5, 'M. Rajagopalan', '16446_730478.jpg', 5, 'Son of Shri P. Damodaran and Smt. M. Devaki; born at Kayyoor. Social Worker. Started social activity through Deshabhimani Balasangham. Was District, State office bearer, Balasangham; State Joint Secretary and Central Committee Member of S.F.I.; Kasaragod District President, Secretary and State Joint Secretary of D.Y.F.I.; Union Councillor, Calicut University Union (Twice); General Secretary, Calicut University Union (1986); Senate Member, Calicut University (1984-85); President, KayyoorCheemeni Grama Panchayat (2000-05); Director Board Member, Agro Industries Corporation; Governing Body Member, RAIDCO; Member, DRUCC (Divisional Railway Users Consultative Committee); President, Kayyoor Service Co-operative Bank. Now, Member, C.P.I.(M) Kasaragod District Secretariat; Secretary, C.I.T.U. Kasaragod District Committee; Chairman, Thejaswini Co-operative Hospital, Neelaswaram.', '', 0, 'CPI(M)', 'LDF'),
(6, 'C. Krishnan', '16454_730574.jpg', 6, 'Son of Shri Pavoor Kannan and Smt. Cherootta Chiri; born at Vellur. Social and Political Worker. Entered politics through trade union activities. Was Local Committee Secretary and Area Committee Secretary, C.P.I.(M), Payyannur; Member, Kannur District Committee,C.P.I.(M); President, Vellur Weavers Society and Payyannur Co-operative Rural Bank; Executive Committee Member, Rubco and Beedi Thozhilali Co-operative Society; Member, Kerala Khadi & Village Industries Board, Khadi Workers Welfare Fund Board and Kerala State Housing Board. Now, Member, District Secretariat, C.P.I.(M), Kannur; General Secretary, C.I.T.U. Kannur District Committee; State Committee Member, Working Committee Member and All India General Council Member of C.I.T.U.; State Committee General Secretary, Khadi Workers Federation (C.I.T.U.). Chairman, Committee on the Welfare of Fishermen and Allied Workers. Previously elected to KLA in 2011. ', '', 0, 'CPI(M)', 'LDF'),
(7, 'T. V. Rajesh', 'tvrajesh.jpg', 7, 'Son of Shri V. V. Chandukutty and Smt. T. V. Madhavi; born at Kulappuram. Advocate. Started political activities at a young age itself being the Unit Secretary, Balasangham, Kulappuram; Was Secretary, Cheruthazham Village; Secretary, S.F.I., S.F.I. Cheruthazham Govt. High School unit; President, S.F.I., Madayi Arts & Science College Unit; University Union Councillor, Payyannur College; Senate Member, Calicut University; President and Secretary, S.F.I. Kannur; State Secretary, S.F.I. (2002-05); All India Joint Secretary, S.F.I.; President, D.Y.F.I. Kannur District; State Secretary and President (2007-16) and All India Joint Secretary, D.Y.F.I. C.P.I.(M) State Committee Member since 2012. Chairman, Committee on the Welfare of Youth and Youth Affairs. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(8, 'James Mathew', '16454_730930.jpg', 8, 'Son of Shri N. J. Mathew and Smt. Chinnamma Mathew; born at Kannur. Political Worker. Started political life as an S.F.I. activist during Emergency period and served as Kannur District President, Secretary, State President and All India Joint Secretary of S.F.I.; assaulted by police while participating in students agitations led by S.F.I.; was Area Secretary, C.P.I.(M), Sreekandapuram: Member, C.P.I.(M) District Secretariat and District Committee, Kannur; Member: Calicut University Senate, Kannur University Syndicate and Kannur District Panchayat; Standing Committee Chairman, District Panchayat, Kannur. Now, Chairman, House Committee . Previously elected to KLA in 2011.\r\n', '', 0, 'CPI(M)', 'LDF'),
(9, 'K. C. Joseph', '16454_730906.jpg', 9, 'Son of Shri K. M. Chacko and Smt. Thresiamma Chacko; born at Poovam, Changanassery. Advocate, Social and Political Worker. Was General Secretary, All Kerala Balajana Sakhyam; President, KSU District Committee, Kottayam; Vice-President, KSU State Committee; Secretary, Kerala University Union; General Secretary, NSUI; President, Kerala Pradesh Youth Congress; Executive Committee Member, Kerala Agricultural University; Member, Kerala University Syndicate; Managing Editor, Socialist Youth Weekly; Chairman, Committee of Privileges and Ethics and Committee on Private Members Bills and Resolutions of the Kerala Legislative Assembly; Member, AICC; Secretary, Congress Legislature Party; President, District Congress Committee, Kottayam. Minister for Rural Development, Planning and Culture from 23-5-2011 to 17-12-2011 and Minister for Rural Development, Planning, Culture and NORKA from 18-12-2011 to 20-5-2016. Now, Deputy Leader of Congress Legislature Party. Previously elected to KLA in 1982, 1987, 1991, 1996, 2001, 2006 and 2011.', '', 0, 'INC', 'UDF'),
(10, 'K.M. Shaji', '16446_730486.jpg', 10, 'Son of Shri K.M. Beeran Kutty and Smt. P.C. Aysha Kutty; born at Kaniyambetta on; Businessman. Was School leader, G.H.S. Kaniyambetta; Chairman, Calicut University Union; Vice President and President, Kaniyambetta Grama Panchayat; Secretary and President, State Youth League.', '', 0, 'IUML', 'UDF'),
(11, 'Kadannappally Ramachandran', '79168_730602.jpg', 11, 'Minister for Ports, Museums and Archaeology Son of Shri P.V. Krishnan Gurukkal and Smt. T. K. Parvathi Amma; born; Social and Political Worker. Started political career as I.S.U. Unit President in school life; Was, President, K.S.U. Cannanore Taluk Committee; General Secretary, K.S.U. State Committee; Kannur District Convenor, Youth Congress; State Vice President, K.S.U. (1969-71); Member, Lok Sabha from Kasaragod Constituency (1971 and 1977); Convenor, Congress Members of Parliament from Kerala; Member, Public Accounts Committee of Lok Sabha; Member, Consultative Committee of Ministry of Railways and Communication; General Secretary, Congress(S) State Committee (1989); State President, Congress(S) since 1990; Minister for Devaswom, Printing and Stationery (17-8-2009 to 14-5-2011). Previously elected to KLA in 1980 and 2006.', '', 0, 'INC(S)', 'LDF'),
(12, 'Pinarayi Vijayan', '16446_730493.jpg', 12, 'Son of Shri Maroli Koran and Smt.Alakkatt Kalyani; born at Pinarayi. Entered politics through students movements while studying at Govt. Brennen College, Thalassery. Was President and Secretary of K.S.F. and President of K.S.Y.F.; Joined C.P.I.(M) in 1964; In 1968, at the age of 24, he was elected to the Kannur District Committee of C.P.I.(M); Member, Kannur District Secretariat (1972); Member, C.P.I.(M) State Committee (1978); Secretary, C.P.I.(M) Kannur District (1986); Member, C.P.I.(M) State Secretariat (since 1988); Secretary, State Committee, C.P.I.(M) (1998-2015); Member, Polit Bureau C.P.I.(M) (since 1998). During Emergency, he was arrested and tortured by the police. Minister for Electricity and Co-operation (20-5-1996 to 19-10-1998). Previously elected to KLA in 1970, 1977, 1991 and 1996.', '', 0, 'CPI(M)', 'LDF'),
(13, 'A.N Shamseer', '16446_730279.jpg', 13, 'Son of Shri Usman Komath and Smt. A. N. Sareena; born at Thalassery, Kannur. Was College Union General Secretary, Govt. Brennen College, Thalassery (1995); First Chairman, Kannur University Union (1998); President, S.F.I. Kannur District Committee (2003); State Secretary (2008) and All India Joint Secretary of S.F.I.; D.Y.F.I. Kannur District President (2012). Member, Kannur District Committee, C.P.I. (M) (since 2012); President, Thalasserry Co-operative Hospital (since 2014); State President, D.Y.F.I.; Working Chairman, Ashraya Pain and Palliative Unit, Malabar Cancer Centre.', '', 0, 'CPI(M)', 'LDF'),
(14, 'K. K Shailaja', '16446_730490.jpg', 14, 'Minister for Health & Social Justice Daughter of Shri K. Kundan and Smt. K. K. Shantha; born at Mattannoor, Kannur; Teacher. Entered politics through S.F.I.; Was State Secretary of Democratic Women\'s Association. Now Member, Central Committee, C.P.I.(M); National Joint Secretary, All India Democratic Women\'s Association and Chief Editor, \'Sthree Shabdam\' monthly. Previously elected to KLA in 1996 and 2006.', '', 0, 'Choose...', 'LDF'),
(15, 'E. P. Jayarajan', '16446_730484.jpg', 15, 'Son of Shri B. M. Krishnan Nambiar and Smt. E. P. Parvathi Amma; born at Irinavu, Kannur. Political Worker. Was the first All India President of D.Y.F.I.; General Manager, Deshabhimani; President, Kerala Karshaka Sangham. Served as Minister for Industries and Sports from 25.05.2016 to 14.10.2016. Now, Central Committee Member, C.P.I.(M). Previously elected to KLAin 1991 and 2011. ', '', 0, 'CPI(M)', 'LDF'),
(16, 'Sunny Joseph', '16446_730281.jpg', 16, 'Son of Shri Joseph Vadakkekunnel and Smt. Rosakutty; born at Thodupuzha; Advocate and Political Worker. Started political life as K.S.U. worker; Was Syndicate Member, Calicut University; President, D.C.C., Kannur; President, Service Co-operative Bank, Ulickal; President, Primary Agricultural Development Bank, Thalassery; President, Bar Association, Mattannur. Now, President, Mahatma Gandhi College Committee, Iritty. Previously elected to KLA in 2011. ', '', 0, 'INC', 'UDF'),
(17, 'O.R. Kelu', '16445_731002.jpg', 17, 'Son of Shri.Raman and Smt.Ammu; born at Wayanad.  Agriculturist. Was Member, Tirunelli Grama Panchayat for five years; President, Tirunelli Grama Panchayat for 10 years; Member, Mananthavady Block Panchayat for 2 years. ', '', 0, 'CPI(M)', 'LDF'),
(18, 'I. C. Balakrishnan', '16446_730496.jpg', 18, 'Son of Shri Chandu and Smt. Meenakshy; born at Valad on 25th May 1975; S.S.L.C.; Political Worker. Started political activities as K.S.U. leader Govt. H.S.S, Valad in 1991; Was President, Youth Congress Thavinjal Mandalam (2002); Thavinjal Grama Panchayat Member, Welfare Standing Committee Chairman(2000-05); Member, District Panchayat, Wayanad (2005-10); District President, Indian Youth Congress, Wayanad (2008-10). Now State President, \"Adivasi Congress\". Previously elected to KLA in 2011.', '', 0, 'INC', 'UDF'),
(19, 'C. K. Sasindran', '16454_730900.jpg', 19, 'Son of Shri C. P. Kesavan Nair and Smt. C. K. Janaki Amma; born at Arinchermala, Panamaram in Wayanad district; Agriculturist, Dairy farmer, Social Worker. Was District Secretary, S.F.I. (1979-86); District Secretary and President, D.Y.F.I. (1987-96); Secretary, C.P.I.(M), Wayanad District Committee; Member, K.S.K.T.U. State Committee; Convenor, Adivasi Bhoosamara Sahaya Samithi; Executive Director, Pazhassi Charitable Society. Now Member, C.P.I.(M) State Committee.', '', 0, 'CPI(M)', 'LDF'),
(20, 'C K Nanu', '16445_730954.jpg', 20, 'Son of Shri Kunhappu and Smt. Chirutha; born at Vadakara. Social and Political Worker. Started public life in 1958 as an active worker of K.S.U and Congress Sevadal Volunteer; was District Secretary, Kozhikode and State Committee Member of Youth Congress; after the split in Congress (1969), joined Sanghatana Congress and served as State President and General Secretary of Sanghatana Youth Congress. During Emergency, arrested and detained in jail for 21 days; after the formation of Janatha Party he served as the Member of its State and Kozhikode District Committees; Member Secretary, State General Secretary and Parliamentary Board Chairman; Served as National Committee Member of Janata Party for years. Was Minister for Forest and Transport from February 17-2-2000 to 13-5-2001. Now, Chairman, Committee on the Welfare of Senior Citizens. Previously elected to KLA in 1996, 2001 and 2011.', '', 0, 'JD(S)', 'LDF'),
(21, 'Parakkal Abdullah', '79168_730628.jpg', 21, 'Son of Shri Moithu Haji and Smt. Kunhami; born at Eramala. Businessman. Active in social, cultural, educational and charity activities. \r\n', '', 0, 'IUML', 'UDF'),
(22, 'E. K. Vijayan', '16446_730276.jpg', 22, 'Son of Shri T.V. Balakrishna Kidavu and Smt. E. K. Kamalakshi Amma; born at Muthuvana; Social and Political Worker. Entered politics through A.I.S.F. while a student; Was Kozhikode District Secretary and State Joint Secretary of A.I.S.F.; Branch Secretary, Mandalam Assistant Secretary, Mandalam Secretary, District Assistant Secretary and District Secretary, C.P.I., Kozhikode; Kozhikode District President and Secretary and State Working Committee Member, A.I.T.U.C.; Member, Medical College H.D.C. and R.T.A., Kozhikode; Chairman, K.F.D.C.; Kozhikode Unit Manager, \'Janayugam\'; Senate Member, Calicut University and CUSAT. Now District President, A.I.T.U.C.; Member, C.P.I. State Council. Previously elected to KLA in 2011. ', '', 0, 'CPI', 'LDF'),
(23, 'K. Dasan', '79168_730809.jpg', 23, 'Son of Shri Kunhiraman and Smt. Kalliani; born at Viyyur, Quilandy Taluk. Political Worker. Started political life as Secretary, C.P.I.(M) Quilandy Constituency Committee; was Taluk Secretary, Chethu Thozhilali Union (C.I.T.U.), Handloom Workers Union (C.I.T.U); Member, Panchayat Board, Quilandy (1978); Chairman, BDC (1989) and Quilandy Municipality (1995-2005). Now, Member, C.P.I.(M) District Committee, Kozhikode; All India General Council, C.I.T.U.; State Committee, C.I.T.U.; Vice President, Kozhikode District Committee, C.I.T.U.; President, Matsya Thozhilali Federation (C.I.T.U.), Kozhikode; Secretary, Chethu Thozhilali Union (C.I.T.U.), Quilandy Taluk; Area Secretary, C.I.T.U., Quilandy. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(24, 'T.P. Ramakrishnan', '79168_730623.jpg', 24, 'Minister for Labour and Excise Son of Shri Sankaran and Smt. Manikyam; born at Nambrathkara. Political Worker. Entered politics through student movements; Was Quilandy Taluk Secretary of K.S.F. (1969) and S.F.I. (1970); was K.S.Y.F. Quilandy Taluk Secretary (1971). Became C.P.I.(M) Member in 1968; served as C.P.I.(M) Kizhariyur Local Committee Member; C.P.I.(M) Kadiyangad Local Committee Secretary for 3 years from 1973; C.P.I.(M) Perambra Area Secretary for 9 years from 1981; C.P.I.(M) Balussery Area Secretary for 2 ½ years and Kozhikode District Committee Member. Also served as Secretary, C.P.I.(M) Kozhikode District . Actively engaged in trade union field and agricultural workers organisation from 1970; was leader for the Kizhariyur Meerod Surplus Land agitation (1972) and \'Kudikidappu Samaram\'; severely wounded and hospitalised in a lathi charge in 1969; arrested, assaulted and jailed for 3 months during Emergency in 1976; was Secretary and President of Perambra Estate Labour Union for 18 years from 1972; President, Quilandy Taluk Chethu Thozhilali Union (C.I.T.U.); Joint Secretary, State Chethu Thozhilali Federation (C.I.T.U.); Joint Secretary, State Madya Vyavasaya Thozhilai Federation. C.I.T.U. Quilandy Taluk Secretary and Kozhikode District Secretary; Senate Member, Calicut University; Director, Plantation Corporation, Labour Welfare Fund Board, Kerala Toddy Workers Welfare Fund Board; Chairman, TEXFED. Now serves as State Secretariat Member, C.P.I.(M); State Committee Vice President and All India Working Committee Member, C.I.T.U. Previously elected to KLA in 2001. ', '', 0, 'CPI(M)', 'LDF'),
(25, 'Purushan Kadalundy', '16446_730501.jpg', 25, 'Son of Shri P.K. Kanaran and Smt. Ammalukutty; born at Kadalundi. Playwright, Screenplay Writer and Director. Entered politics through K.S.Y.F.; Became C.P.I.(M) Member in 1970. Was an employee in Mavoor Gwalior Rayons from 1968-2001; Was Joint Secretary, Electrical- Mechanical Association, Govt. Polytechnic, Calicut; Led students strike against price hike of ration commodities during which he was arrested; Was arrested for writing and performing the street play \"Pattini\' during emergency. Vice President, Gwalior Rayons Staff Association; Secretary, Kerala Sahitya Academy; Member, Kerala Sangeetha Nataka Academy; Member, Samskarika Kshemanidhi Board; Programme Committee Member, Thanjavoor South Zone Cultural Centre; Member, Kerala Folklore Academy; Director, Kerala SC/ST Development Corporation; Member, Keluvettan Padana Kendram. Recipient of First E.M.S. Trophy (1972), Sreekantan Nair Smaraka Puraskaram (1978), Pallam Trophy (1980,1981 &1982), National Safety Award (1984), V.K. Krishna Menon Award (1992), Thoppil Bhasi Award (1993), Mahatma Jyothiba Phulae National Award from Kendra Dalit Sahitya Academy, Delhi (2002), Abudhabi Sakthi Award (2005), Swami Anandatheertha Puraskaram (2011), Sree Narayana Guru Puraskaram (2016), Ambedkar Award for best Legislator, Aksharam Puraskaram. Now serves as Chairman, Orchard India-Children\'s Theatre; State Vice President, Purogamana Kala Sahithya Sangham; Senate Member, Calicut University; State Joint Secretary, Pattikajathi Kshema Samithi. Previously elected to KLA in 2011. ', '', 0, 'CPI(M)', 'LDF'),
(26, 'A. K. Saseendran', '16446_730498.jpg', 26, 'Minister for Transport Son of Shri A. Kunhambu and Smt. M.K. Janaki; born at Kannur; Social and Political Worker. Was President, District Committee, Kerala Students Union Kozhikode (1963-66), General Secretary, Kerala Students Union (1967-69); General Secretary (1969-77), Vice President (1969-78) and President (1978-80), Kerala Pradesh Youth Congress Committee; Member, Coffee Board (1978-80); ISCUS (1977-81); Governing Board of Kerala Saksharata Samiti (1987-91 & 1992-97); Kerala State Housing Board (1997-2001); Vice-President, Jawaharlal Nehru Public Library and Research Centre, Kannur; General Secretary, Congress (U) & Indian Congress Socialist (Congress(S)); Member, Advisory Committee, Food Corporation of India; State General Secretary, State Vice President, National Committee Member, Working Committee Member and Parliamentary Party leader of Nationalist Congress Party (N.C.P.); Chairman, Library Advisory Committee, Kerala Legislative Assembly Minister for Transport (from 25-5-2016 to 27-3-2017 and since 1-2-2018) Previously elected to KLA in 1980, 1982, 2006 and 2011. ', '', 0, 'NCP', 'LDF'),
(27, 'A. Pradeepkumar', '79168_730620.jpg', 27, 'Son of Shri Gopalakrishna Kurup and Smt. Kamalakshi; born at Chelakkad, Vadakara Taluk. Social and Political Worker. Entered politics during school days through S.F.I.; was Secretary, S.F.I., Kozhikode Zamorin\'s Guruvayoorappan College Unit (1984-86); Chairman, Calicut University Union; Member, Calicut University Senate (1986-87); Secretary, S.F.I. Kozhikode District Committee (1988-90); State President (1990-92), State Secretary and All India Vice-President, S.F.I. (1992-94); Kozhikode District President and Secretary, State Joint Secretary (2000-03), State Secretary, All India Joint Secretary (2003-07), D.Y.F.I.; Member, Kozhikode District Committee and State Committee, CPI(M); Member, Kozhikode District Council; President, Calicut Co-operative Urban Bank (1998-2003); President, Kozhikode District Football Association; Executive Member, District Sports Council, Kozhikode (2003-07); Member, Kerala State Sports Council (2006-09). Now, Member, C.P.I.(M) State Committee; Vice President, State Football Association; Chairman, Committee of Privileges and Ethics. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(28, 'Dr. M.K. Muneer', '79168_730618.jpg', 28, 'Son of former Chief Minister and former Speaker C.H. Mohammed Koya and Smt. Amina Mohammed Koya; born at Kozhikode. Doctor. Was President, Muslim Youth League Kerala State Committee; Secretary, Kerala State Muslim League; State Secretariat Member, Muslim League; Councillor, Kozhikode Corporation. Served as Minister for Works (26-5-2001 to 12-5-2006), Minister for Panchayats and Social Welfare (23-5-2011 to 20-5-2016) Now, Leader of Muslim League Legislature Party and Deputy Leader of Opposition. Received C. Achutha Menon Award for Literature in 1998 for the book \"Fascisavum Sanghaparivarum\"; selected as Best Singer, Best Actor and Best Cartoonist in Inter Medicos competition. Previously elected to KLA in 1991, 1996, 2001 and 2011.', '', 0, 'IUML', 'UDF'),
(29, 'V.K.C Mammed Koya', '16446_730504.jpg', 29, 'Son of Imbichi Mothi(late)and Fathimakutty(late); born at Nallalam, Kozhikode. Industrialist; Social Worker; Philanthropist. Member, C.P.I.(M) since 1975; Participated in \"Michabhoomi Samaram\", \"Kudikidappu Samaram\" and \"Karshaka Thozhilali Samaram\"; Jailed and kept in custody during the \'Emergency\' period (1975-76); President, Grama Panchayats Cheruvannur Nallalam (1979-84); Director, Service Cooperative Bank, Feroke (25 years); Member, District Council (1990), Kozhikode and Calicut Development Authority (5 years); Chairman, Public Works Standing Committee; Vice President, District Panchayat (1995), Kozhikode; Jilla Panchayat Member, Beypore Division (2000); Member, Kozhikode Development Authority; Director, K.S.I.D.C.; President, Coir Vyavasaya Sahakarana Sangham; State President, KSSIA(2008-12); Vice-Chairman, NORKA Roots; Mayor, Kozhikode Corporation (2015). Now, Chairman, V.K.C. Group; Managing Trustee, \"VKC Charitable Foundation\"; Chairman, Beypore Development Mission Charitable Trust; State Vice President, Kerala Vyapari Vyavasayi Samithi; State President, Kerala State Contractors Federation; President, Footwear Design and Development Centre, Calicut; Director, Kozhikode District Co-operative Homoeo Hospital and Kozhikode District Co-operative Travel and Tourism Development Society; President, Feroke Co-Operative Labour Contract Society; Member, C.P.I.(M) Area Committee, Feroke; Member, Karshaka Sangham District Committee; President, Karshaka Sangham Area Committee, Feroke; Director, Nallalam Co-operative Consumer Store; Chairman, Beypore Mandalam Development Trust, Calicut. Previously elected to KLA in 2001.', '', 0, 'CPI(M)', 'LDF'),
(30, 'P. T. A. Rahim', '16445_730956.jpg', 30, 'Son of Shri Ismailkutty Haji and Smt. Ayisha; born at Koduvally. Advocate. Was President: Koduvally Grama Panchayat (1988-93, 1998-2006), MSF Kozhikode District Committee, Koduvally Service Co-operative Bank, Muslim League(R); Vice President, Koduvally Grama Panchayat (1993-98); Member: Kerala State Hajj Committee (2006-09), Kozhikode District Council, Kozhikode Taluk Land Board and Legal Service Authority; Secretary, IUML Kozhikode District Committee; Director, Kerala State Textile Corporation; Chairman, Kerala State Hajj Committee (2009-12). Now Chairman, C.H. Muhammed Koya Charitable Trust, Thiruvananthapuram; State President, National Secular Conference (NSC); President, Koduvally Muslim Orphanage; Member, Orphanage Control Board; Working President, Vadi Husna-Elettil; Secretary, Ansarul Huda Sangam, Koduvally. Previously elected to KLA in 2006 and 2011. ', '', 0, 'Independent', 'LDF'),
(31, 'Karat Razak', '79168_730614.jpg', 31, 'Son of Shri Ahmed Haji and Smt. Pathumma; born at Koduvally; Businessman Entered political and social activities while a youth; Was Member, Grama Panchayat, Koduvally; Member and President, Block Panchayat, Koduvally; President, Koduvally Housing Co-operative Society. ', '', 0, 'Independent', 'LDF'),
(32, 'George M Thomas', '79168_730611.jpg', 32, 'Son of Shri Thomas and Smt. Annamma; born at Koorachundu. Agriculturist. Was Member, C.P.I.(M) Kozhikode District Secretariat; Kozhikode District Joint Secretary and State Committee Member, Karshaka Sangham; President, E.M.S. Co-operative Hospital, Mukkam. Previously elected to 12th KLA through by-election held on 04.12.2006 consequent on the demise of Shri Mathai Chacko.', '', 0, 'CPI(M)', 'LDF'),
(33, 'T. V. Ibrahim', '16441_730835.jpg', 33, 'Son of Shri T.V.Mohammedaji and Smt. Ithikkutty K.; born at Pookkottur. Teacher. Was State President and General Secretary of M.S.F.; State Secretary, Youth League; President, Block Panchayat, Malappuram (1995-2000); Member, District Panchayat, Malappuram (2000-05); Secretary, Muslim League, Malappuram District. Now Secretary, Athanikkal M.I.C. Orphanage; President, Athanikkal Public Library; Executive Committee Member, State and National Committee, I.U.M.L.; Vice President, P.K.M.I.C. Orphanage, Pookkottur. ', '', 0, 'IUML', 'UDF'),
(34, 'P. K. Basheer', '16441_730828.jpg', 34, 'Son of Shri P. Seethi Haji and Smt. Fathima Hajumma; born at Edavanna. Businessman. Entered politics through M.S.F. (1977); Was President, Edavanna Panchayat Committee (1985-90); Vice President, Malappuram District Committee (1991-95) and State Working Committee Member (1996-2001) of Muslim Youth League; President, Indian Union Muslim League, Wandoor Constituency Committee (1990-95), Eranad Constituency Committee; President of Edavanna Service Co-operative Bank for 13 years; President, Edavanna Grama Panchayat; Member, Malappuram District Panchayat; Director, Malappuram District Co-operative Bank. Now a member of Indian Union Muslim League State working committee (since 2004); General Secretary, Edavanna Yatheem Khana; President, Manjeri PCC Marketing Society; Managing committee member, Jamia Trust; General Secretary, Eranad Muslim Educational Association. Previously elected to KLA in 2011.', '', 0, 'IUML', 'UDF'),
(35, 'P. V. Anwar', '16441_730822.jpg', 35, 'Son of Shri P.V. Shoukathali and Smt. Mariyumma; born at Edavanna. Social worker. Entered politics while a student; Was School Leader, Malabar Christian College School,Kozhikode; College Union General Secretary and Chairman, DGM MES College, Mampad; State Secretary, K.S.U.(S); Vice President of Youth Congress (I), Malappuram District Committee and Democratic Indira Congress, Malappuram District Committee. Very closely involved in many social, charitable and humanitarian activities both in India and abroad; Chairman, Western Ghats Peoples Protection Forum National Committee.', '', 0, 'Independent', 'LDF'),
(36, 'A. P. Anilkumar', '16441_730855.jpg', 36, 'Son of Shri A. P. Balan and Smt. K.C. Devaki; born at Malappuram. Entered politics through K.S.U. and was its Unit President (1979), Taluk President (1982); Secretary, K.S.U. Malappuram District (1984); Chairman, P.T.M. College Union; District Vice President, Youth Congress (1986); State General Secretary, Youth Congress. Minister for Welfare of Scheduled Communities and Youth and Cultural Affairs (5-9-2004 to 12-5-2006); Minister for Welfare of Scheduled Castes, Backward Classes & Tourism (23-5-2011 to 20-5-2016). Previously elected to KLAin 2001, 2006 and 2011.', '', 0, 'INC', 'UDF'),
(37, 'M. Ummer', '16441_730867.jpg', 37, 'Son of Shri M. Moideen Haji and Smt. Ayisha; born at Karuvarakundu . Advocate. Was President, District Panchayat, Malappuram; Chairman, Malappuram District Panchayat Welfare Standing Committee; President, Karuvarakundu Grama Panchayat; Treasurer, Indian Union Muslim League, Malappuram District Committee; Director, K.M.M.L.; Chairman, Subordinate Legislation Committee; Member, I.U.M.L. National Committee; Member, Kerala Wakf Board, Orphanage Control Board; Secretary, Dharul Najath Islamic Centre, Manjeri C.H. Centre. Previously elected to KLA in 2006 and 2011.', '', 0, 'IUML', 'UDF'),
(38, 'Manjalamkuzhi Ali', '16456_730782.jpg', 38, 'Son of Shri Mohamed and Smt. Aysha; born at Panangangara, Malappuram. Businessman. Was Chairman, Mahakavi Moyinkutty Vaidyar Smaraka Trust (1996- 2001); Director, K.S.F.D.C (1996-2001); Director, NORKA-ROOTS (2006-2011); Chairman, Public Undertakings Committee (2011-2012); Director, E.M.S Memorial Hospital, Perinthalmanna; General Secretary, Kerala Pravasi Sangham State Level Committee. Minister for Urban Affairs and Welfare of Minorities from 12-4-2012 to 20-5-2016. Previously elected to KLA in 2001, 2006 and 2011. ', '', 0, 'IUML', 'UDF'),
(39, 'T. A. Ahamed Kabeer', '16445_731003.jpg', 39, 'Son of Shri K. K. Ahmad and Smt. K. M. Fathima; born at Alappuzha. Was General Secretary, District Committee, M.S.F. Ernakulam (1972); Member, Muslim Youth League First State Council (1973); Treasurer (1975), Secretary (1982) and General Secretary (1983) of Muslim Youth League State Committee; President, Muslim Youth League District Committee, Ernakulam (1975); President, Secretary and General Secretary of Muslim League Ernakulam District Committee; Secretary, State Muslim League Committee; President, KSTEO, STU District Committee, Ernakulam; Chairman, United Electrical Limited (UEL), Kollam, SIDCO, Kerala Electrical and Allied Engineering Co. Ltd.(KEL); Director Board Member, Chandrika Daily (Kochi Edition); Editor, Sargadhara Magazine; Member, Ernakulam District Council; Syndicate Member, Mahatma Gandhi University, CUSAT; Member, Kerala Wakf Board; Governing Body Member of S.C.E.R.T. Now Secretary, Parliamentary Party, Indian Union Muslim League; Executive Committee Member, I.U.M.L. All India Committee; Secretariat Member, I.U.M.L. Kerala State Committee; Chairman, Quaide Mullath Foundation, K. M. Seethi Sahib Foundation; Vice President, KMEA; Governing Body Member of Inter University Centre for I.P.R. Studies; Executive Member, Kerala History Association; Member, Samastha Kerala Sahithya Parishath. Previously elected to KLA in 2011.', '', 0, 'IUML', 'UDF'),
(40, 'P. Ubaidulla', '16441_730842.jpg', 40, 'Son of Shri Ahamed Kutty Master and Smt. Zainaba; born at Anakkayam; S.S.L.C. Entered politics through M.S.F. while a student in Manjeri Boys\' High School; Secretary, M.S.F. Eranad Taluk; Member, M.S.F. State Council; General Secretary, Muslim Youth League Malappuram District Committee; President, Muslim League Malappuram Constituency Committee; Member, IUML National Committee President, District Cooperative Bank Employees Union; President, Travancore Titanium Employees Organisation, Spinning Mill Employees Organisation and Cooperative Employees Organisation, Kerala; Member, District Council, Malappuram(1991-95); Member, District Panchayat, Malappuram (1995-2000, 2000-2005); Member, District Planning Committee, Malappuram; Member, Telecom Advisory Committee Kerala Circle; Member, Juvenile Welfare Board. Previously elected to KLA in 2011.', '', 0, 'IUML', 'UDF'),
(41, 'K.N.A. Khader', 'image_41.jpg', 41, 'Son of Shri Alavi Musliar and Smt. Elachola Aysha; born at Vadakkemanna. Advocate; Political and Social Worker. Started political career through Student Movements in 1970; Worked as State President, Secretary and National Executive Committee Member, A.I.S.F.; was Malappuram District Secretary and State Committee Member, C.P.I.; Joined I.U.M.L. in 1987; Malappuram District Council Member and Vice President, Malappuram District Secretary and State Secretariat Member of Muslim League; State President, S.T.U; Member, Regional Transport Authority, National Savings Scheme Advisory Committee, Kerala Wakf Board, KSRTC Advisory Committee, Hajj Committee; Malappuram District President, Kerala State Library Council; Chairman, Mahakavi Moyinkutty Vaidyar Smaraka Committee; Chairman, SERIFED; Chairman, Committee on Public Undertakings (2012-16). Previously elected to KLA in 2001 and 2011.', '', 0, 'IUML', 'UDF'),
(42, 'P. Abdul Hameed', 'Abdul_Hameed_p.jpg', 42, 'Son of Shri Puliyakkuthu Kunjalu Master and Smt. Pathumma; born at Pattikkadu, Malappuram. Rtd. Teacher. Engaged in social and political fields in Valluvanadu in the middle of 1960s; General Secretary, Muslim League and U.D.F. Convener, Perinthalmanna for three decades; President, District Youth Muslim League; District Treasurer, Muslim League; District General Secretary, Muslim League; President, District Co-Operative Bank, Malappuram since 1989; Director, State Co-operative Bank for 30 years; President, Kizhattoor Grama Panchayat; President, Perinthalmanna Block Panchayat; Member, Vengoor M.E.A. Engineering College Administrative Board; Secretary, Pattikad Jamia Nooria Arabic College. ', '', 0, 'IUML', 'UDF'),
(43, 'P. K. Abdu Rabb', '16441_730832.jpg', 43, 'Son of Shri Avukader Kutty Naha and Smt. P. K. Kunhibiriyam Umma; born at Parappanangadi. Social and Political Worker. Was President, Parappanangadi Grama Panchayat (1988-2000); Member, I.U.M.L. National Executive, I.U.M.L. State Executive, Calicut University Senate and Syndicate; President, Parappanangadi Educational Society; President, Tirurangadi Daya Charity Centre, Kondotty Eranad Muslim Educational Association; Member: Governing Body, Tirur S.S.M. Polytechnic College, Tirurangadi Yatheem Khana Committee, Ernakulam K.M.E.A. Committee. Education Minister from 23-5-2011 to 20-5-2016. Previously elected to KLA in 1996, 2001, 2006 and 2011.\r\n\r\nV. ABDURAHIMAN [N.S.C. – TANUR] Son of Shri Muhammed Hamza Vellekkatt and Smt. Khadeeja Nediyal; born at Poroor Agriculturist; Businessman. Entered politics while a student through K.S.U.; Was an active worker of Balajana Sakhyam and Youth Congress; Member, K.P.C.C.; Councillor, Standing Committee Chairman (5 years) and Vice Chairman(5 years), Tirur Municipality. Now President, ‘Act Tirur’– a cultural organisation.', '', 0, 'IUML', 'UDF'),
(44, 'V. Abdurahman', '16441_730826.jpg', 44, '', '', 0, 'Independent', 'LDF'),
(45, 'C. Mammootty', '16445_7309571.jpg', 45, 'Son of Shri C. Ebrahim Haji and Smt. Fathima; born at Taruvana, Vellamunda Panchayat in Wayanad district. Businessman. Entered politics through M.S.F. being Branch Secretary in Kelloor (1971); Was General Secretary and President, M.S.F. Mananthavady Taluk Committee and Wayanad District Committee (1977-81); Union Secretary and Academic Council Member, Calicut University (1981-82); State General Secretary, M.S.F.(1982-85); State President, M.S.F. (1985-88); Member, Kerala Youth Welfare Director Board (1983-87); Member, K.S.R.T.C. Advisory Council and Kerala State Education Advisory Committee; Secretary, I.U.M.L. Wayanad District Committee (1983-86); Editor, M.S.F. Review Magazine (1982-88); Chief Editor, Thoolika Magazine (1988-99); State General Secretary, Muslim Youth League (1988-99); Chairman, Kerala Financial Corporation (1993); Chairman, Hantex (1993-97); Director, All India Handloom (1994-97); Member, Kerala State Orphanage Control Board; General Secretary, I.U.M.L. Wayanad District Committee (2006-11); Convenor, U.D.F. Wayanad District (2008-11); Chairman, Committee on the Welfare of Backward Class Communities in 13th KLA; Muslim League State Working Committee member since 1982. Previously elected to KLA in 2001 and 2011.', '', 0, 'IUML', 'UDF'),
(46, 'Prof. K.K. Abid Hussain Thangal', '16441_730863.jpg', 46, 'Son of late K.K.S. Thangal, former MLA and Smt. Ummachutty K. T.; born at Vadakkangara, Makkaraparambu Retd. Professor, Head of the Department of Sociology, Farook College, Calicut. Was President, M.L. Mankada; Working Committee member, M.L., Malappuram District; State Working Committee member, M.L.; Syndicate Member, Calicut University; Member, Calicut University Academic Council; Member, Sociology Board of Studies, Indian Sociological Society, Higher Secondary Board Curriculum Committee, Higher Education Committee; Kerala University, K.P.S.C. Question Paper Setter; State President, Kerala State College Teachers Federation; Vice President, All Kerala Muslim Employees Association; Patron, K.K.S. Thangal Memorial YatheemKhana; President, Makkaraparambu Grama Panchayat for 11 years, in two terms.\r\n', '', 0, 'IUML', 'UDF'),
(47, 'Dr. K.T. Jaleel', '16441_730839.jpg', 47, 'Minister for Local Self Governments, Welfare of Minorities, Wakf and Hajj Pilgrimage Son of Shri K.T. Kunhumuhammed Haji and Smt. Parayil Nafeesa; born at Tirur on 30th May 1967. Associate Professor, Post Graduate Department of History, P.S.M.O. College, Tirurangadi, Malappuram (now on leave). Was Union Chairman, P.S.M.O. College Tirurangadi; Member, Malappuram District Council from Kuttippuram Division; Chairman, Standing Committee on Education and Health, District Panchayat, Malappuram; General Secretary, Kerala State Muslim Youth League Committee; Syndicate Member, Calicut University; Director, NORKA; Chief Editor, \'Mukhyadhara\' Journal. Previously elected to KLA in 2006 and 2011.', '', 0, 'Independent', 'LDF'),
(48, 'P. Sreeramakrishnan', '79168_730629.jpg', 48, 'Speaker Son of Shri P. Govindan Nair and Smt. P. Seetha Lakshmi; born at Perinthalmanna; Teacher. Was Secretary, Deshabhimani Balasangham, Keezhattoor Panchayat Unit (1980); Unit Secretary, S.F.I., Pattikkadu Government High School (1981); S.F.I. Unit Secretary and Office bearer of N.S.S.College Union, Ottapalam (1983-88); Union Chairman and Senate Member, Calicut University (1988-89); Syndicate Member, Calicut University (1990); District President, S.F.I., Palakkad (1988-91); Block Secretary, Perinthalmanna (from 1991), District Secretary, Malappuram (from 1997), State Joint Secretary, State President (2005), State Secretary and All India President (2007) of D.Y.F.I.; Managing Editor, \'Yuvadhara Masika\'; Vice Chairman, Kerala State Youth Welfare Board (2006-11); Director, E.M.S. Hospital and Research Centre, Perinthalmanna; Co-ordinator, Asia-Pacific World Federation of Democratic Youth; Actively involved in various cultural and literary programmes. Participated in Commonwealth Parliamentary Association Conference held at London in December 2016. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(49, 'V. T. Balram', '16454_730376.jpg', 49, 'Son of Shri K. Sreenarayanan and V.T. Saraswathy (late); born at Othaloor, Thrithala. Advocate. Member, Indian National Congress Party; Former General Secretary, Kerala Pradesh Youth Congress Committee. Previously elected to KLAin 2011.\r\n', '', 0, 'INC', 'UDF'),
(50, 'Mohammed Muhasin', '16441_730818.jpg', 50, 'Son of Shri Aboobacker and Smt. Jameela Beegam; born at Pattambi. Social Worker. Entered politics through A.I.S.F.; Member, A.I.S.F. District Committee, Palakkad and Delhi State Committee; Vice President, A.I.S.F., J.N.U. Unit; Member, A.I.Y.F. Mandalam Committee, Pattambi; Member, Yuva Kala Sahiti. Now, Member, National Council, A.I.S.F.', '', 0, 'CPI', 'LDF'),
(51, 'P. K. Sasi', '16441_730814.jpg', 51, 'Son of M. Prabhakaran Nair and Smt. P. K. Padmini Amma; born at Kulikkiliyad. Became active in politics through S.F.I. while a High School student; was office bearer of S.F.I. Palakkad District Committee; State Secretariat Member, S.F.I. Now, Secretary, C.I.T.U. Palakkad District; State Committee Member, C.I.T.U.; Member, C.P.I.(M) Palakkad District Secretariat.', '', 0, 'CPI(M)', 'LDF'),
(52, 'P. Unni', '16454_730576.jpg', 52, 'Son of Shri P.V. Sankunni Paniker and Smt. P. Narayani Amma; born at Kuttippuram; Social Worker. Joined C.P.I.(M) in 1964; Member, District Committee Kerala State Karshaka Thozhilali Union, Palakkad (till 1975); Secretary, K.S.Y.F., Palakkad District (5 years); District Secretary, D.Y.F.I., Palakkad (1980); State President, Road Transport Workers Union; President, District Motor Thozhilali Union (C.I.T.U.), Palakkad; District Secretary, C.P.I.(M), Palakkad (1998-2012); President and District Secretary, C.I.T.U., Palakkad; Finance Committee Chairman, District Council, Palakkad. Now, C.I.T.U. State Committee Member.', '', 0, 'CPI(M)', 'LDF'),
(53, 'K. V. Vijayadas', '16446_730510.jpg', 53, 'Son of Shri K. Velayudhan and Smt. A. Thatha; born at Elappully; Social worker. Started political life through Kerala Socialist Youth Federation (K.S.Y.F.) in 1975. Entered parliamentary field as a Grama Panchayat Member (1987). Participated in \'Michabhoomi Struggle\' and got imprisoned for 13 days. Was Member, C.P.I.(M) City Branch Committee (1977); Founder President of Thenaari Ksheerolpadaka Sahakarana Sangham (1990); First District Panchayat President of Palakkad (1996); State Committee Member, Karshaka Sangham (1999); Director Board Member, Kerala State Co-operative Bank (2007); District President, Primary Co-operative Societies Association, Palakkad (2008); President, Elappully Service Cooperative Bank (since 2004). Now, Area Secretary, C.P.I.(M), Chittur; Member, C.P.I.(M) District Committee, Palakkad. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(54, 'N. Shamsudheen', '16446_730272.jpg', 54, 'Son of Shri Mohammedkutty and Smt. Mariyakutty; born at Paravanna in Tirur; Advocate. Was School Leader in Govt. High School Paravanna, Calicut; University Union Councillor for four times at college level and student representative of the Calicut University Senate (1991). Entered politics while a student through M.S.F. Was State General Secretary and Malappuram District President of M.S.F.; State General Secretary and Malappuram District President and Secretary of Muslim Youth League; Member, Malappuram District Panchayat (2005); Elected to the Kerala State Wakf Board as representative from Kerala Legislative Assembly. Previously elected to KLA in 2011.', '', 0, 'IUML', 'UDF'),
(55, 'V. S. Achuthanandan', '16446_730514.jpg', 55, 'Chairman, Administrative Reforms Commission Son of Shri Sankaran and Smt. Accamma; born at Punnapra. Social and Political Worker. Entered politics through Trade Union activities; joined State Congress in 1939; became member of the Communist Party in 1940; Organized Coir Factory Workers, Toddy Workers, Agricultural Labourers etc. Was founder of Travancore Karshaka Thozhilali Union, the erstwhile form of Kerala State Karshaka Thozhilali Union. Participated in various people\'s agitations, was one of the leaders in the Punnapra-Vayalar struggle in 1946. Arrested and imprisoned several times and even subjected to harassment of third degree. He had to undergo five and a half years imprisonment and four and a half years underground life. He is the only living leader who, along with 32 members walked out of the National Council meeting of the undivided communist party, and formed C.P.I.(M) in 1964. Was, Secretary, C.P.I.(M) State Committee (1980 - 92); Convener L.D.F. (1996 - 2000); Leader of Opposition (1992 - 1996 , 2001 - 2006 & 2011- 2016); Chief Minister (18-5-2006 to 14-5-2011). Previously elected to KLA in 1967, 1970, 1991, 2001, 2006 and 2011.\r\n', '', 0, 'CPI(M)', 'LDF'),
(56, 'Shafi Parambil', '16441_730810.jpg', 56, 'Son of Shri Shanavas and Smt. Maimoona; born at Valanchery; Social and Political Worker. Started political activities through K.S.U. in Pattambi Government College. Was Office bearer, K.S.U. Unit (2002); General Secretary, Commerce Association (2003) and College Union (2004) in S.N.G.S. College, Pattambi; District General Secretary (2005) and District President (2006), K.S.U. Palakkad; State General Secretary (2007) and State President (2009), K.S.U.; President, Youth Congress Parliament Committee, Palakkad. Now, All India General Secretary, Youth Congress. Previously elected to KLA in 2011.', '', 0, 'INC', 'UDF'),
(57, 'A. K. Balan', '16445_730962.jpg', 57, 'Minister for Welfare of Scheduled Castes, Scheduled Tribes, Backward Classes, Law, Culture and Parliamentary Affairs Son of Shri Kelappan and Smt. Kunhi; born at Tooneri, Nadapuram. Advocate; Social and Political Worker. Entered politics through students\' movement; Was Chairman, Thalassery Brennan College Union (1973-74); Senate Member, Calicut University (1975-76); Secretary and President of S.F.I. State Committee (1979-82); President, Palakkad District Council (1989-91); Chairman, K.S.F.E.(1996- 2001); District President and Secretary, C.I.T.U., Palakkad. Led Kozhikode Thottakkad Excess Land Agitation and was imprisoned for 30 days in Kannur Central Jail; led the struggle for a University Centre at Thalassery; led the struggle to introduce the shift system in colleges and also to provide educational concessions to parallel college students; led the struggle for enhancing educational concessions to students belonging to Scheduled Caste and Scheduled Tribe communities. Elected to Parliament from Ottappalam (1980-85). Was Member, Estimates Committee and Defence Consultation Committee; Minister for Welfare of Scheduled Communities and Electricity from 18-5-2006 to 14-5-2011; Secretary, C.P.I.(M) Parliamentary Party in the 13th KLA. Now, All India Executive Committee Member and State Vice-President, C.I.T.U.; Central Committee Member, C.P.I.(M). Previously elected to KLA in 2001, 2006 and 2011. ', '', 0, 'CPI(M)', 'LDF'),
(58, 'K.Krishnankutty', '16446_730753.jpg', 58, 'Son of Shri Kunjukutty and Smt. Janaki; born at Ezhuthani; Agriculturist. Started political career in the year 1964 through Indian National Congress. Was, Member, Kerala Pradesh Congress Committee; State Treasurer, National Labour Organization; District President, Janatha Party, Palakkad; National Executive Member and State Secretary General, Janatha Dal; President, Perumatty Service Co-operative Bank; Vice- President, Palakkad District Co-operative Bank; Director, State Co-operative Bank; President, Agricultural Processing and Marketing Society; Chairman, State Agriculture Policy Making Committee (2011-16). Now, State President, Janata Dal (Secular) Previously elected to KLA in 1980, 1982, 1991.', '', 0, 'JDS', 'LDF'),
(59, 'K. Babu', '16454_730373.jpg', 59, 'Son of Shri K. Kitta and Smt. Lakshmi; born at Nemmara. Was an active member of Balasangham, S.F.I. and D.Y.F.I.; Was Member, C.I.T.U. District Committee, Palakkad; Member, C.P.I.(M) District Committee, Palakkad; Member, A.I.R.T.W.F. All India Working Committee.', '', 0, 'CPI(M)', 'LDF'),
(60, 'K. D. Prasenan', '16445_730963.jpg', 60, 'Son of Shri K. Devadas and Smt. C. N. Bhanumathi; born at Alathur. Social worker. Started political activities through students union. Was, Calicut University Union Councillor, General Secretary, S.N.College Union, Alathur; Palakkad District Executive Member, Calicut University Union; District Secretary, S.F.I., Palakkad; Palakkad District President and Secretary, D.Y.F.I.; Member: Alathur Grama Panchayat, Palakkad District Panchayat and Kerala State Co-operative Union Managing Committee; Secretary, C.P.I.(M) Area Committee, Alathur. Now, Member, Palakkad District Committee; Director, Alathur Agricultural Development Bank and Alathur Labour Contract Co-operative Society.', '', 0, 'CPI(M)', 'LDF'),
(61, 'U. R. Pradeep', '16446_730266.jpg', 61, 'Son of Shri Raman and Smt. Santha; born at Desamangalam. Social and Political Worker. Became Member: D.Y.F.I. in 1998 and C.P.I.(M) in 1999; President, Desamangalam Grama Panchayat (2000-05); Vice President, Desamangalam Grama Panchayat (2005-10); President, Desamangalam Service Co-operative Bank (2009-14); Secretary, Local Committee, C.P.I.(M), Desamangalam (2010); Area Secretary, (2012) and District Committee Member of Patikajadhi Kshema Samithy (P.K.S.), Thrissur; Member, C.P.I.(M) Area Committee (2012), Chelakkara; Vice President, K.S.K.T.U. Chelakkara Area Committee (2013).', '', 0, 'CPI(M)', 'LDF'),
(62, 'A. C. Moideen', '79168_730770.jpg', 62, 'Minister for Industries, Sports & Youth Affairs Son of Shri Chiyamu and Smt. Fathima Beevi; born at Wadakkanchery. Social Worker. Entered politics through S.F.I.; Was Secretary, K.S.Y.F. Wadakkanchery Area Committee, D.Y.F.I. Wadakkanchery Block Committee and Panangattukara Grameena Vayanasala; President, Thekkumkara Grama Panchayat, Kallampara Milk Marketing Co-operative Society; Secretary, C.P.I.(M) Thekkumkara Local Committee and Wadakkanchery Area Committee; Secretary, C.P.I.(M) District Committee, Thrissur. Now, Member, C.P.I.(M) State Committee; State Working Committee Member, Kerala Karshaka Sangham. Served as Minister for Co-operation and Tourism from 25-5-2016 to 21-11-2016. Previously elected to the 11th KLA in the by-election on 10.05.2004 and to the 12th KLA in 2006. ', '', 0, 'CPI(M)', 'LDF');
INSERT INTO `member` (`id`, `name`, `image`, `constituency_id`, `description`, `html_content`, `points`, `party`, `front`) VALUES
(63, 'K. V. Abdulkhader', '16445_730965.jpg', 63, 'Son of Shri K.V. Abu and Smt. Pathu; born at Blangad. Photographer and Media person. Was State Committee Member and District President, D.Y.F.I. Thrissur; Area Secretary, C.P.I.(M), Chavakkadu; Reporter, Deshabhimani, Guruvayur area for 12 years; Chairman, Kerala State Wakf Board; Member, Central Wakf Council; First Chairman, Committee on the Welfare of Non-Resident Keralites, Kerala Legislative Assembly. Now, Member, C.P.I.(M) District Secretariat, Thrissur; State General Secretary, Kerala Pravasi Sangham; District President, Fishermen Union and Beedi Worker Union (C.I.T.U.) ; Chairman, Committee on the Welfare of Non-Resident Keralites. Previously elected to KLAin 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(64, 'Murali Perunelli', '79168_730756.jpg', 64, 'Son of freedom fighter Shri T. Raghavan Nair and Smt. P. Malathy Amma; born at Oorakam. Social and Political Worker. Served as Vice President, Mullassery Grama Panchayat; President, Annakara Service Co-operative Sangham; Advisory Board Member, Viyyoor Central Jail; Vice President, Thrissur Toddy Workers Union; Thrissur District Committee member, D.Y.F.I.; Secretary, C.P.I.(M) Manalur Area Committee. Now, Member, C.P.I.(M), Thrissur District Secretariat; Member, All India Kisan Council; Thrissur District Committee President and State Joint Secretary, Kerala Karshaka Sangham; Organizing Secretary, Thrissur District Kole Karshaka Sangham; President, Thrissur District Library Council; Member, Kole Development Agency; Chairman, Committee on Subordinate Legislation. Previously elected to KLA in 2006. ', '', 0, 'CPI(M)', 'LDF'),
(65, 'Anil Akkara', '16446_730269.jpg', 65, 'Son of Shri Antony and Smt. Lilly; born at Thrissur. Organic farmer. Was Unit President, K.S.U. (1987); President, Youth Congress Adat Mandalam (1991); Block General Secretary, Youth Congress (1994); Vice President, Adat Grama Panchayat (2000-03); District General Secretary, Youth Congress (2002); President, Adat Grama Panchayat (2003-10), Rajiv Gandhi Rural Development & Research Centre (2007); Chairman, Malayalam Organic Agro Farms & Research Centre (2009); Chairman, District Panchayat Development Standing Committee, Thrissur (2010-13); Member, Kerala Pradesh Congress Committee(2011); President (in-charge), District Panchayat, Thrissur (2013); National General Secretary, A.I.C.C. Rajiv Gandhi Panchayati Raj Sangathan(2014); won numerous State & National awards, in the fields of Panchayathi Raj and Environmental Conservation.', '', 0, 'INC', 'UDF'),
(66, 'K. Rajan', '79168_730762.jpg', 66, 'Son of Shri P. Krishnankutti Menon and Smt. K. Remani; born at Anthikad .Advocate. Was State Secretary and National Vice President of A.I.S.F.; State Secretary, A.I.Y.F. Now National Secretary, A.I.Y.F.; State Executive Member and National Council Member, C.P.I.', '', 0, 'CPI', 'LDF'),
(67, 'V. S. Sunilkumar', '16446_730527.jpg', 67, 'Minister for Agriculture Son of Shri V. S. Subrahmannian and Smt C.K. Premavathi; born at Anthikkad, Thrissur; Advocate. Entered Politics through A.I.S.F. while a high school student; Was President and Secretary, A.I.S.F Thrissur District; State Secretary (1993-97) and National Secretary(1998), A.I.S.F.; Thrissur District Secretary (1999-2003), State Secretary, State President and National Working Committee Member of A.I.Y.F.; Thrissur District Committee Member, State Executive Member and National Council Member of C.P.I.; Chairman, V. K. Mohanan Karshika Samskrithi; District Secretary, C. Achutha Menon Foundation, Thrissur; Chairman, Committee on Government Assurances during 13th KLA. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI', 'LDF'),
(68, 'Geetha Gopi', '79168_730766.jpg', 68, 'Daughter of Shri C.T. Ayyappan and Smt. P.S. Ammukutty; born at Punnayurkulam.Social Worker. Entered politics through C.P.I. in 1995; Was Member and Secretary, Mahila Sangham Guruvayoor Mandalam Committee; Member, C.P.I. Guruvayoor Mandalam Committee, C.P.I. Mandalam Secretariat, Thrissur District Council; State Vice President, Kerala Mahila Sangham; Chairperson, Guruvayoor Municipality (2004 and 2009) and its Health Committee; Deputy Chairperson, Guruvayoor Municipality (2011). Introduced many developmental programmes in the municipality such as office computerisation, touch screen project, distribution of one-day permit, inclusion of \'Clean and Green Guruvayoor Project\' in the Budget and road developmental projects during her tenure as Chairperson. Guruvayoor Municipality became the only Municipality which banned the use of plastic carry bags and received an award for the same from the Government of Kerala. Previously elected to KLA in 2011.', '', 0, 'CPI', 'LDF'),
(69, 'E. T. Tyson', '16446_730260.jpg', 69, 'Son of Shri E. C. Thomas and Smt. Anasthassia Thomas; born at Edavilangu, Thrissur.Headmaster. Was Member, C.P.I. District Committee, Thrissur; Secretary, C.P.I. Kaipamangalam Mandalam; Member, Edavilangu Grama Panchayat, Kodungalloor Block Panchayat, Thrissur Jilla Panchayat. Now, President, Manava Karunyasangam; Chairman, Bahadoor Smrithi Kendram; Patron, Daya Sadhu Jana Samrakshana Samithi. ', '', 0, 'CPI', 'LDF'),
(70, 'K. U. Karunan', '79168_730768.jpg', 70, 'Son of Shri Kumbalaparambil Unniyappan and Smt. Panchali; born at Thrissur. Retired College Principal Was Member, P.S.C.; Executive Member, State Library Council; Member, Calicut University Academic Council; Officer, India Population Project; Member, Edathiruthy Grama Panchayat; Chairman, Board of Examiners, Calicut University. Now, Member, C.P.I.(M) Area Committee; Executive Member, State Committee, Purogamana Kerala Sahithya Sangam; Director, Nattika Firka Rural Bank Ltd; Member, Kerala State Library Council. ', '', 0, 'CPI(M)', 'LDF'),
(71, 'Prof. C. Raveendranath', '79168_730760.jpg', 71, 'Minister for Education Son of Shri Peethambaran Kartha and Smt. Lekshmikutty Kunjamma; born at Cheranallur; Rtd. Teacher. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(72, 'B. D. Devasi', '16446_730263.jpg', 72, 'Son of Shri Devassy and Smt. Mariyam; born at Konoor. Social Worker. Entered public life through library activities and politics through K.S.Y.F.; Member, C.P.I.(M) since 1971; Was C.P.I.(M) Branch Secretary and Local Committee Secretary, Koratty; Chalakkudy Area Secretary; Thrissur District Committee Member (since 1977); Ward Member (1979-84) and President, Grama Panchayat Koratty (1988-2000); Area Secretary, Karshaka Thozhilali Union; Director Board Member, Koratty Service Co-operative Bank for five years from 1987. Now, C.P.I.(M) District Committee Member; District Vice-President and State Committee Member of Kerala State Karshaka Thozhilali Union. Previously elected to KLAin 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(73, 'V. R. Sunilkumar', '16446_730530.jpg', 73, 'Son of Shri V. K. Rajan (Ex. Minister) and Smt. K. K. Sathy; born at Kodungallur; Social and Political Worker. Was Secretary, Kodungalloor Mandalam Committee and Member, State Council, A.I.S.F., A.I.Y.F. and A.I.T.U.C.; Kodungalloor Mandalam Committee Secretary and Thrissur District Committee Member, C.P.I.', '', 0, 'CPI', 'LDF'),
(74, 'Eldhose Kunnapilly', '16445_730968.jpg', 74, 'Son of Shri K. U. Paulose and Smt. Mary Paulose; born at North Marady, Muvattupuzha. Advocate. Started political career while a college student. Served as Magazine Editor (1997-98), College Union Chairman (1998-99) and General Secretary (1999-2000) of Nirmala College, Muvattupuzha; University Union Councillor and Students Council Member, M.G. University, Kottayam (2000-01); General Secretary, K.S.U. District Committee, Ernakulam(2005-06); General Secretary, K.S.U. State Committee (2006-07); President, Ernakulam Youth Congress District Committee (2007-10); President, District Panchayat, Ernakulam(2010-2015); Secretary, Bar Association, Muvattupuzha; Patron, Kerala Blood Donors Forum; Member, Kerala State Khadi Board. Serves as Vice President, D.C.C. Ernakulam since 2015. The Ernakulam District Panchayat won many awards and honours like Nirmal Gram Puraskar (2012-13) and the Best District Panchayat Award (2013) awarded by the Govt. of India, Best District Planning Committee Award, Arogya Keralam Puraskaram (2013) and Best District Panchayat Award (2014-15) awarded by Govt. of Kerala during his presidentship; Became one among the two selected as young opinion leaders from India in the U.S. Government\'s International Visitor Leadership Programme held at Washington D.C.', '', 0, 'INC', 'UDF'),
(75, 'Roji M John', '16456_730802.jpg', 75, 'Son of Shri John M.V. and Smt. Elsamma John; born at Taliparamba, Kannur; Political Worker. Was Chairman, Thevara Sacred Heart College, Ernakulam; Councillor, J.N.U., New Delhi; President, Vice President, N.S.U.; Member, A.I.C.C.', '', 0, 'INC', 'UDF'),
(76, 'Anwar Sadath', '16454_730727.jpg', 76, 'Son of Shri Abdul Sathar and Smt. Aisha Beevi; born at Parambayam. Politician. Was School Unit President, K.S.U.; School Leader; Taluk Secretary and District Executive Committee Member, K.S.U.; Block Secretary, District Secretary and State Secretary of Youth Congress; Member, Chengamanad Grama Panchayat and Ernakulam Jilla Panchayat, Nedumbassery Division. Previously elected to KLA in 2011.', '', 0, 'INC', 'UDF'),
(77, 'V. K. Ibrahim Kunju', '16454_730731.jpg', 77, 'Son of Shri V. U. Khader and Smt. Chithumma; born at Kongorpilly, Aluva. Social Worker. Entered public life through M.S.F. and later became active in Muslim Youth League; Was Secretary/General Secretary (for 25 years), President (for 5 years), Muslim League District Committee, Ernakulam; Chairman, Forest Industries (Travancore) Limited (1993-96); Vice President, Kerala Muslim Educational Association (KMEA); Chairman, C. H. Mohammed Koya College of Engineering and Technology, Edathala; Chaired various organisations and trade unions like Swathanthra Thozhilali Union (S.T.U.) of Eloor Industrial Area, T.E.L.K. Technical Employees Association, Angamaly, Traco Cables Staff and Workers Organisation, Irumpanam, Kerala Minerals and Metals Employees Organisation, Chavara, K.E.L. Employees Union, Mamala, T.C.C. Employees Union, Udyogamandal, Thiruvalla Sugars Union; Director, Cochin International Airport Limited; Syndicate Member, Cochin University of Science and Technology; WHO’S WHO FOURTEENTH KERALA LEGISLATIVE ASSEMBLY 85 Member, Executive Committee, Goshree Islands Development Authority (G.I.D.A.) and Executive Committee, Greater Cochin Development Authority (G.C.D.A.); Secretary, Al Manar Public School; State Vice-President, S.T.U.; President, Officers Forum of T.C.C., Traco Employees Organisation, Thiruvalla; General Secretary, Binani Zinc Workers Federation, G.T.N. Workers Association; Member, District Board of the Kerala Grandhasala Sanghom, Ernakulam District Development Council. Was Minister for Industries and Social Welfare (6-1-2005 to 12-5-2006); Minister for Public Works (23-5-2011 to 20-5-2016). Now member, Kerala State Muslim League Secretariat; Chairman, Committee on Government Assurances. Won many awards like, Best Minister in the State Cabinet 2012 by the Deccan Chronicle, a leading English Daily; Kerala Ratna Puraskaram by the Keraleeyam U.K. Chapter of Global Kerala Initiative; Best Minister title by Keli Kerala in 2013; Icon award by Rotary International; Palakkad Development Award by Palakkad Development Samithy; Global Road Achievement Award by International Road Federation, a United States based organisation in 2015; Minister of Excellence Award by Indo American Press Club. Previously elected to KLA in 2001, 2006 and 2011.', '', 0, 'IUML', 'UDF'),
(78, 'V. D. Satheesan', '16456_730798.jpg', 78, 'Son of Shri K. Damodara Menon and Smt.V.Vilasini Amma; born at Nettoor; Lawyer; Political Worker. Was Chairman, Mahatma Gandhi University Union; Chairman, Committee on Estimates (2011-16), Kerala Legislative Assembly; Secretary, N.S.U.(I) and A.I.C.C.; Whip, Congress Legislature Party. Now serves as Vice President, K.P.C.C.; Chairman, Committee on Public Accounts; Leader of various trade unions. Previously elected to KLA in 2001, 2006 and 2011.', '', 0, 'INC', 'UDF'),
(79, 'S. Sharma', '16445_730969.jpg', 79, 'Son of Shri Sekharan and Smt. C.C. Kavukutty, born at Ezhikkara. Social, Political and Trade Union Worker. Started political life through Students Federation (1972); involved in various agitations led by S.F.I. and D.Y.F.I.; Was District Secretary, D.Y.F.I., Ernakulam (1984); Member, District Committee, C.P.I.(M) (1985); State President(1986), Central Committee Member (1988-95), State Secretary and All India Vice President, D.Y.F.I.; C.P.I.(M) State Secretariat Member; C.P.I.(M) Parliamentary Party Secretary(1996-98); Chairman, Committee on Public Undertakings (1996-98) of Kerala Legislative Assembly; Syndicate Member, CUSAT (1991-95); Founder Chairman, Smart City, Kochi. Minister for Electricity and Co-operation (25.10.1998 to 13.05.2001) and Minister for Fisheries and Registration (18.05.2006 to 14.05.2011). Was Protem Speaker in 2016. Now, Member, State Committee, C.P.I.(M); C.I.T.U. State Vice President; C.P.I.(M) Parliamentary Party Secretary; Chairman, Committee on Estimates since 2016. Previously elected to KLA in 1987, 1991, 1996, 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(80, 'K. J. Maxi', '16445_730970.jpg', 80, 'Son of Shri K. X. Jacob and Smt. Agnes; born at Kochi - is the member of 14th Kerala Legislative Assembly. He represents Kochi constituency and belongs to Communist Party of India (Marxist). Earlier, he served as the counsellor at Kochi municipal corporation. Entered politics through student movements and served as Area Committee Member S.F.I., Kochi. Became active in youth movements; Served as Block Secretary, D.Y.F.I., Kochi; District Vice President, D.Y.F.I., Ernakulam; C.P.I.(M) Kochi Area Secretary; C.I.T.U. District Committee Member, Ernakulam; Health-Education Standing Committee Chairman, Kochi Corporation. ', '', 0, 'Choose...', 'LDF'),
(81, 'M. Swaraj', '16445_730972.jpg', 81, 'Son of Shri P. N. Muraleedharan Nair and Smt. P.R. Sumangi Amma; born at Pathar.; Advocate. Became a member in the S.F.I. in 1988 while studying in 5th Standard; Was Chairman, Calicut University Union (1999); State Secretary, All India Joint Secretary, S.F.I.; Editor, \'Student\', \'Yuvadhara\' Magazines and the online magazine \'The Resistance\'. Now State Secretary and All India Joint Secretary, D.Y.F.I.; Member, C.P.I.(M) State Committee.', '', 0, 'Choose...', 'LDF'),
(82, 'Hibi Eden', '16456_730799.jpg', 82, 'Son of George Eden Ex.MLA and Smt. Rani Eden; born at Thoppumpady, Ernakulam. Social and Political Worker. Was Councillor, Mahatma Gandhi University Students Union (2001-02); General Secretary (2002-03), Chairman (2003-04), S.H.College Students Union; President, K.S.U., S.H. College Unit (2003-04); President, K.S.U. Ernakulam District (2004-07);President, K.S.U. State Committee (March 2007-December 2008); President, National Students Union of India (2008-11); Patron, Inline Roller Skating Club, Kochi; Syndicate Member, CUSAT; President, Kerala State Archery Association, Cochin International Airport Limited Workers Union, Modern Bread Workers Congress, TATA Ceramics Workers Congress, Hindustan Insecticides Limited Workers Congress. Represented India in the Commonwealth Country Seminar held at Samova Island and presented a paper on ‘Parliament and Civil Society’. Represented a ten member Kerala Assembly delegation to China in 2012. Represented INC as a member of Multiparty delegation invited by Singapore High Commission in 2013. Represented India in the Indo-Pak delegation to American Council of Young Political Leaders (ACYPL) held WHO’S WHO FOURTEENTH KERALA LEGISLATIVE ASSEMBLY 105 in US in 2014. Represented India in the European Union Visitors Programme in 2015. Attended the Conference of the Regions conducted as the preparatory session of the World Climate Change Conference in Lyon, France (2015) and presented a paper on \'Transport and mobility\'. Previously elected to KLA in 2011.', '', 0, 'INC', 'UDF'),
(83, 'P. T. THOMAS', '16445_730975.jpg', 83, 'Son of Shri P. T. Thomas and Smt. Annamma; born at Plassanal, Kottayam; Advocate, Social and Political Worker. Was State President, K.S.U.; State General Secretary, Youth Congress; President, Idukki District Congress Committee; Member, A.I.C.C.; Member, Idukki District Council; Member of Parliament from Idukki (2009-14); Member, State Library Council (for 15 years); Member, Executive Committee, State Library Council (for 5 years); State Chairman, Manava Samskrithi; Senate member, Kerala University; Member of various committees of Parliament & Kerala Legislative Assembly; Leader, Indian Team for International Youth Festival, Venice, 1989; Chairman of the Committee named P.T.Thomas Committee, appointed by the Government of Kerala to review the guidelines for financial assistance to art and cultural organisation during 2001-06 and 2011-16. Now,Managing Director and Chief Editor of \"Veekshanam\" daily. Previously elected to KLA in 1991 and 2001.', '', 0, 'INC', 'UDF'),
(84, 'V.P. Sajeendran', '16445_730976.jpg', 84, 'Son of Shri K. S. Padmanabhan and Smt. Janaki; born at Anikkad, Kottayam district.  Businessman. Was K.S.U. President, Govt. Law College, Thiruvananthapuram; Kerala University Union Councillor; Member, Students Council, Kerala University; State Executive Member, K.S.U.; Editor, Kalasala; K.P.C.C. Secretary; Executive Member K.P.C.C; Member, A.I.C.C; Senate Member, K.U.F.O.S.; Syndicate Member, Cochin University; Chairman, Committee on the Welfare of Scheduled Castes and Scheduled Tribes in 13th KLA. Previously elected to KLA in 2011.', '', 0, 'INC', 'UDF'),
(85, 'Anoop Jacob', '16445_730978.jpg', 85, 'Son of late T. M. Jacob, former minister and Smt. Annie Jacob; born at Kottayam. Lawyer; Politician & Social worker. Was Magazine editor of Mar Ivanious College during 1996-97; Unit President, Thiruvananthapuram District President, State General Secretary and State President of Kerala Students Congress (Jacob); State General Secretary and State President of Kerala Youth Front (Jacob); Acted as High Power Committee Member of Kerala Congress (Jacob)in 2004. Minister for Food, Civil Supplies, Consumer Protection and Registration from 12 April 2012 to 20 May 2016. Now, leader (since 2013) and Parliamentary Party leader (since 2016) of Kerala Congress (Jacob); Treasurer, U.D.F. Parliamentary party. Elected to 13th KLA in the by-election held on 17th March 2012 consequent on the demise of T. M. Jacob.', '', 0, 'KC(JACOB)', 'UDF'),
(86, 'Eldho Abraham', '16445_731015.jpg', 86, 'Son of Shri M. P. Abraham and Smt. Aleyamma Abraham; born at Thrikkalathoor. Started political life through A.I.S.F. in 1991; Was, University Union Councillor (1995, 1996), Executive member, M.G. University Union; Member, Students Council, M.G. University; Councillor, I.T.I. Union; District Secretary and State Secretariat Member, A.I.S.F.; President and Secretary, A.I.Y.F. Muvattupuzha; District President and Secretary, State Committee Member, A.I.Y.F.; Member, Payyipra Grama Panchayat (2005, 2010); Chairman, Ward Development Sub Committee. Now Member, C.P.I. District Committee; Secretary, Toddy Workers Federation, Muvattupuzha Mandalam; Director, Thrikkalathur Service Co-operative Bank.', '', 0, 'CPI', 'LDF'),
(87, 'Antony John', '16445_731016.jpg', 87, 'Son of Shri Johny K. M. and Smt. Lilly Johny, born at Kothamangalam. Entered politics through Students\' Organisation; Was Union Chairman, Kothamangalam M.A. College; President, S.F.I. Ernakulam District Committee; Member, State Committee, S.F.I.; Block Secretary, D.Y.F.I. Kothamangalam. Now Member, C.P.I.(M) Area Committee, D.Y.F.I. District Secretariat. ', '', 0, 'CPI(M)', 'LDF'),
(88, 'S. Rajendran', '16446_730257.jpg', 88, 'Son of Shri Shammugavel and Smt. Suppammal; born at Periyavarai near Munnar. Social and Political Worker. Was Area Secretary, S.F.I. and D.Y.F.I., Munnar; Local Secretary, C.P.I.(M), Marayoor; District Committee Member, K.S.K.T.U., C.I.T.U. and C.P.I.(M), Idukki; President, District Panchayat, Idukki. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(89, 'M. M. Mani', '16448_730720.jpg', 89, 'Minister for Electricity since 22-11-2016 Son of Shri Madhavan and Smt. Janaki; born at Kidangoor, Kottayam district . Social and Political Worker. Entered politics at the age of 14. Was Taluk Committee Member, Karshaka Sangham; C.P.I(M) Baison Valley Local Secretary (1970), Rajakkad Local Secretary (1971), Taluk Secretary (1976), Idukki District Secretariat Member (1978), Idukki District Secretary (1985-2015). Elected as Karshaka Sangham State President in June 2016. Member, C.P.I.(M) State Secretariat since 2015. ', '', 0, 'CPI(M)', 'LDF'),
(90, 'P. J. Joseph', '16448_730715.jpg', 90, 'Son of Shri P.O. Joseph and Smt. Annamma; born at Purapuzha.  Agriculturist. Was Minister for Home Affairs (16.01.1978 - 15.09.1978); Minister for Revenue and Education (28.12.1981 - 17.03.1982), Minister for Revenue and Housing (24.05.1982 - 25.03.1987), Minister for Education, Works, Housing and Registration (20.05.1996 - 13.05.2001) and Minister for Public Works (18.05.2006 - 04.09.2006) and (17.08.2009 - 30.04.2010), Minister for Water Resources (23.05.2011 - 20.05.2016). Also served as the Member of the Syndicate of Kerala University (1975); Chairman of Gandhiji Study Centre; Member, Indian Science Congress; One of the prominent Bio and Organic Farmers in Kerala; Pioneered Bio farming in Kerala, Dairy Farming, Conducts \'Karshikamela\' (Agricultural Fair) in Thodupuzha annually, the first of which was in 1989 at Kottayam. Working Chairman, Kerala Congress (M). Previously elected to KLA in 1970, 1977, 1980, 1982, 1987, 1996, 2006 and 2011.\r\n', '', 0, 'KC(M)', 'UDF'),
(91, 'Roshy Augustine', '16448_730712.jpg', 91, 'Son of Shri Augustine and Smt. Leelamma Augustine; born at Pala; Social and Political Worker. Entered politics while a student; Was Leader, Edakkoli Govt. High School Parliament; held different positions in K.S.C. (M) such as College Unit President, St. Thomas College, Palai; State General Secretary and President; Member, Kerala State Legal Aid and Advisory Board; Director Board Member, Ramapuram Service Co-operative Bank; led 43 days long \'Vimochana Padayathra\' from Kasaragod to Thiruvananthapuram in 1995 and \'Vimochana Yatra\' in 2001 to awaken the people against corruption, alcoholism and moral degeneration. Now, General Secretary, Kerala Congress (M); Whip, Kerala Congress(M) Parliamentary Party. Previously elected to KLA in 2001, 2006 and 2011.', '', 0, 'KC(M)', 'UDF'),
(92, 'E. S. Bijimol', '16448_730723.jpg', 92, 'Daughter of Shri E. A. George and Smt. Annamma; born at Upputhara. Began social activities through National Service Scheme and National Cadet Corps and also served in Deepika Children’s League and Nature Club; Was Block Panchayat President, Azutha (1995-2000); Member, Executive Committee, Kerala Agricultural University (1997-2000); Member, District Panchayat, Idukki (2005 - 06). Now, National Executive Member, National Federation of Indian Women; State Secretary, Kerala Plantation Workers’ Federation; State President, Kerala Agricultural University Labour Association; State Vice-President, Kerala Forest Watchers’Association; District President, Scouts & Guides, Idukki; Chairperson, Committee on Official Languages. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI', 'LDF'),
(93, 'K. M. Mani', '16448_730706.jpg', 93, 'Son of Karingozhackal Shri Thomas Mani and Smt. Aleyamma Mani; born at Marangattupilly. Advocate, Politician. Right from his school days, participated in the revolt for the formation of a responsible Government in Travancore. After enrolment as an Advocate, started legal profession at Calicut Bar for a short while and set up practice at Pala and District Court, Kottayam. Entered politics as a worker of the Indian National Congress; was a member of K.P.C.C. and General Secretary of Kottayam D.C.C.; Was founder member and General Secretary of the Kerala Congress Party. Became Chairman of Kerala Congress (M); Was a member of the State Planning Board; Chairman, State Law Reforms Committee. Served as Minister for Finance and Law (1975-77), Home Affairs and Ports (April 1977 to December 1977 and September 1978 to July 1979); Finance and Law (January 1980 to October 1981); Finance, Law and Transport (December 1981 to March 1982); Finance and Law (May 1982 to March 1986); Irrigation and Law and Urban Affairs (June 1986 to March 1987); Revenue and Law (June 1991 to May 1996 and May 2001 to May 2006); Finance, Law and Housing ( May 2011 to November 2015). Holds the record of getting elected from the same constituency in 13 elections continuously, serving as a Member for a total period of 52 Years and holding office of Minister for 25 years. As Finance Minister, has presented 13 Budgets. In 2014-15, he also served as the Chairman of the Council of State Finance Ministers for the implementation of the Goods and Services Tax. Won the election in 1965 and was previously elected to the KLA in 1967, 1970, 1977, 1980, 1982, 1987, 1991, 1996, 2001, 2006 & 2011.', '', 0, 'KC(M)', 'UDF'),
(94, 'Mons Joseph', '16448_730711.jpg', 94, 'Son of Shri O. Joseph and Smt. Mariamma Joseph; born at Appanchira in Kaduthuruthy. Advocate. Entered politics through K.S.C. while a student; was Chairman and General Secretary of Baselius College, Kottayam; President, Kerala Students Congress; President, Kerala Post Graduate Students\' Association State Committee; President, Youth Front State Committee and Kerala Congress Kottayam District Committee; Member, Mahatma Gandhi University Senate, Kerala State Consumer Protection Forum, State Youth Welfare Expert Committee; Associated with Students\' and Youth Movements, led activities for the entire development and protection of rights of Keralites. Led many social welfare programmes for the development of Farmers, Labourers and Youth. Conducted many students\' agitations including eight days fasting struggle in front of the Secretariat for educational reforms. Was Minister for Works from 18-10-2007 to 16-8-2009 and during his tenure, conducted many developmental projects for road renovation including VISION 2010. He was the Chairman of the Committee on Petitions (2006-07) and the Committee on the Welfare of Women, Children and Physically Handicapped (2011-16). Now, General Secretary, K.C.(M) and Secretary, Parliamentary Party, K.C.(M); Chairman, Committee on Papers Laid on the Table. Previously elected to KLA in 1996,2006 and 2011. ', '', 0, 'KC(M)', 'UDF'),
(95, 'C. K. Asha', '16456_730776.jpg', 95, 'Daughter of Shri K. Chellappan and Smt. Bhasurangi. V.B.; born at Vaikom.Was Vice President, A.I.S.F. Kottayam District Committee; State Committee Member, A.I.S.F.; Vice Chairperson, College Union St. Xaviers College, Vaikom (two times); Member, Mahilasangam Mandalam Committee; C.P.I. Branch Executive Member.', '', 0, 'CPI', 'LDF'),
(96, 'K. Suresh Kurup', '16448_730702.jpg', 96, 'Son of Shri P.A. Kunjan Pillai and Smt. Bharathi Amma; born at Kottayam; Advocate, Political Worker. Was Chairman, Kerala University Union (1978-79); State President, S.F.I. (1982-84); Elected to Parliament in the VIII, XII, XIII and XIV Lok Sabha from Kottayam. Now,Chairman, Committee on Local Fund Accounts. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(97, 'Thiruvanchoor Radhakrishnan', '16448_730695.jpg', 97, 'Son of Shri K.P. Parameswaran Pillai and Smt. Gourikutty Amma; born at Kottayam. Advocate and Social Worker. Entered politics while a student; was School Leader, Marthoma School, Kottayam (1964); General Secretary, All Kerala Balajanasakhyam (1965); Chairman, Baselios College Union (1967); Kottayam District Committee President (1967) and State General Secretary, K.S.U. (1969); General Secretary, Kerala University Union (1972); Chairman, Kerala University Union (1973); State President, K.S.U. (1974-77); State General Secretary (1978) and State President (1982) of Youth Congress; General Secretary, K.P.C.C. (1984-92) & (1994-2000); Syndicate Member, Cochin University of Science and Technology (1986-96); Senate Member, Kerala University. Was Minister for Water Resources and Parliamentary Affairs (5-9-2004 to 12-5-2006) and Minister for Revenue (23-5-2011 to 11-4-2012), Home & Vigilance (12-4-2012 to 31-12-2014), Forest, Environment, Transport, Sports and Cinema (1-1-2014 to 20-5-2016); was Opposition Chief Whip in 12th KLA. Now, Member: A.I.C.C., K.P.C.C. Executive Committee, K.P.C.C. Election Committee, K.P.C.C. Political Affairs Committee. Previously elected to KLA in 1991, 1996, 2001, 2006 and 2011. ', '', 0, 'INC', 'UDF'),
(98, 'Oommen Chandy', '16448_730699.jpg', 98, 'Son of Shri K.O. Chandy and Smt. Baby Chandy; born at Kumarakom. Political, Trade Union and Social Worker. Entered politics through K.S.U.; Was President, All Kerala Balajana Sakhyam (1961-62); President, K.S.U. (1967-69) and Youth Congress (1970-71); Member, A.I.C.C. Served as Minister for Labour (11.4.1977 — 27.10.1978); Minister for Home (28.12.1981 — 17.3.1982); Minister for Finance (2.7.1991 — 22.6.1994); Chief Minister (31.08.2004—12.05.2006 and 18.05.2011—20.05.2016); Opposition Leader in the 12th KLA; U.D.F. Convener and President of various Trade Unions. Previously elected to KLA in 1970, 1977, 1980, 1982, 1987, 1991, 1996, 2001, 2006 and 2011. ', '', 0, 'INC', 'UDF'),
(99, 'C. F. Thomas', '16448_730698.jpg', 99, 'Son of Shri C.T. Francis and Smt. Annamma; born at Changanassery; Teacher (Retired), Political and Trade Union Worker. Started political activities while a student. Was the General Secretary of Kerala Catholic Students League (K.C.S.L.); Joined Indian National Congress and then became Vice-President of Changanassery Town (West) Mandalam. Joined Kerala Congress on its formation and served as its Secretary and President of Changanassery Constituency, District Secretary, Kottayam, Member, State Executive Committee. Worked as General Secretary and Chairman of Kerala Congress (M). Minister for Rural Development, Registration, Khadi and Village Industries from 26-5-2001 to 5-9-2004. Now, Deputy Chairman, Kerala Congress (M). Previously elected to KLA in 1980, 1982, 1987, 1991, 1996, 2001, 2006 and 2011.\r\n', '', 0, 'KC(M)', 'UDF'),
(100, 'N. Jayaraj', '79168_730804.jpg', 100, 'Son of Shri K. Narayana Kurup, former Minister and former Deputy Speaker and Smt. K. Leela Devi; born at Champakara; Awarded Ph.D. from Kerala University on the topic \"RevenueExpenditure Pattern and its Implication on Economic Development of Kerala\"; Rtd. College Lecturer, Political Worker. Was Member, District Panchayat, Kottayam twice; Founder Member of \'Sanskriti\', an organisation for eco-social development; Executive Member, Kerala Folklore Academy for 5 years; Founder of \'Ente Manimalayar\'- an organisation for conservation of Manimalayar; active participant in environmental issues and General Editor of \'Ela\', an Environment Magazine. Now, General Secretary, Kerala Congress (M). Previously elected to KLA in 2006 and 2011.', '', 0, 'KC(M)', 'UDF'),
(101, 'P. C. George', '16456_730775.jpg', 101, 'Son of Shri Chackochan and Smt. Mariamma; born at Aruvithura. Social and Political worker. Entered politics through students\' movements. Was President, K.S.C. Ernakulam District Committee (1973-76); Chairman, Committee on Petitions of Kerala Legislative Assembly for seven and a half years, Committee on Subordinate Legislation for two and a half years, Committee on Papers Laid on the Table for two and a half years; General Secretary, Kerala Congress; Party leader, Kerala Congress (Secular) (2003-10); Govt. Chief Whip with Cabinet rank (2011-15); Board Member, K.S.R.T.C. for ten years; Vice-Chairman, Kerala Congress (M). In 2016 formed a new political party named \'Kerala Janapaksham\'. Previously elected to KLA in 1980, 1982, 1996, 2001, 2006 and 2011.', '', 0, 'Independent', '_'),
(102, 'A. M. Arif', '16456_730803.jpg', 102, 'Son of Shri Abdul Majeed and Smt. (Thankamma) Nabeesa; born at Mannar, Alappuzha. Advocate. Started political activity through S.F.I. Was the Cherthala Area President, Alappuzha District Secretary and State Committee Member of S.F.I.; College Union Chairman and Magazine Editor, S.N. College Cherthala; Cherthala Area Committee President and Secretary, State Committee Member of D.Y.F.I.;Cherthala Area Secretary and Alappuzha District Committee Member (since 1996) of C.P.I.(M); District Committee Member, C.I.T.U., Alappuzha; Secretary and Vice President, Shops and Establishment Employees Union, Cherthala Taluk; Senate member, Kerala University (three times). C.P.I.(M) District Committee Member, Alappuzha since 1996.Chairman, Committee on Private Members\' Bills and Resolutions. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(103, 'P. Thilothaman', '16448_730685.jpg', 103, 'Minister for Food & Civil Supplies Son of Shri Parameswaran and Smt. Gouri; born at Cherthala South; Social and Political activist. Entered politics through A.I.S.F. while studying in S.N. College, Cherthala; Was, Cherthala Taluk Secretary, Alappuzha District President and Vice-President of A.I.Y.F; became a Member of C.P.I. in 1977; President, Cherthala South Grama Panchayat(1995-96), C.V.C.S (1996) and Coastal Motor Transport Co-operative Society; Vice-President, Coirfed; Taluk President, Theeradesa Matsya Chumattu Thozhilali Union; Secretary, C.P.I. Cherthala Mandalam Committee; Secretary, C.P.I. Alappuzha District Council. Now, Member, C.P.I. State Council; State President, Coir Thozhilali Federation (A.I.T.U.C); President, Cherthala Taluk Chethu Thozhilali Union (A.I.T.U.C), Cherthala Coir Factory Workers Union. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI', 'LDF'),
(104, 'Thomas Isaac', '16448_730680.jpg', 104, 'Minister for Finance and Coir Son of Shri T.P. Mathew and Smt. Saramma Mathew; born at Kottappuram near Kodungalloor; Economist, Politician. Started political life as an S.F.I. activist (1971); was office bearer of SFI, Ernakulam Maharaja\'s College (1973-74); S.F.I. Ernakulam District President (1974-80); President, S.F.I. State Committee (1979); Senior Activist of Kerala Sasthra Sahithya Parishad from 1977; Research Associate, Associate Fellow and Fellow of Centre for Development Studies from 1980 to 2001. Awarded Doctorate in 1985; Honorary Fellow, Centre for Development Studies, Thiruvananthapuram; State Planning Board Member from 1996 to 2001; State President, Coir Centre (C.I.T.U.), Parliamentary Party Secretary of C.P.I. (M) in Kerala Legislative Assembly (2001-06); C.P.I. (M) State Committee Member since 1991; Chairman, Committee on Public Accounts, Kerala Legislative Assembly (2004-06, 2011-16). Was Minister for Finance from 18-5-2006 to 14-5-2011. Published a number of articles on Economics, Planning and Politics in the leading Regional, National and International Periodicals. Presented Lectures and Papers in National and International Conferences and Seminars. Books published in English include: 1. Science for Social Revolution (with Dr. B. Ekbal) (1989) 2. Modernisation and Employment : The Coir Industry in Kerala (with R.A. Van Stuijveberg and K.N. Nair) (1984). 3. Democracy at Work in an Industrial Co-operative: The story of Kerala Dinesh Beedi (with Richard W. Franke and Pyaralal Raghavan) (1998) 4. Local Democracy and Development: People\'s Campaign for DeCentralised Planning (With Richard W. Franke) (2000) Previously elected to KLA in 2001, 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(105, 'G. Sudhakaran', '16448_730691.jpg', 105, 'Minister for Public Works and Registration Son of Shri P. Gopala Kurup and Smt. L. Pankajakshi Amma; born at Vedaruplav, Thamarakulam, Mavelikkara; Social, Political, Educational and Cultural Worker. Actively participated in politics while a student. Was best speaker and best essayist in the college level. Joined C.P.I.(M) in 1967; became Joint Secretary of K.S.F.; office bearer of K.S.Y.F. at State Level and District Level in Alappuzha and Kollam; Founder Member, First State President, General Secretary, All India Vice-President and one of the first Members of the Central Executive Committee of S.F.I.; Member, C.P.I.(M) Kollam and Alappuzha District Committees; District Secretary, C.P.I.(M) Alappuzha; Secretary, C.P.I.(M) Kuttanadu and Ambalappuzha Taluks; Alappuzha District Council; District Secretary, State Treasurer and All India Committee Member, Agricultural Workers Union; District President, C.I.T.U. Alappuzha; President, C.I.T.U. Union of Kerala Spinners Limited, Scooters Kerala, K.S.D.P., Excel Glass, Lorry Transport Drivers and Cleaners Union, Alappuzha District Committee, Kuttanadu Rice Cultivators Protection Convention, Tagore Memorial Comparative Literature Society; Member, Executive Committee, K.A.U.; Urban Development Bank, Alappuzha and Kayamkulam Spinning Mill; Finance Committee Chairman, University of Kerala; Member, Kerala University Syndicate for 12 years; Chairman, FOURTEENTH KERALA LEGISLATIVE ASSEMBLY 265 Alappuzha District Co-operative Spinning Mill; District President, Head Load and General Workers Union, Alappuzha. Member, Regional Transport Authority (RTA) and Industrial Relations Committee (IRC), Kuttanadu. Now Member, C.P.I.(M) State Committee and Alappuzha District Committee. Served as Minister for Co-operation, Coir and Devaswoms (18-5-2006 to 16-8-2009) and Minister for Co-operation and Coir (17-8-2009 to 14-5-2011.) Previously elected to KLA in 1996, 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(106, 'Thomas Chandy', '16456_730795.jpg', 106, 'Son of Shri V. C. Thomas and Smt. Alyamma Thomas; born at Chennamkary; Diploma in Telecommunication Engineering; Businessman. Was President, K.S.U. and Youth Congress Kuttanad Constituency (1970); Chairman, Daveed Puthra Charitable Society, Kuttanad; Chairman of three Indian schools in Kuwait and one Indian school in Saudi Arabia; Chairman, Lake Palace 5 Star Resort, Alappuzha; Chairman, Atlanta Travels (IATA), Ernakulam; N.C.P. Parliamentary Party Leader; Served as Minister for Transport from 1-4-2017 to 15-11-2017. Now, Member, N.C.P. National Working Committee, Chairman Library Advisory Committee. Previously elected to KLA in 2006 and 2011.', '', 0, 'NCP', 'LDF'),
(107, 'Ramesh Chennithala', '16448_730687.jpg', 107, 'Son of Shri V. Ramakrishnan Nair and Smt. Devaki Amma; born at Chennithala. Social and Political Worker. Started political career right from the school days. Was, State Executive Member, State General Secretary and State President of K.S.U.; National General Secretary, Indian Youth Congress; sworn in as the youngest Minister in the State in 1986; Member Lok Sabha from Kottayam (1989, 1991,1996), from Mavelikara (1999); Member and Secretary of A.I.C.C. and President of K.P.C.C. (9 years); Was Member of Congress Working Committee (C.W.C.). Was Member, Public Accounts Committee, Lok Sabha, Parliamentary Consultative Committees for Commerce and Finance, Parliamentary Committees for Labour and Welfare, Civil Aviation Consultative Committee, Joint Parliamentary Committee on \'Pesticide Residues in and Safety Standards for Soft Drinks, Fruit Drinks and other Beverages\'; Member, Coir Board; Governing Body Member, All India Institute of Medical Sciences, New Delhi; Central Secretariat Member, World Federation of Young Parliamentarians; President, Youth for Social Action; Secretary, Youth Movement for Non-Apartheid. Was Minister for Rural Development (from 5-6-1986 to 25-3-1987); Minister for Home and Vigilance (from 1-1-2014 to 20-5-2016) Leader of Opposition Previously elected to KLA in 1982, 1987 and 2011. ', '', 0, 'INC', 'UDF'),
(108, 'U. Prathibha Hari', '16445_730981.jpg', 108, 'She was Alapuzha District Panchayat President and former President of Thakazhy Grama Panchayat. She Was Born in Thakzhy Village of Alapuzha District.\r\nShe earned fame and success in many areas like save Pampa ( Varattar) campaign and numerous nature preservation program mainly focused in her home district of Alappuzha. Her ability to mix amongst the cross section of society won her accolade and candidature in Kayamkulam constituency.\r\n', '', 0, 'CPI(M)', 'LDF'),
(109, 'R. Rajesh', '16446_730548.jpg', 109, 'Son of Shri A.T. Raghavan and Smt. Santha Raghavan; born at Mavelikara. Social Worker. Was Local Committee Secretary, C.P.I.(M), Cheriyanadu; State Committee Member and Alappuzha District Committee President of S.F.I.; State Committee Member, D.Y.F.I. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(110, 'Saji Cheriyan', 'image_110.jpg', 110, 'He also holds the post of Alappuzha District Secretary of Communist Party of India (Marxist). Cheriyan\'s entry into politics was through Students\' Federation of India, the student cadre of CPI(M), of which he was once the Alappuzha district secretary and later the president. He rose through the ranks and went on to become the district president of Democratic Youth Federation of India, the youth wing of CPI(M). He represented Mulakuzha division in the district panchayat in 1995 and became chairman of the development standing committee in the local body. He was elected CPM Chengannur area secretary in 2001. He has also holds positions of Area Committee Secretary of CPIM, Member of Alappuzha Jilla Panchayath, Kerala University Syndicate member, and CITU district President.\r\nOn 17 August 2018, his controversial Facebook post went viral among social media, stating his helplessness and request to the central government, regarding his constituency, which was at a highly dangerous zone during 2018 Kerala floods. Aftermath, actions were taken in order to save Chengannur.\r\n', '', 0, 'CPI(M)', 'LDF'),
(111, 'Mathew T. Thomas', '16448_730672.jpg', 111, 'Minister for Water Resources Son of Rev. T. Thomas and Annamma Thomas (late); born at Thiruvalla. Advocate. Entered politics during student days influenced by the J.P. Movement and protesting against the imposition of Emergency; Enrolled as a Member of Kerala Vidhyarthi Janata (KVJ) in 1977; Served as Unit Secretary, Marthoma College, Thiruvalla, Taluk Secretary, Thiruvalla, District President, Alappuzha, State Secretary and State President (1985-88) of K.V.J.; State President, Yuva Janata; Participated in many student agitations and was arrested several times; Volunteer Secretary of NSS in Marthoma College Unit; Secretary as well as Speaker , BRAINS TRUST, Marthoma College, Thiruvalla; Union Councillor, Kerala University and M.G. University; Secretary, Janata Head Load Workers Union, Thiruvalla, Quarry Workers Union, Thiruvalla; President and Secretary of Y.M.C.A, Thiruvalla. In 1987, got elected to the 8th KLA at the age of 25 and holds the record of being the youngest person to be elected as a Member of Kerala Legislative Assembly; Served as Minister for Transport, Printing and Stationery from 18-5-2006 to 20-3-2009; served as State President (2010-16), National WHO’S WHO FOURTEENTH KERALA LEGISLATIVE ASSEMBLY 151 Executive Member, Janata Dal (S); Senior Vice President of Janata Dal (S) State Committee; Parliamentary Party Leader of Janata Dal (S) in 13th KLA. Previously elected to KLA in 1987, 2006 and 2011. ', '', 0, 'JD(S)', 'LDF'),
(112, 'Raju Abraham', '16448_730665.jpg', 112, 'Son of Shri K. S. Abraham and Smt. Achumma Abraham; born at Ranni. Political and Social Worker. Entered politics through student movements; Was Unit Secretary, S.F.I., M.S. High School, Ranni; Chairman, St. Thomas College Union, Ranni; Councillor, Kerala University Union; Secretary, C.P.I.(M) Ranni Taluk Committee; President, Rubber Marketing Co-operative Society, Ranni; Bureau Chief, Deshabhimani Daily, Pathanamthitta; Joint Secretary, Press Club, Pathanamthitta; Member, Senate and Syndicate of M.G. University; Chairman, House Committee of Kerala Legislative Assembly; Acted in Telefilms; Individual Champion in Sports Meet for MLAs; Won many awards for excellence in social activities. Now, President, Pathanamthitta District Auto-Taxi Workers Union (C.I.T.U.); State President, SIDCO Employees Union; Member, C.P.I.(M) Pathanamthitta District Secretariat; Syndicate Member, M.G. University; Chairman, Committee on Petitions. Previously elected to KLA in 1996, 2001, 2006 and 2011.', '', 0, 'CPI(M)', 'LDF'),
(113, 'Veena George', '16448_730666.jpg', 113, 'Daughter of Adv. P. E. Kuriakose and Smt. Rosamma Kuriakose; born at Thiruvananthapuram; Journalist. Was active member of S.F.I.', '', 0, 'CPI(M)', 'LDF'),
(114, 'Adoor Prakash', '16448_730671.jpg', 114, 'Son of late N. Kunjuraman and Smt. V. M. Vilasini; born at Adoor. Advocate, Political and Trade Union Worker. Entered politics through student movement; Was, President, K.S.U. (I) Kollam Taluk Committee (1976-77), K.S.U.(I) Kollam District Committee (1978-79); Vice President, K.S.U. (I) State Committee (1979-81); General Secretary, Youth Congress (I) Kollam District Committee (1981-84); President, Youth Congress(I) Pathanamthitta District (1984-88); General Secretary, Youth Congress(I) State Committee (1988-92). Joint Secretary, KPCC (I) (1997-2001); elected Member of the KPCC(I) from 1992; Vice President, D.C.C.(I), Pathanamthitta from 1993. Also held the posts of Working President, Kerala Private College Employees Association and General Secretary, Kerala State Electricity Employees Congress; President, Pampa Rubber Employees Association, Traco Cable Employees Association, Financial Enterprises Employees Congress; President, Adoor Taluk Taxi and Mini Vehicles Drivers Congress; Director, Kerala State Co-operative Bank, Kerala State Cooperative Rubber Marketing Federation, District Co-operative Bank, Pathanamthitta; President, Adoor Taluk Co-operative Rubber Marketing Society; President, Other Backward Class Co-operative Agriculture Society. Was Minister for Food and Civil Supplies and Consumer Affairs, Printing and Stationery from 5-9-2004 to 12-5-2006. In the Ministry headed by Shri Oommen Chandy, he was the Minister for Health and Coir,Pollution Control from 23-5-2011 to 11-4-2012 and the Minister for Revenue and Coir,Legal Metrology from 12-4-2012 to 20-5-2016. Previously elected to KLA in 1996, 2001, 2006 and 2011.', '', 0, 'INC', 'UDF'),
(115, 'Chittayam Gopakumar', '16445_730985.jpg', 115, 'Son of Shri T. Gopalakrishnan and Smt. T. K. Devayani; born at Chittayam. Social Worker. Was Member, A.I.S.F. State Committee; Member, A.I.T.U.C. State Working Committee. Now, State Secretary, Kerala Kashuvandi Thozhilali Kendra Council; State President, Asha Workers Union (A.I.T.U.C.); Member, C.P.I., State Council; State Executive member and Kollam District President, Kerala State Karshaka Thozhilali Federation B.K.M.U.-A.I.T.U.C; Chairman, Committee on Welfare of Backward Class Communities. Previously elected to KLA in 2011.', '', 0, 'CPI', 'LDF'),
(116, 'R. Ramachandran', '16448_730654.jpg', 116, 'Son of Shri Raghavan Unnithan and Smt. Eswari Amma; born at Kallelibhagam, Karunagappally; Social and Political worker. Was State Joint Secretary, A.I.S.F.; Kollam District Joint Secretary, A.I.Y.F.; Chavara Mandalam Committee Secretary, Karunagapally Taluk Secretary, Kollam District Committee, C.P.I.; Member, C.P.I. State Executive, Kollam District Council; Vice President, District Panchayat, Kollam; Convener, L.D.F. Kollam District Committee; Chairman, SIDCO; President, various trade unions; President, Urban Co-operative Society, Karunagappally. Now Member, State Council, C.P.I.', '', 0, 'CPI', 'LDF'),
(117, 'N. Vijayan Pillai', '16445_730988.jpg', 117, 'Son of Shri V. Narayana Pillai and Smt. Bhavani Amma; born at Chavara. Social and Political Worker, Agriculturist, Industrialist. Active in politics for 45 years. Was, Member (20 years), Chavara Grama Panchayat and Chairman, Standing Committee, Chavara Grama Panchayat; Member, Kollam District Panchayat (5 years); Vice President, Kollam District Co-operative Bank; President, Kollam District Human Resource Co-operative Society; President, Pattathanam Service Co-operative Bank. ', '', 0, 'CMP', 'LDF'),
(118, 'Kovoor Kunjumon', '16448_730661.jpg', 118, 'Son of Shri Podiyan and Smt. K. Saradha; born at Kovoor. Social Worker. Entered politics while a student; Was President, P.S.U. District Committee, Kollam; Joint Secretary, P.S.U. State Committee. Had active participation and office bearership in library activities and socio cultural activities. Now Secretary, R.S.P.(L) Kerala State Committee. Previously elected to KLA in 2001, 2006 and 2011. Resigned from the 13th KLA on 28.01.2016', '', 0, 'RSP', 'LDF'),
(119, 'Adv P. Ayisha Potty', '16448_730655.jpg', 119, 'Daughter of Shri N. Vasudevan Potty and Smt. M. J. Parvathy Antharjanam; born at Kidangoor in Kottayam. Advocate. Now Member, C.P.I.(M) Kottarakkara Area Committee; State Committee Member, A.I.D.W.A.; State Joint Secretary, A.I.L.U.; State President, Women Lawyers Association; President, Anganawadi Workers & Helpers Association, Kollam District. Chairperson, Committee on the Welfare of Women, Children and Physically Handicapped. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI(M)', 'LDF');
INSERT INTO `member` (`id`, `name`, `image`, `constituency_id`, `description`, `html_content`, `points`, `party`, `front`) VALUES
(120, 'K.B. Ganesh Kumar', '16445_730993.jpg', 120, 'Son of Shri R. Balakrishna Pillai, former minister and Smt. Valsala Balakrishna Pillai; born at Kottarakkara. Cine Artist. Was Founder Member and Treasurer, AMMA (Association of Malayalam Movie Artists); Founder President, ATMA (Association of Malayalam Television Media Artists); Founder Chairman, Malayalam Television Fraternity. Minister for Transport (17.5.2001—10.3.2003); Minister for Forest, Sports & Cinema (18.5.2011—2.4.2013). Now, Vice Chairman, Kerala Congress (B); Vice President, AMMA; President, ATMA; President, Malayalam Television Artists Association Co-operative Society; State President, Kerala Elephant Owners Federation; Chairman, Malayalam Television Fraternity. Previously elected to KLA in 2001, 2006 and 2011. ', '', 0, 'KC(B)', 'LDF'),
(121, 'K. Raju', '16448_730658.jpg', 121, 'Minister for Forests, Animal Husbandry and Zoos. Son of Shri G. Karunakaran and Smt. K. Pankajakshy; born at Nettayam, Yeroor, Kollam. Advocate. Was State Vice-President, Library Council (1995-2005); Member, Yeroor Panchayat; Chairman, Standing Committee, Kollam District Panchayat; Member, Scheduled Tribe State Advisory Committee, Forest-Wildlife State Advisory Committee; Senate Member, Kerala University; Member, General Council, National University of Advanced Legal Studies, Kochi. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI', 'LDF'),
(122, 'Mullakkara Ratnakaran', '16445_730990.jpg', 122, 'Son of Shri A. Purushothaman and Smt. D. Sulochana; born at Mullakkar; Social and Political Worker. Entered politics through student movements. Was State Secretary and State President, A.I.Y.F; Member, C.P.I. National Council and C.P.I. State Secretariat. Served as Minister for Agriculture (18-5-2006 to 14-5-2011). Now, Chairman, Committee on Environment. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI', 'LDF'),
(123, 'J. Mercykutty Amma', '16448_730656.jpg', 123, 'Minister for Fisheries, Harbour Engineering and Cashew Industry Daughter of Shri Fransis and Smt. Jainamma; born at Munrothuruth.  Social Worker. As student, entered politics through S.F.I.; Was students’ representative, Kerala University Senate (1978); Central Committee Vice President, S.F.I. (1987); Served as Chairperson, MATSYAFED; Chairperson, Committee on Public Undertakings (during 10th KLA). Now, Member, State Committee, C.P.I.(M); All India Vice President and State Secretary, C.I.T.U.; Vice President, Kerala Cashew Workers Centre. Previously elected to KLA in 1987 and 1996.', '', 0, 'CPI(M)', 'LDF'),
(124, 'M. Mukesh', '16456_730784.jpg', 124, 'Son of Shri O. Madhavan and Smt. Vijaya Kumari; born at Kollam. Actor. Was Chairman, Kerala Sangeetha Nataka Academy. Now, Executive Member, AMMA.', '', 0, 'CPI(M)', 'LDF'),
(125, 'M. Noushad', '16456_730786.jpg', 125, 'Son of late Muhammed Haneefa and late Khadeeja Beevi; born at Vadakkevila, Pallimukku. Social and Political Worker. Entered politics through S.F.I. Was General Secretary, College Union, Kollam Fatima Matha National College (1984-85); Vice Chairman, Kerala University Union (1986-87); Vice President, S.F.I. Kollam District (1988-92); Member, S.F.I State Committee (1988-92); Joint Secretary, D.Y.F.I. Eravipuram Area Committee (1992-96); Vice President, Vadakkevila Grama Panchayat (1995-2000); Chairman, Standing Committee for Development, Kollam Corporation (2005-10); Chairman, Standing Committee for Welfare, Kollam Corporation (2010-14); Deputy Mayor, Kollam Corporation (2014-15); Chairman, Standing Committee for Development, Kollam Corporation (2015- 16); C.P.I.(M) Kollam East Area Committee Member since 2000.', '', 0, 'CPI(M)', 'LDF'),
(126, 'G. S. Jayalal', '16448_730657.jpg', 126, 'Son of Shri S. Gopalakrishna Pillai and Smt. Sathi Bai Amma; born at Chirakkara . Social Worker. Was Member, Kollam District Committee, Balavedi; Secretary, Chathannoor Mandalam Committee, Kollam District Joint Secretary, A.I.S.F.; Secretary, Chathannoor Mandalam Committee, Kollam District Committee Member, A.I.Y.F.; Secretary, Chathannoor Mandalam Committee, Kollam District Committee Member, State Committee Member, C.P.I.; State President, All Kerala Water Authority Employees Union (A.I.T.U.C.); Vice-President, Kollam Co-operative Spinning Mill Employees Union; Working President, Chathannoor Construction Workers Union; Vice-President, Kasuvandi Thozhilali Council, Chathannoor; Region President, Auto-Motor Thozhilali Union; Member, District Library Council, Kollam. Previously elected to KLA in 2011. ', '', 0, 'CPI', 'LDF'),
(127, 'V. Joy', '16446_7309231.jpg', 127, 'Son of Shri P. K. Vijayan and Smt. T. Indira; born at Perunguzhi. Advocate. Was President, S.F.I. Thiruvananthapuram District; Vice President, D.Y.F.I. Thiruvananthapuram District; President, Azhoor Grama Panchayat, chirayinkeezhu Block Panchayat; Member, Thiruvananthapuram District Panchayat; Vice President, State Cooperative Bank; Senate member, University of Kerala. Now, Member, C.P.I.(M) Thiruvananthapuram District Committee.', '', 0, 'CPI(M)', 'LDF'),
(128, 'B. Sathyan', '79168_730584.jpg', 128, 'Son of Shri K. Bhaskaran and Smt. S. Thankamma; born at Thiruvananthapuram; Advocate. Entered politics through student movements. Was, Chairman, Thiruvananthapuram University College Union; General Secretary, State I.T.I. Union; Member, Kerala University Senate and Syndicate; Councillor, Thiruvananthapuram Corporation; President, Thiruvananthapuram District Panchayat (1995-2000). Now, C.P.I.(M) District Committee Member (for 26 years); Vice President, C.I.T.U. Thiruvananthapuram District and Office bearer of various trade unions; President, C-apt Employees Union (C.I.T.U.); Vice-President, Pattikajathi Kshema Samithi; President, All Kerala D.T.P.C. Union; Chairman, Committee on the Welfare of SC/ST; Member, Fisheries University Senate, SC/ST Advisory Committee; Patron, Kerala State Document Workers Union. Previously elected to KLA in 2011.', '', 0, 'CPI(M)', 'LDF'),
(129, 'V. Sasi', 'image_129.jpg', 129, 'Son of Shri A. Velu and Smt. K. Saradha; born at Thiruvananthapuram; Engineer. After graduating in Engineering, engaged in public service for nearly three decades holding senior positions in government; Was Director, Managing Director and Chairman of various Public Sector Undertakings, Cooperative institutions in the Public Sector; Member, Board of Directors, National Textile Corporation, Coir Board; Private Secretary to Shri. P.K. Raghavan, the Minister for SC/ST Development (1987-91); Chairman, Kerala Agricultural Workers Welfare Fund Board (2006-09); Member, Travancore Devaswom Board. Member, C.P.I District Council, Thiruvananthapuram; Member, BKMU State Council. Previously elected to KLA in 2011.', '', 0, 'CPI', 'LDF'),
(130, 'C. Divakaran', '16446_730929.jpg', 130, 'Son of Shri Sivarama Panicker and Smt. Ammukutty Amma; born at Thiruvananthapuram. Social Worker. Entered politics while a student and took part in various political struggles; was elected as Secretary, Mahatma Gandhi College Pre-University Association in 1960; started political career as A.I.Y.F. member; was District Secretary, A.I.Y.F., Thiruvananthapuram; State General Secretary, All India Vice President, A.I.T.U.C; National Executive Member and Parliamentary Party Leader of C.P.I.; Minister for Food & Civil Supplies and Animal Husbandry from 18-5-2006 to 14-5-2011. Now, Chairman and Managing Director of Prabhath Book House; Chairman, Committee on Public Undertakings. Previously elected to KLA in 2006 and 2011.', '', 0, 'CPI', 'LDF'),
(131, 'D. K. Murali', '16456_730778.jpg', 131, 'Son of Shri V. Damodaran Nair and Smt. T. P. Jagadamma; born at Vattayam near Venjaramood . Advocate; Politician Member, Thiruvananthapuram District Committee, C.P.I.(M); Thiruvananthapuram District Treasurer and State Working Committee Member, Kerala Karshaka Sangham. ', '', 0, 'CPI(M)', 'LDF'),
(132, 'Kadakampally Surendran', '16445_730996.jpg', 132, 'Minister for Co-operation, Tourism and Devaswoms Son of Shri C. K. Krishnankutty and Smt. Bhagavathikutty; born at Thiruvananthapuram. Political Worker. Entered politics through Student and Youth Movements; Was All India Vice President, Democratic Youth Federation of India; Member, State Committee, C.P.I.(M); Leader of various trade unions. Was Minister for Electricity and Devaswom from 25-5-2016 to 21-11-2016. Previously elected to KLA in 1996.', '', 0, 'CPI(M)', 'LDF'),
(133, 'K. Muraleedharan', 'muraleedharan.jpg', 133, 'ഒരു ഇന്ത്യൻ രാഷ്ട്രീയ പ്രവർത്തകനും, മുൻ ലോകസഭാംഗവുമാണ്‌ കെ. മുരളീധരൻ. ഇന്ത്യൻ നാഷണൽ കോൺഗ്രസ്ന്റെ കേരള പ്രസിഡണ്ടായിരുന്ന (KPCC) കെ. മുരളീധരൻ, കേരളത്തിലെ പ്രമുഖ കോൺഗ്രസ് നേതാവ് കെ. കരുണാകരന്റെ മകനാണ്.', 'ഇന്ത്യൻ നാഷണൽ കോൺഗ്രസ്സിൽ പ്രവർത്തിച്ചിരുന്ന മുരളീധരൻ മൂന്നുതവണ കോഴിക്കോട് മണ്ഡലത്തിൽനിന്ന് ലോക്‌സഭയിലേക്ക് തിരഞ്ഞെടുക്കപ്പെട്ടിട്ടുണ്ട്. ഇന്നു കേരളത്തിൽ കോൺഗ്രസിന്റെ ഏറ്റവും ജനകീയരായ നേതാക്കളിൽ ഒരാളെയാണ് കെ മുരളീധരൻ അറിയപ്പെടുന്നത് . 2018 സെപ്റ്റംബറിൽ കെ മുരളീധരനെ സംസ്ഥാന പ്രചരണാ സമിതി അധ്യക്ഷനായി കോൺഗ്രസ് ഹൈക്കമാൻഡ് നിയമിച്ചു', 0, 'INC', 'UDF'),
(134, 'V. S. Sivakumar', '16446_730939.jpg', 134, 'Son of Shri K.V. Sadasivan Nair and Smt. B. Subhadra Amma; born at Amaravila. Advocate, Social and Political Worker. Started political career as activist of the Kerala Students Union and rose through the Youth Congress. Was, Councillor, Kerala University Union and State Executive Member, K.S.U. (1978-79); Chairman, College Union, V.T.M.N.S.S College, Dhanuvachapuram(1979-80); General Secretary, Block Congress Committee, Parassala (1980-82); Magazine Chief Editor, Thiruvananthapuram Law Academy (1983-84); President, Block Youth Congress, Parassala (1982-1987); Vice-President, District Youth Congress, Thiruvananthapuram (1987-93); President, District Youth Congress, Thiruvananthapuram (1993-2000); Member, K.P.C.C. and A.I.C.C. (since 1999); Member, Lok Sabha, from Thiruvananthapuram Constituency (1999-2004); General Secretary, K.P.C.C. (2005-07); President, District Congress Committee, Thiruvananthapuram (2007-11); Member, Parliamentary Standing Committee for Science, Technology and Commerce and Consultative Committee for Ministry of Road Transport and Highways. Was Minister for Transport and Devaswom (23-5-2011 to 11-4-2012) and Minister for Health, Family Welfare and Devaswoms (12-4-2012 to 25-5-2016.) Previously elected to KLA in 2011.', 'V. S. Sivakumar', 0, 'INC', 'UDF'),
(135, 'O. Rajagopal', '79168_730579.jpg', 135, 'Son of Shri Madhavan Nair and Smt. Kunjhikkavu Amma; born at Manappadam, Palakkad. Advocate, Social Worker. Was Member, National Executive of Jan Sangh (1964-77); State President, Jan Sangh (1974-77); President, Small Farmers Association, B.J.S.; General Secretary, Kerala State Janata Party (1977-80); President (1980-84), National Secretary (1986-89), National General Secretary (1989-92), National Vice President B.J.P.; and Member, B.J.P. Parliamentary Board (1992-95), Member, Rajya Sabha (1992-2004); Chairman, Committee on Petitions (1996-98); Deputy Leader, B.J.P. in Rajya Sabha (2002-04); Minister of State in the Ministry of Law, Justice and Company Affairs and Minister of State in the Ministry of Parliamentary Affairs (1999-04); Minister of State in the Ministry of Railways (September 2001-July 2002); Minister of State in the Ministry of Urban Development and Poverty Alleviation (July 2002 - January 2003); Minister of State in the Ministry of Defence Production (29 January 2003 - 22 May 2004). Patron of Madhya Pradesh Malayalee Sangh, Bhopal, Delhi Malayalee Sangh and Matha Amritanandamayi Devi Seva Samithi, Delhi; President, K.G. Marar Smaraka Samithi and Trust, Aurobindo Cultural Society, Thiruvananthapuram; Director, Mathruka Pracharanalayam, Cochin; Editor \"Chithi\" Monthly news magazine, Thiruvananthapuram.', '', 0, 'BJP', 'NDA'),
(136, 'K. S. Sabarinathan', 'image_136.jpg', 136, 'Son of former Minister and former Speaker G. Karthikeyan and DR. M. T. Sulekha; born at Thiruvananthapuram; Social Worker. Worked extensively in Maharashtra, North-East, Madhya Pradesh and other Indian states on development projects in the areas of health, nutrition and livelihoods. Previously elected to KLA in 2015 through by-election held on 27-6-2015.', '', 0, 'INC', 'UDF'),
(137, 'C. K. Hareendran', '16446_730940.jpg', 137, 'Son of Shri G. Krishnan Nair and Smt. B. Sumathy Amma; born at Amboori. Social Worker. Entered politics through S.F.I. Unit activities while a high school student. Was Secretary, S.F.I. Unit, V.T.M.N.S.S. College Dhanuvachapuram; Joint Secretary and Secretary, S.F.I. Neyyattinkara Taluk Committee; Branch Secretary, Local Committee Secretary, Area Secretary and District Committee Member, C.P.I.(M); Local and Taluk Committee Secretary, K.S.Y.F.; Neyyattinkara Taluk Secretary and Thiruvananthapuram District Committee Joint Secretary, D.Y.F.I.; Area Secretary, C.P.I.(M) Neyyattinkara Area Committee (for 24 years); Vice President, Karshaka Sangham Trivandrum District Committee; Director Board Member, KAL; Advisory Board Member, Nettukaltheri Open Jail. Now, C.P.I.(M) Thiruvananthapuram District Committee Member; District President and State Committee Member, NREG Workers’ Union.\r\n', '', 0, 'CPI(M)', 'LDF'),
(138, 'I. B. Satish', '16446_730943.jpg', 138, 'Son of Shri P. Balakrishnan Nair and Smt. K. Indira Devi; born at Kattakkada; Advocate. Entered politics through Balasangham and student movements; Was Union Chairman, Kattakkada Christian College; Chairman, Kerala University Union; District President, S.F.I., Thiruvananthapuram; District President, D.Y.F.I., Thiruvananthapuram; Area Secretary, C.P.I.(M), Kattakada; Thiruvananthapuram District Committee Member, C.P.I.(M).', '', 0, 'CPI(M)', 'LDF'),
(139, 'M. Vincent', '16456_730790.jpg', 139, 'Son of Shri M. Michael Pillai and Smt. M. Phillis; born at Balaramapuram; Advocate. Entered politics while a student; Served as K.S.U. Unit President, Christian College, Kattakada; Taluk Secretary, K.S.U., Neyyattinkara; District General Secretary, K.S.U., Thiruvananthapuram; General Secretary, D.C.C. Thiruvananthapuram; Secretary, K.P.C.C.; Member, Nemom Block Panchayat (1996-2000); Convener, Students Wing and District President, Kerala Deshiya Vedi; Chairman, Dr. B.R. Ambedkar Center.', '', 0, 'INC', 'UDF'),
(140, 'K. Ansalan', '16446_730946.jpg', 140, 'Son of Shri Karunakaran and Smt. Thankom, born at Neyyattinkara. Political worker. Was Area Committee Secretary, S.F.I., Neyyattinkara; Area President, Secretary, D.Y.F.I., Neyyattinkara; Local Committee Secretary, Area Committee Secretary, C.P.I.(M), Amaravila; Vice Chairman, Leader of Opposition, Neyyattinkara Municipality.', '', 0, 'CPI(M)', 'LDF');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(500) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `banner_image` varchar(50) DEFAULT 'placeholder.jpg',
  `img_status` tinyint(1) DEFAULT NULL,
  `language` char(2) NOT NULL,
  `constituency_id` int(11) NOT NULL DEFAULT '0',
  `author_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `up_votes` int(11) NOT NULL DEFAULT '0',
  `down_votes` int(11) NOT NULL DEFAULT '0',
  `is_voted` tinyint(1) NOT NULL,
  `postTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `banner_image`, `img_status`, `language`, `constituency_id`, `author_id`, `category_id`, `owner`, `up_votes`, `down_votes`, `is_voted`, `postTime`, `update_time`) VALUES
(8, 'ഷുഹൈബ് അനുസ്‌മരണം ', 'ഗാന്ധി സെന്ററിന്റെ നേതൃത്വത്തിൽ സെക്രട്ടറിയേറ്റിന് മുമ്പിൽ സംഘടിപ്പിച്ച ഷുഹൈബ് അനുസ്മരണത്തിൽ സംസാരിച്ചു.', 'shuhaib2.jpg', NULL, 'ml', 133, NULL, 1, NULL, 1, 0, 1, '2019-02-14 02:51:50', NULL),
(9, 'പേരൂർക്കട മാനസിക ആരോഗ്യ കേന്ദ്രത്തിൽ നെയ്ത്തുശാല ഉദ്ഘാടനം', 'പേരൂർക്കട മാനസിക ആരോഗ്യ കേന്ദ്രത്തിൽ രോഗികളുടെ പുനരധിവാസത്തിനായി എം.എൽ.എയുടെ ആസ്തി വികസന ഫണ്ടിൽ നിന്നും ഒൻപത് ലക്ഷം രൂപ വിനിയോഗിച്ചു നിർമ്മിച്ച നെയ്ത്തുശാലയുടെ ഉദ്ഘാടനം നിർവ്വഹിച്ചു.', 'peroorkada.jpg', NULL, 'ml', 133, 0, 1, NULL, 2, 0, 1, '2019-02-14 03:16:23', NULL),
(10, 'വട്ടിയൂർക്കാവ് റോഡ് വികസനം ', 'വട്ടിയൂർക്കാവ് റോഡ് വികസനത്തെക്കുറിച്ച് ഞാൻ നിയമസഭയിൽ ഉന്നയിച്ച സബ്‌മിഷനു മറുപടിയായി വകുപ്പുമന്ത്രിയിൽ നിന്നും പദ്ധതിനടത്തിപ്പിൻറെ വിശദമായ പ്രോജെക്ട് റിപ്പോർട്ട് ഉടൻ തന്നെ തയ്യാറാക്കുമെന്നുള്ള ഉറപ്പ് ലഭിച്ചു. അതിലേക്കായി ബന്ധപ്പെട്ട വകുപ്പിന്റെ തിരുവനന്തപുരം എക്സിക്യൂട്ടീവ് എൻജിനീയറിനെ ചുമതലപ്പെടുത്തിയതായും അറിയിച്ചു.<br>', 'VATTIYOORKAVU.jpg', NULL, 'ml', 133, 0, 1, NULL, 6, 0, 1, '2019-02-14 06:35:25', NULL),
(11, 'ശാസ്തമംഗലം എച്ച്.എസ്.എസിൽ എസ്.പി.സി പാസ്സിംഗ് ഔട്ട് പരേഡ് ഉദ്ഘാടനം ചെയ്തു.', '<div>ശാസ്തമംഗലം എച്ച്.എസ്.എസിൽ എസ്.പി.സി പാസ്സിംഗ് ഔട്ട് പരേഡ് ഉദ്ഘാടനം ചെയ്തു. സംസ്‌ഥാന ആഭ്യന്തരവകുപ്പും വിദ്യാഭ്യാസവകുപ്പും ചേർന്ന്‌ 2010 ൽ കേരളത്തിൽ രൂപംകൊടുത്ത പദ്ധതിയാണ് സ്‌റ്റുഡന്റ്‌ പോലീസ്‌ കേഡറ്റ്‌ പദ്ധതി (എസ്.പി.സി). 2010 ഓഗസ്‌റ്റ് 2ന്‌ കേരളത്തിലാകെ 127 സ്‌കൂളുകളിലായി 11176 ഹൈസ്‌കൂൾ വിദ്യാർഥികളെ ഉൾപ്പെടുത്തിക്കൊണ്ടാണ്‌ എസ്‌.പി.സി. എന്ന ചുരുക്കപ്പേരിലറിയപ്പെടുന്ന സ്‌റ്റുഡന്റ്‌ പോലീസ്‌ കേഡറ്റ്‌ പദ്ധതിക്കു തുടക്കംകുറിച്ചത്‌.</div><div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; വിദ്യാർഥികളിൽ പ്രകൃതിസ്നേഹം, പരിസ്ഥിതി സംരക്ഷണബോധം, പ്രകൃതി ദുരന്തങ്ങൾക്കെതിരെ പ്രവർത്തിക്കാനുള്ള സന്നദ്ധത എന്നിവ വളർത്തുക, സാമൂഹ്യപ്രശ്നങ്ങളിൽ ഇടപെടാനും ദുരന്തഘട്ടങ്ങളിൽ ഉണർന്നു പ്രവർത്തിക്കാനും ഉള്ള മനോഭാവം വിദ്യാർഥികളിൽ വളർത്തുക, സ്വഭാവ ശുദ്ധിയിലും പെരുമാറ്റ ശീലത്തിലും മൂല്യങ്ങൾ ഉയർത്തിപ്പിടിക്കുന്ന ഒരു മാതൃകാ വിദ്യാർഥി സമൂഹത്തെ വാർത്തെടുക്കുക,&nbsp;<span style=\"letter-spacing: 0.2px;\">പൗരബോധവും, ലക്ഷ്യബോധവും, സാമൂഹ്യ പ്രതിബദ്ധതയും, സേവനസന്നദ്ധതയുമുള്ള ഒരു യുവജനതയെ വാർത്തെടുക്കുക,</span><span style=\"letter-spacing: 0.2px;\">&nbsp;എന്നിവയാണ് പദ്ധതിയുടെ ലക്ഷ്യങ്ങൾ.</span></div>', 'sasthamangalam-school.jpg', NULL, 'ml', 133, 0, 1, NULL, 0, 0, 0, '2019-03-04 22:37:37', NULL),
(12, 'KSEB വഴി ഉപഭോക്താക്കൾക്ക് എല്‍ ഇ ഡി ബള്‍ബുകൾ വിതരണം ചെയ്യുന്നു.', '<div>KSEB കേന്ദ്ര സ്ഥാപനമായ എനര്‍ജി എഫിഷ്യന്‍സി സര്‍വീസസ് ലിമിറ്റഡിന്റെ ഡൊമെസ്റ്റിക് എഫിഷ്യന്റ് ലൈറ്റിംഗ് പ്രോഗ്രാം (ഡെല്‍പ്‌) വഴി നടത്തുന്ന ലാഭപ്രഭ മൂന്നാം സീസണ്‍ പ്രകാരമുള്ള എല്‍ ഇ ഡി ബള്‍ബ് വിപണനം സംബന്ധിച്ച വിവരങ്ങള്‍ :</div><div><br></div><div>ഒന്‍പത് വാട്ടിന്റെ രണ്ട് എല്‍ ഇ ഡി ബള്‍ബുകളാണ് പദ്ധതിപ്രകാരം 190 രൂപയ്ക്ക് ഒരു ഉപഭോക്താവിന് ലഭ്യമാക്കുന്നത്. മാര്‍ക്കറ്റില്‍ 400 രൂപയോളം വിലവരുന്ന ഒരു ബള്‍ബിന് 95 രൂപയേ ഈടാക്കുന്നുള്ളൂ. ആയിരം വാട്ട്സില്‍ താഴെ കണക്റ്റഡ് ലോഡും മാസം 40 യൂണിറ്റില്‍ താഴെ ഉപഭോഗവുമുള്ള ‘നോണ്‍ പേയിംഗ് വിഭാഗ’ത്തില്‍പ്പെട്ടവര്‍ക്ക് രണ്ട് ബള്‍ബ് സൗജന്യമായി അവരവരുടെ വീട്ടിലെത്തിക്കും. ബില്ലിനൊപ്പം അറിയിപ്പുലഭിച്ചതിനുശേഷം മാത്രം ബള്‍ബിനായി സെക്ഷന്‍ ഓഫീസിനെ സമീപിച്ചാല്‍ മതി.&nbsp;</div><div>വൈദ്യുതി ബില്‍ ഓണ്‍ലൈന്‍ നല്‍കുന്നവര്‍ക്ക് ബള്‍ബുകള്‍ ഓര്‍ഡര്‍ ചെയ്യാന്‍ മാര്‍ച്ച് ഒന്നുമുതല്‍ ഓണ്‍ലൈന്‍ സംവിധാനമൊരുക്കും. ഇവര്‍ ബള്‍ബുകള്‍ വീട്ടിലെത്തിക്കുന്നതിനുള്ള ഡെലിവറി ചാര്‍ജ് കൂടെ നല്‍കണം.</div>', 'LED_Bulb.jpg', NULL, 'ml', 0, 0, 1, NULL, 0, 0, 1, '2019-03-11 01:46:56', NULL),
(13, 'സുകന്യ സമൃദ്ധി യോജന - പെൺകുഞ്ഞുങ്ങൾ ഉള്ളവർക്കു മാത്രം !', '<span id=\"docs-internal-guid-44c71a36-7fff-228b-b2d8-e9bdfcf8c61f\"><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\"><span style=\"font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"> പെണ്‍കുട്ടികളുടെ ഉന്നത വിദ്യാഭ്യാസം, വിവാഹം എന്നിവ സുഗമമായി നടത്തുവാന്‍ മാതാപിതാക്കളെ പ്രാപ്തരാക്കുകയെന്ന ലക്ഷ്യത്തോടെ സർക്കാർ ആരംഭിച്ച പദ്ധതിയാണ് \'സുകന്യ സമൃദ്ധി\'. 2015 ജനുവരിയിലാണ് പദ്ധതിയ്ക്ക് തുടക്കമിട്ടത്. &nbsp;10 വയസ്സ് കഴിയാത്ത പെൺകുട്ടികളുടെ പേരിൽ രക്ഷിതാക്കൾക്ക് നിക്ഷേപിക്കാവുന്നതാണ്. പോസ്റ്റ് ഓഫീസുകളിലോ ബാങ്ക് ശാഖകളിലോ നിക്ഷേപം നടത്താം. 250 രൂപയിൽ തുടങ്ങി പരമാവധി 1.5 ലക്ഷം രൂപവരെ സുകന്യ അക്കൗണ്ടിൽ നിക്ഷേപിക്കാം. 100 രൂപയുടെ ഗുണിതങ്ങളായി ഒരുവർഷം എത്ര തവണ വേണമെങ്കിലും അടയ്ക്കാം. രാജ്യത്തെ എല്ലാ തപാൽ ഓഫീസുകളിലും ഈ അക്കൗണ്ട് തുടങ്ങാനുള്ള അവസരമുണ്ട്. അക്കൗണ്ട് തുറക്കുന്ന തീയതി മുതൽ 14 വർഷം വരെ നിക്ഷേപം നടത്താവുന്നതാണ്. പെൺകുട്ടിക്ക് 21 വയസ്സ് ആകുമ്പോഴാണ് പണം തിരിച്ചെടുക്കാൻ കഴിയുക. 18 വയസ്സ് കഴിഞ്ഞാൽ 50 ശതമാനം പണം പിൻവലിക്കാൻ അനുവദിക്കുന്നുണ്ട്. ഉന്നത വിദ്യാഭ്യാസച്ചെലവിനായാണ് 50 ശതമാനം പിൻവലിക്കാൻ കഴിയുക</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\"><b style=\"background-color: transparent; color: rgb(0, 0, 0); font-family: Arial; font-size: 11pt; white-space: pre-wrap; letter-spacing: 0.2px;\">അക്കൗണ്ടുകളുടെ എണ്ണം</b><br></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\"><span style=\"font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">ഒരു പെൺകുട്ടിക്ക് ഒരു അക്കൗണ്ട് മാത്രമേ എടുക്കാൻ സാധിക്കുകയുള്ളൂ. ഒരു കുടുംബത്തിൽ രണ്ട് പെൺകുട്ടികൾ ഉണ്ടെങ്കിൽ രണ്ട് അക്കൗണ്ട് എടുക്കാം. കൂടാതെ, ആദ്യത്തെയോ രണ്ടാമത്തെയോ പ്രസവത്തിൽ ഇരട്ട പെൺകുട്ടികളുണ്ടായാൽ ഒരു കുടുംബത്തിൽ മൂന്ന് അക്കൗണ്ട് എടുക്കാം.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\"><b style=\"background-color: transparent; color: rgb(0, 0, 0); font-family: Arial; font-size: 11pt; white-space: pre-wrap; letter-spacing: 0.2px;\">പലിശ നിരക്ക്</b><br></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\"><span style=\"font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">മറ്റ് ചെറു സമ്പാദ്യ പദ്ധതികളും പബ്ലിക് പ്രൊവിഡന്റ് ഫണ്ടും (പി.പി.എഫ്.) പോലെ സുകന്യ സമൃദ്ധി അക്കൗണ്ടിലെ പലിശനിരക്ക് ഓരോ പാദത്തിലും മാറ്റം വരും. ജൂലായ്-സെപ്റ്റംബർ കാലയളവിൽ ഇത് 8.1 ശതമാനമായിരുന്നു.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\"><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><b>നികുതി </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><span class=\"Apple-tab-span\" style=\"white-space:pre;\">	</span></span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><span class=\"Apple-tab-span\" style=\"white-space:pre;\">	</span></span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><span class=\"Apple-tab-span\" style=\"white-space:pre;\">	</span></span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><span class=\"Apple-tab-span\" style=\"white-space:pre;\">	</span></span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><span class=\"Apple-tab-span\" style=\"white-space:pre;\">	</span></span><span style=\"background-color: transparent; letter-spacing: 0.2px; font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"><span class=\"Apple-tab-span\" style=\"white-space:pre;\">	</span></span><span style=\"background-color: transparent; letter-spacing: 0.2px; color: rgb(0, 0, 0); font-family: Arial; font-size: 11pt; white-space: pre-wrap;\">ഈ പദ്ധതിയിൽ നിക്ഷേപിക്കുമ്പോൾ 1.50 ലക്ഷം രൂപയ്ക്കുവരെ 80 സി പ്രകാരം ആദായനികുതി ഇളവ് ലഭിക്കും. കാലാവധിയെത്തുമ്പോൾ പിൻവലിക്കുന്ന തുകയ്ക്ക് നികുതി നൽകേണ്ട. </span><br></p><br></span>', 'sukanya_samriddi_yojana_ssy1.jpg', NULL, 'ml', 0, 0, 11, NULL, 0, 0, 0, '2019-03-11 23:32:50', NULL),
(14, 'കേരള കോണ്‍ഗ്രസ് (എം) ചെയര്‍മാന്‍ കെ.എം.മാണി അന്തരിച്ചു', 'കേരള കോണ്‍ഗ്രസ് (എം) ചെയര്‍മാനും മുന്‍ മന്ത്രിയുമായ കെ.എം.മാണി (86) അന്തരിച്ചു. എറണാകുളത്തെ സ്വകാര്യ ആശുപത്രിയിലായിരുന്നു അന്ത്യം. ശ്വാസകോശ സംബന്ധമായ അസുഖത്തെ തുടര്‍ന്ന് തീവ്രപരിചരണ വിഭാഗത്തില്‍ കഴിയുകയായിരുന്നു.ഞായറാഴ്ചയാണ് മാണിയെ ആശുപത്രിയില്‍ പ്രവേശിപ്പിച്ചത്. കേരളത്തില്‍ ഏറ്റവും കൂടുതല്‍ കാലം ധനകാര്യ മന്ത്രിയായും നിയമസഭാ സാമാജികനായും പ്രവർത്തിച്ചയാളാണ് ശ്രീ കെ.എം.മാണി.<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ഏറ്റവും കൂടുതല്‍ കാലം മന്ത്രിസ്ഥാനം, എറ്റവും കൂടുതല്‍കാലം എംഎല്‍എ, കൂടുതല്‍ മന്ത്രിസഭകളില്‍ അംഗം, ഏറ്റവുംകൂടുതല്‍ ബജറ്റ് അവതരിപ്പിച്ച ധനമന്ത്രി, കൂടുതല്‍ കാലം ധനവകുപ്പും നിയമവകുപ്പും കൈകാര്യം ചെയ്തയാള്‍ തുടങ്ങിയ റെക്കോര്‍ഡുകളും മാണിയുടെ പേരിലുണ്ട്. 1964-ല്‍ പാലാ മണ്ഡലം രൂപീകരിച്ച് മുതല്‍ അവിടുത്തെ എംഎല്‍എയാണ് അദ്ദേഹം. ഒരേ മണ്ഡലത്തില്‍ നിന്ന് ഏറ്റവും കൂടുതല്‍ തവണ ജയിച്ച വ്യക്തിയും കൂടിയാണ് അദ്ദേഹം.', 'kmmani.jpg', NULL, 'Ch', 0, 0, 1, NULL, 0, 0, 0, '2019-04-09 05:57:43', NULL),
(15, 'കൈവല്യ സമഗ്ര തൊഴിൽ പുനരധിവാസ പദ്ധതി', '<div>എംപ്ലോയ്‌മെൻറ്&nbsp; എക്സ്ചേഞ്ചുകളിൽ പേര് രജിസ്റ്റർ ചെയ്തിട്ടുള്ള ഭിന്നശേഷിക്കാരായ തൊഴിലന്വേഷകർക്കായുള്ള സമഗ്ര തൊഴിൽ പുനരധിവാസ പദ്ധതിയാണ് കൈവല്യ. പദ്ധതിപ്രകാരം കുടുംബ വാർഷിക വരുമാനം രണ്ടു ലക്ഷം രൂപയിൽ താഴെയും 21 നും 55 നും&nbsp; ഇടയിൽ പ്രായമുള്ളവർക്ക് പരമാവധി അൻപതിനായിരം രൂപ വരെ വായ്പ ലഭിക്കുന്നതാണ്. വായ്‌പ തുകയുടെ 50% വരെ സബ്‌സിഡിയായി ലഭിക്കും. അപേക്ഷകർ എഴുത്തും വായനയും അറിയുന്നവരായിരിക്കണം.</div><div><br></div><div>അപേക്ഷാ ഫോം താഴെ നൽകിയിട്ടുള്ള ലിങ്കിൽ നിന്നും ഡൗൺലോഡ് ചെയ്യാവുന്നതാണ്.<br><a href=\"https://www.employmentkerala.gov.in/images/kerala/gallery/emppdffiles/Kaivalya_Application_Form.pdf\">https://www.employmentkerala.gov.in/images/kerala/gallery/emppdffiles/Kaivalya_Application_Form.pdf</a><br></div>', 'kaivalya.jpg', NULL, 'Ch', 0, 0, 11, NULL, 0, 0, 0, '2019-04-18 01:13:12', NULL),
(16, 'പ്രധാനമന്ത്രി ആവാസ് യോജന', '<h1 class=\"page-title\" style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; font-family: Noto, opensans, Arial, sans-serif; line-height: 1.3; color: rgb(0, 0, 0); overflow-wrap: break-word; letter-spacing: normal;\"><span property=\"schema:name\" class=\"field field--name-title field--type-string field--label-hidden\" style=\"\"><font size=\"3\">പ്രധാനമന്ത്രി ആവാസ് യോജന (ഗ്രാമീണ്‍)</font></span></h1><span style=\"color: rgb(0, 0, 0); font-family: Noto, opensans, Arial, sans-serif; letter-spacing: normal; text-align: justify;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2016-17 മുതല്‍ 2018-19 വരെയുള്ള 3 വര്‍ഷക്കാലയളവിനുള്ളില്‍ രാജ്യത്തെ ഗ്രാമീണ മേഖലയില്‍ ഭവനരഹിതരായവര്‍ക്കു വേണ്ടി ഒരു കോടി വീടുകള്‍ നിര്‍മ്മിക്കുന്നതിനുള്ള ബൃഹദ് പദ്ധതിയാണ് പ്രധാനമന്ത്രി ആവാസ് യോജന (ഗ്രാമീണ്‍) അഥവാ PMAY(G). 2011-ലെ സാമൂഹിക-സാമ്പത്തിക ജാതി സെന്‍സസില്‍ ഭവനരഹിതരായി കണ്ടെത്തിയവരെ ലക്ഷ്യം വെച്ചു കൊണ്ടുള്ള ഈ പദ്ധതിയില്‍ കേരളത്തില്‍ 2016-17 വര്‍ഷം 24341 വീടുകളാണ് ലക്ഷ്യമിട്ടിരിക്കുന്നത്. പദ്ധതിയില്‍ കേന്ദ്ര/സംസ്ഥാന ധനസഹായമായി 60:40 അനുപാതത്തില്‍ സമതലപ്രദേശങ്ങളില്‍ 120000/- രൂപയും ദുര്‍ഘടപ്രദേശങ്ങളില്‍ 130000/- രൂപയുമാണ് നല്‍കുന്നത്. ടി പദ്ധതി പ്രകാരം ഗുണഭോക്താക്കള്‍ക്ക് തുക നല്‍കുന്നത് പി.എഫ്.എം.എസ് മുഖേന ഗുണഭോക്താക്കളുടെ ബാങ്ക് അക്കൗണ്ടുകളിലേക്കാണ്. പ്രധാനമന്തി ആവാസ് യോജന (ഗ്രാമീണ്‍) പദ്ധതി പ്രകാരം വീടിനോടൊപ്പം ശുചിമുറി ഉള്‍പ്പെടെ നിശ്ചിത സമയത്തിനകം ഗുണമേന്മയുളള ഭവനങ്ങള്‍ നിര്‍മ്മിക്കുവാന്‍ ലക്ഷ്യമിടുന്നു. കേന്ദ്ര-സംസ്ഥാന സര്‍ക്കാര്‍ ധനസഹായങ്ങളും ത്രിതല പഞ്ചായത്തുകളുടെ പദ്ധതി വിഹിതവും ഉള്‍പ്പെടുത്തിയാണ് യൂണിറ്റ് കോസ്റ്റ് നിശ്ചയിക്കുന്നത്. വീട് നിര്‍മ്മാണം പൂര്‍ത്തീകരിക്കുന്നതിന് ബാങ്ക് വായ്പ ആവശ്യമുളളവര്‍ക്ക് 70000/- വായ്പ കുറഞ്ഞ പലിശ നിരക്കില്‍ ലഭിക്കുന്നു. സാമൂഹിക സാമ്പത്തിക ജാതി സെന്‍സസ് ഗുണഭോക്താക്കളുടെ ലിസ്റ്റുമായി ബന്ധപ്പെട്ട് പരാതികള്‍ക്കുളള പരിഹാരം കാണുവാന്‍ ജില്ലാ തലത്തില്‍ അപ്പലേറ്റ് കമ്മിറ്റികള്‍ രൂപീകരിച്ചിട്ടുണ്ട്. പദ്ധതി മിഷന്‍ മോഡലിലാണ് നടപ്പിലാക്കുന്നത്. ഇതിനായി സംസ്ഥാന ജില്ലാ ഗ്രാമതലങ്ങളില്‍ പ്രോജക്ട് മാനേജ്മെന്‍റ് യൂണിറ്റുകള്‍ പ്രവര്‍ത്തിക്കുന്നു.</span><div><div><span style=\"color: rgb(0, 0, 0); font-family: Noto, opensans, Arial, sans-serif; letter-spacing: normal; text-align: justify;\"><font size=\"3\"><br></font></span></div><div><span style=\"text-align: justify; letter-spacing: normal;\"><font color=\"#000000\" face=\"Noto, opensans, Arial, sans-serif\"><div style=\"\"><font size=\"3\">പ്രധാനമന്ത്രി ആവാസ് യോജന (നഗരം)</font></div><div style=\"\">&nbsp; &nbsp; &nbsp; &nbsp; 2022-ാം ആണ്ടോടെ ഭാരതത്തിന്‍റെ 75-ാം സ്വാതന്ത്ര്യ ദിനാഘോഷ വേളയില്‍ നഗരപ്രദേശത്തെ എല്ലാവര്‍ക്കും ഭവനം ലഭ്യമാകണം എന്ന ലക്ഷ്യത്തോടെ കേന്ദ്രസര്‍ക്കാര്‍ വിഭാവനം ചെയ്ത പദ്ധതിയാണ് പ്രധാനമന്ത്രി ആവാസ് യോജന (പി.എം.എ.വൈ) (നഗരം). 2015ല്‍ ഐ.എ.വൈ പദ്ധതി ലയിപ്പിച്ചാണ് പദ്ധതി പി.എം.എ.വൈ നടപ്പിലാക്കുന്നത്. നഗരപ്രദേശത്തെ എല്ലാവര്‍ക്കും പാര്‍പ്പിടങ്ങള്‍ ലഭ്യമാക്കുക എന്ന ലക്ഷ്യത്തോടെ കേന്ദ്ര ഭവന നഗരദാരിദ്ര്യ ലഘൂകരണ മന്ത്രാലയത്തിനു കീഴില്‍ സംസ്ഥാന സര്‍ക്കാരുമായി സംയോജിച്ചാണ് പദ്ധതി നടപ്പിലാക്കുന്നത്.&nbsp; ഈ പദ്ധതിയില്‍വീടില്ലാത്ത എല്ലാവരേയും ഗുണഭോക്താക്കളായി തെരഞ്ഞെടുക്കാവുന്നതും വാസയോഗ്യമല്ലാത്ത ഭവനങ്ങളെ വാസയോഗ്യമാക്കാവുന്നതുമാണ്. ചേരിവികസനം, ക്രെഡിറ്റ് ലിങ്ക്ഡ് സബ്സിഡി, അഫോര്‍ഡബിള്‍ ഹൌസിംഗ് സ്കീം, വ്യക്തിഗത നിര്‍മ്മാണം എന്നീ നാലു വ്യത്യസ്ത ഘടകങ്ങള്‍ സംയോജിപ്പിച്ചു കൊണ്ടാണ് പ്രസ്തുത പദ്ധതി വിഭാവനം ചെയ്തിരിക്കുന്നത്.&nbsp; സ്വന്തമായി സ്ഥലമുള്ള ഭവനരഹിതര്‍ക്ക് ഭവനനിര്‍മ്മാണത്തിന്/ വിപുലീകരണത്തിന് ധനസഹായം നല്‍കുന്ന പദ്ധതി&nbsp; ഘടകത്തിലുള്‍ പ്പെടുത്തി 7 നഗരസഭകളിലായി 8382 വീടുകള്‍ക്കാണ് അനുമതി ലഭിച്ചിട്ടുള്ളത്.&nbsp; 1.5 ലക്ഷം രൂപയാണ് കേന്ദ്രവിഹിതമായി ലഭിക്കുന്നത്.&nbsp; സംസ്ഥാന സര്‍ക്കാര്‍, നഗരസഭ-ഗുണഭോക്തൃ വിഹിതം ഉള്‍പ്പെടെ പരമാവധി 3 ലക്ഷം രൂപ ഓരോ ഗുണഭോക്താവിനും നല്‍കുന്നതാണ്. വിവിധ ഘട്ടങ്ങളിലായി സംസ്ഥാനത്തെ മുഴുവന്‍ നഗരസഭകളെയും പദ്ധതിയില്‍ ഉള്‍പ്പെടുത്തും.</div></font></span></div></div>', 'pmay-gramin-logo1.jpg', NULL, 'Ch', 0, 0, 11, NULL, 0, 0, 0, '2019-05-12 21:33:37', NULL),
(17, 'കടകംപള്ളി മിനി സിവില്‍ സ്റ്റേഷന്‍ യാഥാര്‍ത്ഥ്യമാകുന്നു', '<p data-header=\"3768822\" class=\"zw-paragraph heading3768822\" data-textformat=\"{&quot;ff&quot;:&quot;Meera&quot;,&quot;size&quot;:15}\" data-hd-info=\"3768822\" data_styles=\"{&quot;ff&quot;:&quot;Meera&quot;,&quot;size&quot;:15}\" data-doc-id=\"665035000003412542\" style=\"margin-bottom: 6pt; text-align: justify;\"><span style=\"font-family: Meera; font-size: 15pt;\">കടകംപള്ളി വില്ലേജിലെ&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">15.28</span><span style=\"font-family: Meera; font-size: 15pt;\"> സെന്റിലാണ് മിനി സിവിൽസ്റ്റേഷൻ നിർമിക്കുന്നത്.&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">5</span><span style=\"font-family: Meera; font-size: 15pt;\"> നിലകളിലായാണ് കെട്ടിടസമുച്ചയം ഉയരുക. ആകെ&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">11373</span><span style=\"font-family: Meera; font-size: 15pt;\"> ചതുരശ്ര അടി. ഗ്രൗണ്ട് ഫ്ളോറിലാണ് കടകംപള്ളി വില്ലേജ് ഓഫീസ് .&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">178.34</span><span style=\"font-family: Meera; font-size: 15pt;\"> ചതുരശ്ര മീറ്ററാണ് വില്ലേജ് ഓഫീസിനുവേണ്ടി ഉപയോഗിക്കുക. ഒന്നാംനിലയിൽ&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">201.95</span><span style=\"font-family: Meera; font-size: 15pt;\"> ചതുരശ്ര മീറ്ററിൽ ആനയറ കുടുംബക്ഷേമ ഉപകേന്ദ്രം</span><span style=\"font-family: Meera; font-size: 15pt;\">,&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">ഊളൻകുഴി കുടുംബക്ഷേമ ഉപകേന്ദ്രം</span><span style=\"font-family: Meera; font-size: 15pt;\">,&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">മിനി കോൺഫറൻസ് ഹാൾ എന്നിവയുണ്ടാകും. രണ്ടാം നിലയിലാണ് താലൂക്ക് സപ്ലൈ ഓഫീസ്. ഇതിനും&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">201.95</span><span style=\"font-family: Meera; font-size: 15pt;\"> ചതുരശ്ര മീറ്റർ സ്ഥലമുണ്ടാകും. മൂന്നും നാലും നിലയിലായി ജില്ലാ എംപ്ലോയ്‌മെന്റ് ഓഫീസ് പ്രവർത്തിക്കും.</span></p><span style=\"font-family: Meera; font-size: 15pt;\">5</span><span style=\"font-family: Meera; font-size: 15pt;\"> നിലയുള്ള കെട്ടിടസമുച്ചയത്തിന് പാർക്കിങ് സൗകര്യവും ഒരുക്കുന്നുണ്ട്.&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">11</span><span style=\"font-family: Meera; font-size: 15pt;\"> കാറുകളും സ്കൂട്ടറുകളും പാർക്ക് ചെയ്യാനാകും. മുറ്റത്ത് ഇന്റർലോക്ക് ബ്രിക്സുകള്‍ പാകും. രണ്ട് പ്രവേശനഗേറ്റുകളുണ്ടാകും. അംഗപരിമിതിയുള്ളവർക്കും ഭിന്നശേഷിക്കാർക്കും&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">പ്രവേശിക്കാൻ</span><span style=\"font-family: Meera; font-size: 15pt;\">&nbsp;&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">റാംപ് സൗകര്യവും ഉണ്ടാകും. ലിഫ്റ്റ് വഴി മുകൾനിലകളിൽ ബുദ്ധിമുട്ടില്ലാതെ പോകാനാകും. ടോയ്‌ലെറ്റ് സൗകര്യവും ലഭ്യമാക്കും. മിനി കോൺഫറൻസ് ഹാളിൽ സെമിനാറുകളും ചടങ്ങുകളും മറ്റും നടത്താനാകും. തടസ്സമില്ലാതെ വൈദ്യുതി ലഭിക്കാൻ ഡി ജി സെറ്റ് ഉണ്ടാകും. ഫയർഫൈറ്റിങ് സിസ്റ്റം</span><span style=\"font-family: Meera; font-size: 15pt;\">,&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">ഇടിമിന്നൽ രക്ഷാചാലകം</span><span style=\"font-family: Meera; font-size: 15pt;\">,&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">സോളാർ പാനൽ</span><span style=\"font-family: Meera; font-size: 15pt;\">,&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">മഴവെള്ള സംഭരണി തുടങ്ങിയവയും കടകംപള്ളി മിനി സിവിൽസ്റ്റേഷനിൽ സ്ഥാപിക്കും. തികച്ചും ആധുനിക മിനി സിവിൽസ്റ്റേഷൻ നിർമിക്കാൻ&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">2017 - 18</span><span style=\"font-family: Meera; font-size: 15pt;\"> ബജറ്റിൽ നിന്ന്&nbsp;</span><span style=\"font-family: Meera; font-size: 15pt;\">5</span><span style=\"font-family: Meera; font-size: 15pt;\"> കോടി രൂപയാണ് വകയിരുത്തിയത്. ഭവന നിർമാണ ബോർഡിനാണ് നിർമാണ ചുമതല.</span><br>', 'Mini_civil_station-19.jpg', NULL, 'Ch', 0, 0, 1, NULL, 0, 0, 0, '2019-05-13 06:20:59', NULL),
(18, 'വിദ്യാർത്ഥികൾക്കായി കരിയര്‍ ഗൈഡന്‍സ് വര്‍ക്ക്ഷോപ്പ്', '<div>ഇനിയെന്ത് പഠിക്കണമെന്ന ചോദ്യത്തിന് കുട്ടികള്‍ക്കും രക്ഷിതാക്കള്‍ക്കും ഉത്തരം നല്‍കുന്നതിനായി ആവിഷ്കരിച്ച പ്രകാശം കരിയര്‍ ഗൈഡന്‍സ് വര്‍ക്ക്ഷോപ്പ് കഴിഞ്ഞ 2 വര്‍ഷങ്ങളിലും&nbsp; വിജയകരമായി&nbsp; നടത്തി. കഴക്കൂട്ടം മണ്ഡലത്തില്‍ എസ്എസ്എല്‍സിക്കും ഹയര്‍സെക്കണ്ടറിക്കും വിജയിച്ച് ഉപരിപഠനത്തിന് അര്‍ഹരായ കുട്ടികളും രക്ഷിതാക്കളും പങ്കെടുത്ത കരിയര്‍ ഗൈഡന്‍സ് വര്‍ക്ക്ഷോപ്പ് നയിച്ചത് പ്രശസ്ത കരിയര്‍ ഗുരു ഡോ. പി.ആര്‍ വെങ്കിട്ടരാമനാണ്. കഴക്കൂട്ടം മണ്ഡലത്തിലെ കുട്ടികളുടെ വിദ്യാഭ്യാസ ഉന്നമനത്തിനായി തയ്യാറാക്കിയ പദ്ധതി മറ്റ് ജനപ്രതിനിധികളും മാതൃകയാക്കിയത് സന്തോഷകരമാണ്. ഈ കരിയര്‍ ഗൈഡന്‍സ് വര്‍ക്ക്ഷോപ്പുകള്‍ വഴി പുതിയ സാധ്യതകളിലൂടെ മുന്നേറാന്‍ നമ്മുടെ നാട്ടിലെ നൂറുകണക്കിന് കുട്ടികള്‍ക്ക് സാധിച്ചു കഴിഞ്ഞുവെന്നത് ആഹ്ളാദം പകരുന്നു.&nbsp;&nbsp;</div><div><br></div>', 'career_guidence.jpg', NULL, 'Ch', 132, 0, 1, NULL, 0, 0, 0, '2019-05-13 23:43:31', NULL),
(19, 'ഇനി സ്കൂളിലേക്ക് പോകാം സ്വന്തം ബസില്‍..', '<div>എംഎല്‍എയുടെ ആസ്തി വികസന ഫണ്ട് ചെലവഴിച്ച് 14 സ്കൂളുകള്‍ക്കായി 17 ബസുകള്‍ നല്‍കി. ഓട്ടോറിക്ഷയില്‍ ഞെരിഞ്ഞമര്‍ന്നും, ബസുകളില്‍ തിക്കിലും തിരക്കിലും പെട്ടുമൊക്കെ കഷ്ടപ്പെട്ടിരുന്ന സര്‍ക്കാര്‍ വിദ്യാലയങ്ങളിലെ കുട്ടികള്‍ക്ക് അവരുടെ സ്വന്തം സ്കൂള്‍ ബസില്‍ സ്കൂള്‍ മുറ്റത്ത് എത്താനാണ് ബസുകള്‍ വാങ്ങി നല്‍കിയത്. ഏതാനും ബസുകളില്‍ പരീക്ഷണാടിസ്ഥാനത്തില്‍ ജിപിഎസ് ട്രാക്കിംഗ് സിസ്റ്റവും സ്ഥാപിച്ചു.</div><div><br></div><div><b>സ്കൂള്‍ ബസുകള്‍ അനുവദിച്ച സ്കൂളുകളും, വകയിരുത്തിയ പണവും:</b><br></div><div><ul><li>ചന്തവിള ഗവ. യുപിഎസ് - 9 ലക്ഷം</li><li>വാഴവിള ഗവ. എല്‍പിഎസ് - 9 ലക്ഷം</li><li>കാട്ടായിക്കോണം ഗവ. യുപിഎസ് ( 2 ബസുകള്‍ ) - 22 ലക്ഷം</li><li>കഴക്കൂട്ടം ഗവ. എച്ച്എസ്എസ് - 17.67 ലക്ഷം</li><li>കുളത്തൂര്‍ ഗവ. എച്ച്.എസ്.എസ് ( 3 ബസുകള്‍) - 45.75 ലക്ഷം</li><li>ആറ്റിന്‍കുഴി ഗവ. എല്‍പിഎസ് - 11 ലക്ഷം</li><li>ഉള്ളൂര്‍ ഗവ. യുപിഎസ്- 11 ലക്ഷം</li><li>മെഡിക്കല്‍ കോളേജ്&nbsp; ഗവ. ഹൈസ്കൂള്‍ - 18.75 ലക്ഷം</li><li>ആനയറ വലിയ ഉദേശ്വരം ഗവ. എല്‍പിഎസ് - 11 ലക്ഷം</li><li>ചേങ്കോട്ടുകോണം ഗവ. എല്‍പിഎസ് - 10 ലക്ഷം</li><li>കാര്യവട്ടം ഗവ. യുപിഎസ് - 10 ലക്ഷം</li><li>ശ്രീകാര്യം ഗവ. എച്ച്എസ് -&nbsp; 15.32 ലക്ഷം</li><li>പള്ളിത്തുറ&nbsp; എച്ച്എസ്എസ് -&nbsp; 12 ലക്ഷം (ഐ.ഒ.സി സഹായം)</li><li>ചെമ്പഴന്തി ഗവ. എച്ച് എസ് - 17.67 ലക്ഷം (ഐ.ഒ.സി സഹായം)</li></ul></div><div><br></div>', 'schoolbus.jpg', NULL, 'Ch', 0, 0, 1, NULL, 5, 0, 1, '2019-05-14 01:25:55', NULL),
(20, 'പേട്ട-ആനയറ-വെണ്‍പാലവട്ടം  മാതൃകാപാതയാക്കുന്നു', 'പേട്ട-ആനയറ-വെണ്‍പാലവട്ടം റോഡ് വീതി കൂട്ടി നവീകരിക്കാനുള്ള 63.48 കോടി രൂപയുടെ പദ്ധതിക്ക് കിഫ്ബി അംഗീകാരം നല്‍കി. റോഡ് വികസനത്തിന് ഭൂമി ഏറ്റെടുക്കുന്നതിന് ഇതില്‍ നിന്നും 43.48 കോടി രൂപ വിനിയോഗിക്കും.&nbsp; പേട്ട -&nbsp; ആനയറ -&nbsp; വെണ്‍പാലവട്ടം ജംഗ്ഷന്‍ വരെയുള്ള 3 കിലോമീറ്റര്‍ ദൂരം 14 മീറ്റര്‍ വീതിയില്‍ വികസിപ്പിക്കും. വെണ്‍പാലവട്ടം മുതല്‍ ഒരുവാതില്‍കോട്ട ബൈപാസ് വരെയുള്ള റോഡിന് 12 മീറ്റര്‍ വീതിയുമുണ്ടാകും.&nbsp; ഈ പ്രദേശത്തിന്റെയാകെ മുഖഛായ തന്നെ മാറ്റുന്നതിനും ഗതാഗത കുരുക്ക് ഒഴിവാക്കാനും അപകട സാധ്യത കുറയ്ക്കാനും ഉന്നത നിലവാരത്തില്‍ നിര്‍മ്മിച്ച് വീതി കൂട്ടുന്ന റോഡ് വഴി സാധിക്കും.<br>', 'anayarabridgeroad1.jpg', NULL, 'Ch', 0, 0, 1, NULL, 0, 0, 0, '2019-05-14 02:03:37', NULL),
(27, 'പാര്‍വതീ പുത്തനാര്‍ പുനര്‍ജനിക്കുന്നു..', 'തലസ്ഥാന ജില്ലയിലെ ചരിത്രപ്രാധാന്യമുള്ള പാര്‍വതീ പുത്തനാര്‍ മാലിന്യവാഹിനിയായി മാറിയിട്ട് പതിറ്റാണ്ടുകളായി. ആ പാര്‍വതീ പുത്തനാറിനെ ശുചീകരിച്ച് ജലഗതാഗത യോഗ്യമാക്കുന്നതിനുള്ള പദ്ധതിക്ക് സര്‍ക്കാര്‍ തുടക്കം കുറിച്ചു. 53 വര്‍ഷങ്ങള്‍ക്ക് ശേഷം ആദ്യമായാണ് പാര്‍വതി പുത്തനാറിനെ സമഗ്രമായി ശുചീകരിച്ച് ഗതാഗതയോഗ്യമാക്കാനുള്ള പ്രവൃത്തി നടത്തുന്നത്. മുഖ്യമന്ത്രി പിണറായി വിജയനും, മന്ത്രി കടകംപള്ളി സുരേന്ദ്രനും, ജലവിഭവ വകുപ്പ് മന്ത്രിയും നേരിട്ട് തന്നെ പാര്‍വതീ പുത്തനാറിലെ ശുചീകരണ പ്രവര്‍ത്തനങ്ങളുടെ പുരോഗതി വിവിധ ഘട്ടങ്ങളിലായി പരിശോധിച്ച്&nbsp; നേതൃത്വം നല്‍കുകയാണ്. കോവളം മുതല്‍&nbsp; കാസര്‍കോട് ഹോസ്ദുര്‍ഗ് വരെ 590 കിലോമീറ്റര്‍ നീളുന്ന ഉള്‍നാടന്‍ ജലഗതാഗത പാത പുന:സൃഷ്ടിക്കാനാണ്&nbsp; സര്‍ക്കാര്‍ തീരുമാനിച്ചിരിക്കുന്നത്.<br>', 'parvathyputhanar.jpg', NULL, '', 0, 0, 1, NULL, 1, 0, 1, '2019-05-15 00:53:43', NULL),
(28, 'ചെമ്പഴന്തി ഗുരുകുലത്തിന് ആധുനിക കണ്‍വെന്‍ഷന്‍ സെന്റര്‍', '<div>ശ്രീനാരായണ ഗുരുവിന്റെ ജന്മസ്ഥലമായ&nbsp; ചെമ്പഴന്തിയിലെ ശ്രീനാരായണ ഗുരുകുലത്തില്‍ സംസ്ഥാന ടൂറിസം വകുപ്പ് 10 കോടി രൂപ ചെലവില്‍ അത്യാധുനിക കണ്‍വെന്‍ഷന്‍ സെന്റര്‍ നിര്‍മ്മിക്കുന്നു. രണ്ട് നിലകളിലായി 23622 സ്ക്വയര്‍&nbsp; ഫീറ്റ് വിസ്തൃതിയുള്ള മന്ദിരം ഒഡീഷയിലെ ക്ഷേത്ര സ്തൂപ മാതൃകയിലാണ് രൂപകല്‍പ്പന ചെയ്തിട്ടുള്ളത്. 15751 സ്ക്വയര്‍ ഫീറ്റ് വിസ്തൃതിയുള്ള താഴത്തെ നിലയില്‍ ഒരേ സമയം ആയിരത്തിലധികം പേര്‍ക്ക് ഇരിക്കാനുള്ള സൗകര്യമുണ്ടാകും. പുറത്തു നിന്ന് കാറ്റും വെളിച്ചവും കടക്കുന്ന രീതിയിലുള്ള ഈ കണ്‍വെന്‍ഷന്‍ സെന്ററിന്റെ മുറ്റത്ത് നിന്നും കൂടി ദൃശ്യമാകുന്ന രീതിയിലാണ് വേദി തീര്‍ക്കുന്നത്. ഓഫീസ്, ഗ്രീന്‍ റൂം, സ്റ്റോര്‍, അടുക്കള, ടോയ് ലെറ്റുകള്‍ എന്നിവയും താഴത്തെ നിലയില്‍ ഉണ്ടാകും. മുകളിലത്തെ നിലയില്‍ ശ്രീനാരായണ ഗുരുവിന്റെ ജീവിതവും സന്ദേശങ്ങളും ഉള്‍ക്കൊള്ളുന്ന ഡിജിറ്റല്‍ മ്യൂസിയം സ്ഥാപിക്കും. മ്യൂസിയത്തിലെ 4 ഹാളുകളിലായി ഗുരുവിന്റെ കുട്ടിക്കാലം മുതലുള്ള നാല് വ്യത്യസ്ത ജീവിത കാലയളവുമായി ബന്ധപ്പെട്ട വിശദാംശങ്ങള്‍ മള്‍ട്ടിമീഡിയ സംവിധാനത്തിലൂടെ അവതരിപ്പിക്കും.</div><div><br></div>', 'gurukulam_chempazhanthy.jpg', NULL, '', 0, 0, 1, NULL, 0, 0, 0, '2019-05-15 01:08:40', NULL),
(29, 'നവോത്ഥാന നായകരുടെ സംഗമഭൂമിയായ അണിയൂര്‍ ഇനി തീര്‍ത്ഥാടന ടൂറിസം കേന്ദ്രം', 'കേരളത്തിലെ നവോത്ഥാന പോരാട്ടങ്ങള്‍ക്ക് കരുത്ത് പകര്‍ന്ന ശ്രീനാരായണ ഗുരുവും, ചട്ടമ്പി സ്വാമികളും തമ്മില്‍ ആദ്യ കൂടിക്കാഴ്ച നടന്ന ചെമ്പഴന്തി അണിയൂര്‍ ശ്രീ ദുര്‍ഗാ ഭഗവതി ക്ഷേത്രത്തിന് ചരിത്രപ്രാധാന്യമേറെയാണ്. 1883 ല്‍ നടന്ന ആ നിര്‍ണായകമായ കൂടിക്കാഴ്ചയുടെ ചരിത്രസ്മരണകളുണര്‍ത്തി ഇവിടം തീര്‍ത്ഥാടക ടൂറിസം കേന്ദ്രമാക്കി മാറ്റുന്നതിനുള്ള പദ്ധതി യാഥാര്‍ത്ഥ്യമായി. മൂന്ന് കോടി രൂപ വിനിയോഗിച്ചാണ് സംസ്ഥാന ടൂറിസം വകുപ്പ് അണിയൂര്‍ ക്ഷേത്രത്തില്‍ പില്‍ഗ്രിം അമിനിറ്റി സെന്റര്‍ ഉള്‍പ്പെടെയുള്ള വികസന പ്രവര്‍ത്തനങ്ങള്‍ പൂര്‍ത്തിയാക്കിയത്. ആധുനിക സൗകര്യങ്ങളോടെയുള്ള 6180 ചതുരശ്ര അടി വിസ്തീര്‍ണമുള്ള പില്‍ഗ്രിം അമിനിറ്റി സെന്റര്‍ നിര്‍മ്മിച്ചതിന് പുറമെ ക്ഷേത്രക്കുളം നവീകരണവും, അനുബന്ധ നിര്‍മ്മാണ പ്രവര്‍ത്തനങ്ങളും നടത്തി. വിദ്യാര്‍ത്ഥികള്‍ക്ക് ചരിത്രപഠനത്തിനായി ആധുനിക ലൈബ്രറിയും ഇവിടെ ഒരുക്കിയിട്ടുണ്ട്. ഐക്യരാഷ്ട്രസഭാ പുരസ്കാരത്തിന് അര്‍ഹമായ ഊരാളുങ്കല്‍ ലേബര്‍ കോണ്‍ട്രാക്ട് സഹകരണ സംഘമാണ് നിര്‍മ്മാണ പ്രവര്‍ത്തനങ്ങള്‍ നടത്തിയത്.<br>', 'Aniyoor_temple.jpg', NULL, '', 0, 0, 1, NULL, 1, 0, 1, '2019-05-16 04:44:33', NULL),
(30, 'കൈത്താങ്ങുമായി കെയര്‍ഹോം പദ്ധതി', '<div>2018 ആഗസ്റ്റ് മാസം സംസ്ഥാനം നേരിട്ട പ്രകൃതി ദുരന്തത്തെ നേരിടുന്നതിന് ആശ്വാസവും കൈത്താങ്ങുമായി സഹകരണമേഖല മുന്‍പന്തിയില്‍ നിന്നു. ദുരിതബാധിതരെ സഹായിക്കുന്നതിനായി CARe Kerala (Co-operative Alliance to Rebuild Kerala) എന്ന പദ്ധതി പ്രഖ്യാപിച്ചു. പ്രളയത്തില്‍ സമ്പൂര്‍ണ്ണമായും വീട് നഷ്ടപ്പെട്ട 2000 കുടുംബങ്ങള്‍ക്ക് 5 ലക്ഷം രൂപ വീതം ചെലവ് ചെയ്ത് പുതിയ വീടുകള്‍ നിര്‍മ്മിച്ചു നല്‍കുന്ന Care Home പദ്ധതി പ്രവര്‍ത്തനം സംസ്ഥാനത്തെ സഹകരണ മേഖലയിലൂടെ ത്വരിതഗതിയില്‍ നടന്നു വരുന്നു. 2019 ഫെബ്രുവരി- മാര്‍ച്ച് മാസങ്ങളില്‍ ഈ വീടുകള്‍ ഗുണഭോക്താക്കള്‍ക്ക് കൈമാറാന്‍ കഴിയും. ഒന്നാം ഘട്ടം 2000 വീടുകള്‍ നിര്‍മ്മിച്ചു നല്‍കിയതിനുശേഷം രണ്ടാംഘട്ടമായി 2000 വീടുകള്‍ കൂടെ നിര്‍മ്മിച്ചു നല്‍കാനുള്ള പദ്ധതി ആസൂത്രണം ചെയ്തു വരുന്നു.</div><div><br></div>', 'carehome.jpg', NULL, '', 0, 0, 1, NULL, 0, 0, 0, '2019-05-16 04:58:59', NULL),
(31, 'മുന്‍മന്ത്രി കടവൂര്‍ ശിവദാസന്‍ അന്തരിച്ചു.', '<div>മുതിര്‍ന്ന കോണ്‍ഗ്രസ് നേതാവും മുന്‍മന്ത്രിയുമായിരുന്ന കടവൂര്‍ ശിവദാസന്‍ അന്തരിച്ചു. 87 വയസ്സായിരുന്നു. വെള്ളിയാഴ്ച പുലര്‍ച്ചെ തിരുവനന്തപുരത്തെ സ്വകാര്യ ആശുപത്രിയില്‍ വച്ചായിരുന്നു അന്ത്യം. കൊല്ലം ഡിസിസിയിലും തുടര്‍ന്ന് ആനന്ദവല്ലീശ്വരത്തെ വീട്ടിലും പൊതുദര്‍ശനത്തിന് വയ്ക്കും. തുടര്‍ന്ന് സംസ്‌ക്കാരം വൈകിട്ട് നാലിന് മുളങ്കാടം ശ്മശാനത്തില്‍ നടക്കും.&nbsp;</div><div>&nbsp; &nbsp; &nbsp; &nbsp; കെ. കരുണാകരന്‍, എ.കെ. ആന്റണി മന്ത്രിസഭയിലും അംഗമായിരുന്നു. നിയമസഭാംഗമായ അഞ്ചു വട്ടത്തില്‍ നാല് തവണയും മന്ത്രിയായി. വൈദ്യുതി, തൊഴില്‍, വനം, എക്‌സൈസ്, ആരോഗ്യ വകുപ്പുകൾ കൈകാര്യം ചെയ്‌തിട്ടുണ്ട്‌.</div>', 'kadavur-sivadasan.jpg', NULL, '', 0, 0, 1, NULL, 1, 0, 1, '2019-05-16 21:22:22', NULL),
(32, 'കുഞ്ഞാങ്ങള ചത്താലും വേണ്ടില്ല നാത്തൂന്റെ കരച്ചിൽ കണ്ടാൽ മതി മുല്ലപ്പള്ളിക്ക് - എം. എം. മണി ', 'പാർലമെന്റ് തെരഞ്ഞെടുപ്പിൽ കേരളത്തിൽ യു.ഡി.എഫിന് മെച്ചപ്പെട്ട ഫലം കിട്ടുമെന്ന ഒരടിസ്ഥാനവുമില്ലാത്ത സർവ്വേ റിപ്പോർട്ട് കണ്ട് ആവേശഭരിതനായി തുള്ളിച്ചാടുകയാണ് കെ.പി.സി.സി. പ്രസിഡന്റ് മുല്ലപ്പള്ളി രാമചന്ദ്രൻ. അതേ അവസരത്തിൽ അഖിലേന്ത്യ തലത്തിൽ വന്ന എല്ലാ സർവ്വേ റിപ്പോർട്ടുകളും പറയുന്നത് ബി.ജെ.പി. വീണ്ടും അധികാരത്തിൽ വരുമെന്നും കോൺഗ്രസ് തറപറ്റുമെന്നുമാണ്. അതിൽ കെ.പി.സിസി. അധ്യക്ഷന് ഒരു പ്രയാസവുമില്ല. എൽ.ഡി. എഫിന് സീറ്റ് കുറയുമെന്ന സർവ്വേ റിപ്പോർട്ടിലാണ് പുള്ളിക്കാരന് സന്തോഷം. മുല്ലപ്പള്ളിയുടെ ഈയൊരവസ്ഥ എന്തൊരു ഗതികേടാണ്. \"കുഞ്ഞാങ്ങള ചത്താലും വേണ്ടില്ല നാത്തൂന്റെ കരച്ചിൽ കണ്ടാൽ മതി\" എന്ന മനോഭാവമാണ് മുല്ലപ്പള്ളിക്ക്.<br>', 'mm_mani_pic.jpg', NULL, '', 0, 0, 10, NULL, 0, 0, 0, '2019-05-21 23:57:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news_image`
--

CREATE TABLE `news_image` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_log`
--

CREATE TABLE `news_log` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `up` int(11) NOT NULL,
  `down` int(11) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_log`
--

INSERT INTO `news_log` (`id`, `news_id`, `user_id`, `up`, `down`, `datetime`) VALUES
(7, 6, 6, 1, 0, '2019-02-14 09:41:27'),
(8, 9, 13, 1, 0, '2019-02-14 10:29:49'),
(11, 10, 13, 1, 0, '2019-02-14 13:44:19'),
(12, 10, 18, 1, 0, '2019-02-14 13:44:53'),
(16, 10, 21, 1, 0, '2019-02-15 07:46:41'),
(40, 9, 6, 1, 0, '2019-02-15 11:16:07'),
(42, 4, 6, 1, 0, '2019-02-15 11:16:15'),
(43, 3, 6, 1, 0, '2019-02-15 11:16:20'),
(44, 2, 6, 1, 0, '2019-02-15 11:16:24'),
(46, 10, 17, 1, 0, '2019-02-15 11:17:19'),
(47, 8, 6, 1, 0, '2019-02-16 11:21:25'),
(49, 10, 26, 1, 0, '2019-02-20 07:46:09'),
(51, 10, 6, 1, 0, '2019-03-06 05:50:19'),
(52, 2, 29, 1, 0, '2019-03-18 02:42:42'),
(53, 19, 17, 1, 0, '2019-05-14 08:26:43'),
(54, 19, 30, 1, 0, '2019-05-14 08:27:36'),
(55, 19, 39, 1, 0, '2019-05-14 08:28:21'),
(56, 19, 38, 1, 0, '2019-05-14 08:28:55'),
(57, 27, 42, 1, 0, '2019-05-15 07:58:12'),
(59, 29, 46, 1, 0, '2019-05-20 09:05:53'),
(61, 31, 48, 1, 0, '2019-05-21 05:37:29'),
(62, 19, 48, 1, 0, '2019-05-21 05:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `public_user`
--

CREATE TABLE `public_user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `login_id` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `gender` char(1) NOT NULL,
  `email` varchar(50) NOT NULL,
  `constituency_id` int(11) NOT NULL,
  `mla_id` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `role` varchar(6) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `address` text,
  `status` tinyint(1) DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `language` char(2) DEFAULT NULL,
  `theme` varchar(10) NOT NULL DEFAULT 'dark',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` varchar(20) NOT NULL,
  `amentedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amentedBy` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `public_user`
--

INSERT INTO `public_user` (`id`, `fullname`, `username`, `login_id`, `password`, `gender`, `email`, `constituency_id`, `mla_id`, `image`, `role`, `phone`, `address`, `status`, `token`, `location`, `language`, `theme`, `createdAt`, `createdBy`, `amentedAt`, `amentedBy`, `last_login`) VALUES
(6, 'Chandu J S', 'chandu', 'chandu', 'cGFzc3dvcmQ=', 'm', 'chandu@inometriscs.com', 133, 133, 'IMG_1548318791.jpg', 'public', '9497851381', 'Long Addresss, Street, District, Country, State', 0, '863e6d3c6eb42f2dcd6c19af8b7b8092a98ab3e6d9f9990c91ce79087cf37c03367b27f1339c77fbb22d433dc9ec7d9cfa6243f44175e20009f1568011d881fd', '17.04584, 18.1635265', 'en', 'light', '2019-01-14 11:30:59', '', '2019-01-14 11:30:59', '', '2019-04-01 06:19:36'),
(7, 'aagt', 'fgaba', 'fgaba', 'YmFoYW5h', 'm', 'ffff@ddd.cc', 128, 128, '', 'fgaba', '88888', '', 0, 'fff0ae0edab2406944fa276865d4a710a679b4f7ad108ffd79d82bb4415575d3b322a83bd93e9bceb2d067d8e2b00a75b35154e59a9a335a2d17cb50e87e2bd3', '', 'en', 'light', '2019-01-30 07:39:55', '', '2019-01-30 07:39:55', '', '2019-01-30 14:39:55'),
(8, 'Chindukrishna', 'chindukrishna', 'chindukrishna', 'YWJpc2FjaGlu', 'm', 'chindukrishna@gmail.com', 134, 134, '', 'chindu', '9847540368', '', 0, 'b9009de392364f6057ad64600fb70493faceb529ce20e5e002b1ae340076ac1caa25d2ae861e4a24a07c32cadfa197fd640ec3fb7be894f35dca5b769ca345bc', '', 'en', 'light', '2019-02-04 21:49:49', '', '2019-02-04 21:49:49', '', '2019-02-05 04:49:49'),
(9, 'preethavm', 'preethavm', 'preethavm', 'S3Jpc2huYUAxOTg0', 'f', 'preethavm1984@gmail.com', 76, 76, '', 'preeth', '9846039671', '', 0, '85c0617206cf583f4f2e6598c39533e04bc0fdf4df48db49e7863fa0212d26bcc0a3bf3c3c690d1614423481756303ca8462f9224be5394dc5cc107221b067c5', '', 'en', 'light', '2019-02-04 21:58:05', '', '2019-02-04 21:58:05', '', '2019-02-15 07:39:27'),
(10, 'krishna ', 'krishna', 'krishna', 'MTIz', 'm', 'krishna@inometrics.com', 134, 134, '', 'krishn', '9847540368', '', 0, '092983e82661cca219584ce6d218b120163ef86cb9fb1ad73cea9f9073222e321452af24e4f7bbefadd8a558a28a0e0e0760c39d96781f34237ced644dfcabd3', '', 'ml', 'light', '2019-02-05 01:34:34', '', '2019-02-05 01:34:34', '', '2019-02-05 08:34:37'),
(11, 'Syam', 'Syam', 'Syam', 'cmV2YXRoaQ==', 'm', 'syam@inometrics.com', 133, 133, '', 'Syam', '9497360241', '', 0, '8d27232434ef425c82b13f8d6bac63a8fb97a9f2da816759a20be59be2e5389044db9a7956303b8558fac8fca040c9eb9804d145779a0d2a3a4f93bf6d22b4fc', '', 'ml', 'light', '2019-02-05 07:02:39', '', '2019-02-05 07:02:39', '', '2019-02-05 14:02:39'),
(12, 'test', 'inometrics ', 'inometrics ', 'MTIzNDU2', 'm', 'chindu.krishna@gmail.com', 134, 134, '', 'inomet', '9847540368', '', 0, '3e2b59736e0c1bddc7ea303b0421707d357f5c1e41bce0413eec0f88a7182479b40645467c0c5295d61d5d5e581ac2e5056737a27f9eb6b3b4e039f5f1848a60', '', 'en', 'light', '2019-02-12 21:56:21', '', '2019-02-12 21:56:21', '', '2019-05-17 12:33:15'),
(13, 'Lekshmi Vasanth', '9048649024', '9048649024', 'OTc0NTA1NzUwOA==', 'f', 'lekshmivasanth@gmail.com', 133, 133, '', '904864', '9048649024', '', 0, 'a5119c8603bc33d23489983b808aa7c9f25439d04f885737670d403df7cdec51292cb06a2a0596163c444ac8289b58c26d3caa10ee872d163978030e47c25447', '', 'en', 'light', '2019-02-12 21:59:10', '', '2019-02-12 21:59:10', '', '2019-02-15 06:47:38'),
(14, 'yshaha', 'sasi', 'sasi', 'YnNoYXU=', 'm', '', 105, 105, '', 'sasi', '616161', '', 0, '9b4d1509a1fd4e019b669d0a94dbdb0c176a103b2f93d9f9441bf615cc40bed0de7cfd09d55466a5f2638ff7b190dd69740ffb584545cb565caf737ef1887779', '', 'en', 'light', '2019-02-12 23:10:30', '', '2019-02-12 23:10:30', '', '2019-02-13 06:10:30'),
(15, 'yajaja', 'wgab', 'wgab', 'Z2I=', 'f', 'shhsba@jsjs.zoms', 113, 113, '', 'wgab', '49464', '', 0, '55238fd046c07a47b68658e9133482edd13cba1ae28e6515ff43c14e10d8cfecb0b3d2a504f30a0eae565b5143debef3bf8c001588c04c712f2d609e48b094be', '', 'en', 'light', '2019-02-14 00:31:29', '', '2019-02-14 00:31:29', '', '2019-02-14 07:31:29'),
(16, 'Admin', 'association', 'association', 'cGFzc3dvcmQ=', 'm', 'syam@inometrics.com', 134, 134, '', 'public', '9497360241', 'Inometrics', 0, '461fe824ffb3ef67e9f964c17343d79c3958fd39c341f74b9710c64b4955c6f3190f1fc64389b979b14a31b987a39c148392253c44e271335f9228731d6bf994', '', 'en', 'dark', '2019-02-14 00:35:43', '', '2019-02-14 00:35:43', '', '2019-02-14 11:40:26'),
(17, 'test', 'inometrics2', 'inometrics2', 'MTIzNDU2', 'm', 'test@gmail.com', 133, 133, '', 'public', '9847540368', 'addr', 0, 'ebb7094b655c5beeedb743f8f0c522e2dc828e74a1c849d04c74b49f3b73eca9ceca042ebcdc12750786adb5193a4c32a2c9f747a274edd432015defbe47b0f1', '', 'en', 'light', '2019-02-14 04:41:26', '', '2019-02-14 04:41:26', '', '2019-05-21 05:44:56'),
(18, 'Syam', 'admin', 'admin', 'cGFzc3dvcmQ=', 'm', 'test @gmail.com', 133, 133, '', 'public', '9497360241', 'Aaaa', 0, '47f25c75fda0b84d738ffa035c5f5eb03586535c27b429229e5d5e4147387c501760de449ff0dbe2607d069a51f94c53f44e70abe27d9da39f7f64e0a2c4576b', '', 'en', 'light', '2019-02-14 04:44:47', '', '2019-02-14 04:44:47', '', '2019-02-18 05:32:18'),
(19, 'vjay', 'Vishnu', 'Vishnu', 'dGVzdDEyMw==', 'm', 'vishnu@xilligence.com', 132, 132, '', 'Vishnu', '8089801344', '', 0, '1b19e5566a8171aee05c6a466b85b024f9e0a8e9ee49f5059bd3e9cf6476c2cebfe42f26e5499c067360db9a3e7e04266ee5912b9f9b9812b1142b23c5e6836d', '', 'en', 'dark', '2019-02-14 09:44:33', '', '2019-02-14 09:44:33', '', '2019-02-14 16:44:34'),
(20, 'Reji', 'reji', 'reji', 'cGFzc3dvcmQ=', 'm', '', 115, 115, '', 'reji', '9497360241', '', 0, 'b54d35ea54bb1f895c38bccd2801c3b547bc27a8b69c58bc123abb1ae65ac5b94d085e7f64d5787e5d9878e79e66e062f26520807f3827329c1a6c1d4a6f258b', '', 'en', 'dark', '2019-02-14 23:37:58', '', '2019-02-14 23:37:58', '', '2019-02-15 06:37:59'),
(21, 'test', 'test4', 'test4', 'dGVzdDEyMw==', 'f', 'test1234@gmail.com', 133, 133, '', 'test4', '9846039685', '', 0, 'cd102b933921f7c940ea50e5600c0a1ab20bee4a5b571a50b17b243d06ea0bfc2c4c330ff11029162e876518cbd5ce334b4d53356f894bb66af2b64dce6467d2', '', 'en', 'light', '2019-02-15 00:41:38', '', '2019-02-15 00:41:38', '', '2019-02-15 07:41:39'),
(22, 'Azeem', 'azimsai', 'azimsai', 'ZHVhbDE5OTM=', 'm', 'azimsai@gmail.com', 133, 133, '', 'azimsa', '9567255149', '', 0, '523c77162d83148027704f0c00fb59656eb18af00cbf44039fbc0be403e28b9b8b8638dc037d8fec78ab1601d50445eceeaaa1a20562b2001c3c86ef1b768359', '', 'en', 'dark', '2019-02-15 07:10:51', '', '2019-02-15 07:10:51', '', '2019-02-15 14:10:52'),
(23, 'Priya Krishna', 'Priyakrishna', 'Priyakrishna', 'Q2hpbmR1QDI2NjEq', 'f', 'priyamaharajan.in@gmail.com', 133, 133, '', 'Priyak', '9894718514', '', 0, 'c6dd8903ecfbde13a560aad6aab5c5f36b0bdd4c044e1c87851d6c349419afeb889c1a8ba02f566b6bc9253dafbea00fcaa71a71ea6fc0a5834e7f39961b585c', '', 'en', 'light', '2019-02-17 17:52:53', '', '2019-02-17 17:52:53', '', '2019-02-18 00:52:54'),
(24, 'rajagopal', 'prasoon', 'prasoon', 'cmFqYWdvcGFs', 'm', 'vistaad@gmail.com', 133, 133, '', 'prasoo', '9895011555', '', 0, '408d220a509b55c1942939c7098f2c5c756b80c8f5c560df7062d2801e4e7e76857a6ae8193ad083f1531d65be87c4df9df984839899b337d85aa4cccce820e3', '', 'en', 'light', '2019-02-17 23:08:47', '', '2019-02-17 23:08:47', '', '2019-02-18 06:08:48'),
(25, 'paul p', 'paul', 'paul', 'MjI2Mg==', 'm', '', 91, 91, '', 'paul', '9446400333', '', 0, '502463ce50d9f53a72390fe626de3cb205c02f2d879fa61a67b537ca45b49f39606a792ebe707069644104ec2bae0a0731a21b23c7626965701faa7d91cd979c', '', 'en', 'light', '2019-02-18 02:41:25', '', '2019-02-18 02:41:25', '', '2019-02-18 09:41:25'),
(26, 'Sreekutty Arun', 'sreekutty', 'sreekutty', 'cGFzc3dvcmQ=', 'f', '', 133, 133, '', 'sreeku', '7736169430', '', 0, '20d7b779cefb574d6c5854f9e1c6b33e37a2899fa085a6818500bef78af89144eace4e0ee96c8dc6dbd09f9676c4c948be5b2e392cab51538defd33d8dd2d3b9', '', 'en', 'light', '2019-02-18 19:24:33', '', '2019-02-18 19:24:33', '', '2019-02-19 02:24:34'),
(27, 'vishnu', 'vishnuvjay', 'vishnuvjay', 'YWRtaW44OTg=', 'm', 'vishnu7mgc@gmail.com', 130, 130, '', 'vishnu', '9020434374', '', 0, '4952c5ee2932ade54e56195440c23429962e2d1c3d8e5e627208eb7bb22087f288b54f7a76dc7aeff0fdbe1fe16257e256023a6f6714a683b1a533ad5a02f8dc', '', 'en', 'light', '2019-03-04 03:45:18', '', '2019-03-04 03:45:18', '', '2019-03-04 10:47:12'),
(28, 'Deepak Divakaran ', 'Deepak ', 'Deepak ', 'dmVzcGVyNDE5Ng==', 'm', 'deepak.jagathy@gmail.com', 132, 132, '', 'Deepak', '7907817177', '', 0, 'a85cded3559a4e76aa02d7e0a5b7348158cf27fcb7fbdaa5c9c42deb43f743fb69942cf89a56cffda9ab5027be788fa109c33e52e3083536239b339f747f6ea6', '', 'en', 'light', '2019-03-13 01:04:58', '', '2019-03-13 01:04:58', '', '2019-03-13 08:04:58'),
(29, 'Chethan ', 'kalyan ', 'kalyan ', 'MTIzNDU2Nzg5', 'f', 'www.anjin.k', 136, 136, '', 'kalyan', '9964942686', '', 0, '4b408e1d52631d77e6f7634cd7f20f0874184391d68dda1c9160a67f9e880ba5b64102be85753c37c146c7757d99f1e366ca1c58f7ebe2545d44dd8bb387a875', '', 'en', 'light', '2019-03-17 19:42:13', '', '2019-03-17 19:42:13', '', '2019-03-18 02:42:13'),
(30, 'Syam', 'tk', 'tk', 'dGt0aw==', 'm', 'syam@inometrics.com', 125, 125, '', 'tk', '9497360241', '', 0, 'a1cae018f5de96093b7c076ddab79190baabd6086058a14b158ad6e3fcb34a494b3ddb8ea98765673f771017495d242006d971224c16264027f99287e224af2e', '', 'en', 'light', '2019-03-24 23:10:22', '', '2019-03-24 23:10:22', '', '2019-03-25 06:10:23'),
(31, 'Joy', 'joy', 'joy', 'am95', 'm', 'joy.varkala@gmail.com', 127, 127, '', 'joy', '9142741589', '', 0, '7663aa08d63418da46a8110aa8c0a56419eb33f5109db3c5c70418de7dd164d48673c080efbeffdf5ab924aead558a5258d0bc18d263f9d8b4bdcc00eebb1fc0', '', 'en', 'light', '2019-03-26 07:25:53', '', '2019-03-26 07:25:53', '', '2019-03-26 14:25:54'),
(32, 'Krishnadas', 'creativekd', 'creativekd', 'S2RAamFuYXRoYTk5', 'm', 'creativekrishnadas@gmail.com', 60, 60, '', 'creati', '9496741048', '', 0, '8953f958aa1db386ab4eb1a51b9ea8f9c53bd2a16fd6db547527b34175d5b83a712d46c400c0e7f709f4d1814422574492a88a1f16569d00145ad156fc25269c', '', 'en', 'light', '2019-03-26 23:38:55', '', '2019-03-26 23:38:55', '', '2019-03-27 06:38:56'),
(33, 'Roshy Augustine', 'Roshy Augustine', 'Roshy Augustine', 'QXZlbWFyaWFAMyM=', 'm', 'roshymla@gmail.com', 91, 91, '', 'Roshy ', '9995953450', '', 0, '000f8e9c3c5e0813a46f8bb6b42b7987c4fb1cca6d7a463ee45b7900710d22862ec119000d75e2f8f0e8bd9aeff45c18b54a1f676a211c1733923d1142acad54', '', 'en', 'light', '2019-04-05 00:12:35', '', '2019-04-05 00:12:35', '', '2019-04-05 07:12:35'),
(34, 'kumar', 'pradeep ', 'pradeep ', 'MTk4MDEzNQ==', 'm', 'tpradeep.aisf@gmail.com', 67, 67, '', 'pradee', '9946345615', '', 0, 'e8044bb42dffeb0ac3560ae3f38327a8fe2d7a07231f9045390b5e058b1dcc4772d86bd020e215ed7127a471cfb0710968a1686b5023abb2b16d00ef92bed57c', '', 'en', 'light', '2019-04-29 00:54:11', '', '2019-04-29 00:54:11', '', '2019-04-29 07:54:12'),
(35, 'Subinlal K', 'subinlalk1@gmail.com', 'subinlalk1@gmail.com', 'OTA3MjUwMzM5Mw==', 'm', 'subinlalk1@gmail.com', 22, 22, '', 'subinl', '9072503393', '', 0, '2e39fe076216f7c5dd5837db3db67f54a7987e407d03d1fd8718ad9b0b044f79f4d62794eb0d0cf191e593b61c20033a9710e2320f6b057146c34dcaa866befd', '', 'en', 'light', '2019-05-03 02:25:11', '', '2019-05-03 02:25:11', '', '2019-05-03 09:25:12'),
(36, 'testing name', 'test3', 'test3', 'dGVzdDM=', 'm', 'chindukrishn@gmail.com', 124, 124, '', 'test3', '9847540368', '', 0, 'cdabaf7f244386ef8a92e7fe1d3015cf559435f3055753f3f1696af3fe32eda43fe3f67790447c269ecaa57fb3c00579dda1284c8042087c9acd369ef8b5c581', '', 'en', 'light', '2019-05-08 20:52:53', '', '2019-05-08 20:52:53', '', '2019-05-09 03:52:54'),
(37, 'Redmi note 4', 'inometrics 2', 'inometrics 2', 'cGFzc3dvcmQ=', 'o', 'example@test.com', 132, 132, '', 'inomet', '8139073060', '', 0, '10a2159818606e3b1b28dd87c4c75ebff0908105b0cc259a245e5050c7d33f063a415a5580d2bd978d51c2f8175f6226a7064733883e05c7e10e72695c8c545c', '', 'en', 'light', '2019-05-08 22:33:01', '', '2019-05-08 22:33:01', '', '2019-05-09 05:33:01'),
(38, 'InoPot', 'InoPot', 'InoPot', 'ODEzOTA3MzA2MA==', 'o', 'inopotselfwatering@gmail.com', 132, 132, '', 'InoPot', '8139073060', '', 0, '61a29412c7b5908f49bf253908dfe2a77ebf70a0c258471ee5abe6c1776d96762402adf18b0a7d25639822f7db0feb4f4f56b24dfefcfa49aee642beac39f773', '', 'en', 'light', '2019-05-12 21:40:57', '', '2019-05-12 21:40:57', '', '2019-05-13 04:40:57'),
(39, 'Abhirami C', 'abhirami', 'abhirami', 'MTIzNDU=', 'f', 'abhiramicpani@gmail.com', 104, 104, '', 'public', '8921475913', 'Channaparambil', 0, 'e4cea37fc5db966a5e218d232525face674c2c71efe228c200bb30c9aa3a870aead8163f824be157e3d56f290f9b8c5e54651488df1e8ea59d49a62eeff030ad', '', 'en', 'dark', '2019-05-13 23:49:26', '', '2019-05-13 23:49:26', '', '2019-05-15 14:11:59'),
(40, 'Abhijith B', '9496418491', '9496418491', 'OTg5NTk3MDUyNA==', 'm', 'abhijithpyd@gmail.com', 132, 132, '', '949641', '9496418491', '', 0, 'afd4d69f02224a62d1bff2fd01e34c20610e2dea49e999a193a83ce1fad59b3660af8172ee8921b38a235ad7ee44197386385acce7ea3367482572ea6b40e38b', '', 'en', 'light', '2019-05-14 00:13:35', '', '2019-05-14 00:13:35', '', '2019-05-14 07:13:36'),
(41, 'Redmi Note 4', 'Redmi Note 4', 'Redmi Note 4', 'cGFzc3dvcmQ=', 'o', 'redmi@test.com', 132, 132, '', 'Redmi ', '8139073060', '', 0, 'a1c57a9bfa2c156ac7092c77b45f12696c161aca9551ce63608cb3efc50f248b2d2aa39b29572e0e44e44ee4cefd05b72faeaabf74829e73cb6dbc360d142cf6', '', 'en', 'light', '2019-05-14 04:21:17', '', '2019-05-14 04:21:17', '', '2019-05-14 11:21:18'),
(42, 'Redmi Varrier', 'Redmi', 'Redmi', 'ODEzOTA3MzA2MA==', 'f', 'redmivarier@email.com', 132, 132, '', 'public', '8139073060', 'mi bhavan', 0, 'bb19e3557a6c00a120eeea569a6e40987d295f49f13f6dc68928315a02d1d544451130e96d27f7c5fbf51b10941fce5741957fde9417e7a5ddfa88b181a55fda', '', 'en', 'light', '2019-05-14 22:47:45', '', '2019-05-14 22:47:45', '', '2019-05-22 13:02:01'),
(43, 's', 'syamtk', 'syamtk', 'MTcyOQ==', 'm', 's@gmail.com', 132, 132, '', 'syamtk', '9497360241', '', 0, 'a4d728780202b6913b9da9d2e0b12e205bc20410e5d9d63d9f73d8f954a7cee10977c70c72e8ad7deb919761cf473c05e28670e69984a17b1a57cca3e5d4e2b5', '', 'en', 'light', '2019-05-14 23:02:11', '', '2019-05-14 23:02:11', '', '2019-05-15 06:02:12'),
(44, 'krishna ', 'inometrics5', 'inometrics5', 'MTIzNDU2', 'm', 'krishna@inometrics.com', 128, 128, '', 'inomet', '9847540368', '', 0, '45781a2756ab40a202572bc8175c9d5f9c253112aea9d5c80651a93bee0da69fabbea5ece1c3bfdc275581b7cf60af57865b379445ee831d998bc2b87ee14717', '', 'en', 'light', '2019-05-17 05:10:03', '', '2019-05-17 05:10:03', '', '2019-05-17 12:10:03'),
(45, 'krishna ', 'krishna2', 'krishna2', 'MTIzNDU2', 'm', 'krishna@inometrics.com', 113, 113, '', 'krishn', '9847540368', '', 0, '257b9474ec947efed3629d35cd35e4377c5784d58196d0af880ec231ecdacff8032d6ffa0f2139163bb32ceb35dc77f41171ff2eeca1590affc50bdfa0dc38d6', '', 'en', 'light', '2019-05-17 05:49:11', '', '2019-05-17 05:49:11', '', '2019-05-17 12:49:11'),
(46, 'Abhirami C', 'Abhi', 'Abhi', 'MTIzNA==', 'f', 'abhiramicpani@gmail.com', 104, 104, '', 'Abhi', '8921475913', '', 0, '9de98f94f4f4df2fe1d0f8b935fbdda84bc9d6a2f3fc0fcf7acadaa60e3c1c5e50a3c82985580c3f2c33933d95ad66b4c49a2fa7816d6d4a54e42fdab3c9664f', '', 'en', 'light', '2019-05-19 21:56:04', '', '2019-05-19 21:56:04', '', '2019-05-22 06:14:42'),
(47, 'Anil ', 'anilkp', 'anilkp', 'a2dlZXRoYW0=', 'm', '', 71, 71, '', 'anilkp', '9446553289', '', 0, '3df7b42d37080c0458f1c2d4cbd4e3a44bd082eb572e7c6701424a7689d9736b8a6730522902ac016f0b60e9b7c3ba98b60c98dff5bedc1f103200aaf3f3430b', '', 'en', 'light', '2019-05-20 03:08:59', '', '2019-05-20 03:08:59', '', '2019-05-20 10:08:59'),
(48, 'bsnns', 'zam', 'zam', 'MTIzNDU=', 'm', '', 113, 113, '', 'zam', '1234567890', '', 0, 'f90c0be260c2f2c019628db4ba043d9015cd674251dce6c17422ed70be763e2f2366e7fddf4267250badde6e72c48cd010bc29c91dbb396268cc1dd970448ba3', '', 'en', 'light', '2019-05-20 05:48:28', '', '2019-05-20 05:48:28', '', '2019-05-21 12:27:03'),
(49, 'samii', 'sam', 'sam', 'MTIzNDU=', 'f', '', 136, 136, '', 'sam', '1234567890', '', 0, '4c31aaa28a7d138063854266f2b3b8c391b49532fc2280f84c0833226863e6a5652ba340f6290db3c30d080909981d7132bb47efbb34bc58ccd45131b667d7a4', '', 'en', 'light', '2019-05-20 06:07:00', '', '2019-05-20 06:07:00', '', '2019-05-20 13:07:00'),
(50, 'Niranjan Krishna', 'niranjankrishna', 'niranjankrishna', 'TmlyYW5qYW5AMTIz', 'm', 'niranjan_krishna@gmail.com', 76, 76, '', 'niranj', '8281266077', '', 0, '7be55a78c9116ae918d20e3c7aa5bef14b23e6500662d31076a8f0eabb340958d49b359e79e08e1a7941451dcf6d195cf261db13c514345dc32e13e96d21c75c', '', 'en', 'light', '2019-05-20 08:10:19', '', '2019-05-20 08:10:19', '', '2019-05-20 15:10:20'),
(51, '', 'iba', 'iba', 'NDMyMQ==', '', '', 113, 113, '', 'iba', '1234567890', '', 0, '3915e4da806482a45796b77633a6957bff43f3827c1ff0eb0169ed1c55d78afe8fa34c38f70cb1be5f1b8fddfcfdfed1718f4eba07529bfb2a4d764a9be2cb4d', '', '', '', '2019-05-21 21:35:14', '', '2019-05-21 21:35:14', '', '2019-05-22 04:35:14'),
(52, '', 'abz', 'abz', 'MzIx', '', '', 136, 136, '', 'abz', '9809809806', '', 0, '28bef5bbfe78977d9c04caa0c683e70c56d8e69a1dfbf61c5ce2a6e097768894c883402d2fdf50158794d942e9e5c054755a493c3f9a635bd5df741ee9275d69', '', '', 'light', '2019-05-21 21:43:53', '', '2019-05-21 21:43:53', '', '2019-05-22 04:43:53'),
(53, '', 'pip', 'pip', 'NDU2', '', '', 105, 105, '', 'pip', '5805809632', '', 0, 'e5d2c53e86b132444697cae35a3f38dc9067cacd97c79f45816d9b8c917f7440d1effdaa4a9879b5f6078b50d11425ca0be714c0d4dd7a0fb29a5c7c8c0235d5', '', '', 'light', '2019-05-21 21:50:58', '', '2019-05-21 21:50:58', '', '2019-05-22 04:50:59'),
(54, '', 'ab', 'ab', 'MDk4', '', '', 104, 104, '', 'ab', '8921475913', '', 0, 'c21a848d83bce2a93147cb8c613cc9a523798a2ce8d45714b29149a15d40bb7d57869c354b5c175228adf5b89b00a1fa9eb139e30b547d22a9b66b627033332f', '', '', 'light', '2019-05-21 22:44:45', '', '2019-05-21 22:44:45', '', '2019-05-22 05:44:46'),
(55, '', 'qas', 'qas', 'MTIz', '', '', 104, 104, '', 'qas', '8921475913', '', 0, 'b822a6fa4613e9bc49704cf8f4140d32c946b43f21015f392d7c949bfa145ad1f4521d9ed18bf5d2dd34291141bf9b0ce9ff4a71ecdbaf8b34b01f12ba72464c', '', '', 'light', '2019-05-21 22:59:14', '', '2019-05-21 22:59:14', '', '2019-05-22 05:59:14'),
(56, '', 'was', 'was', 'MTc3', '', '', 10, 10, '', 'was', '891475913', '', 0, 'b492278c5fa05ea460074891d9dc16821c68401a23cf3c2803d32ee96194cddb97ee3dd65b296371d477a627642b0c0e8c18d9e338966d3c379340a9398178c8', '', '', 'light', '2019-05-21 23:17:11', '', '2019-05-21 23:17:11', '', '2019-05-22 06:17:12'),
(57, '', 'dtdt', 'dtdt', '', '', '', 10, 10, '', 'dtdt', '', '', 0, '2d07e4e3bd92bdc423315e2c29845bc3a4e5f2fdc2e69805f6731926aa72269fef7c07caf80789135a36ffc4a4adb17d595574b28ed5c323aa7a42df4d99bc99', '', '', 'light', '2019-05-22 02:15:00', '', '2019-05-22 02:15:00', '', '2019-05-22 09:15:00'),
(58, '', 'dhfh', 'dhfh', '', '', '', 136, 136, '', 'dhfh', '', '', 0, '6a14d1c312f6cbd73c5a27110dfb64796ee27858c14370e74180ef8c4af4cdb48e3d7f9f2381ba6b73d3456e9236508111459978d817efb0c6a34bd072bbf8b8', '', '', 'light', '2019-05-22 02:17:53', '', '2019-05-22 02:17:53', '', '2019-05-22 09:17:54'),
(59, '', 'cufuv', 'cufuv', '', '', '', 128, 128, '', 'cufuv', '', '', 0, '9c62b01fea7922804db306dbed08cf308cf5cde3272cc6567d9e367b9214f2276e018ed10a132aa9a123c2bb4b87760e3d1c5eb10dcfe8663f28d20bdc63887c', '', '', 'light', '2019-05-22 02:23:23', '', '2019-05-22 02:23:23', '', '2019-05-22 09:23:27'),
(60, '', '6t6f', '6t6f', '', '', '', 113, 113, '', '6t6f', '', '', 0, '13663671d162373114a5f4a9231d7fca40aa9985cb3c8a4fdbc0384a8bc4d750bba6693a17ed0842b34098be7a4d086c9d3e8261aaab6dd5fb6090f42a678fab', '', '', 'light', '2019-05-22 02:30:05', '', '2019-05-22 02:30:05', '', '2019-05-22 09:30:06'),
(61, '', '7g8g8', '7g8g8', '', '', '', 136, 136, '', '7g8g8', '', '', 0, 'b0c785b0c67e2d993a6d604d1c771d41697c576024f418c2ff820fb417a5544a5eadc7a7b9e07421af8e0d2a69bb6a4405d0e0cd6f559523913d4ba018fcbc1a', '', '', 'light', '2019-05-22 02:31:34', '', '2019-05-22 02:31:34', '', '2019-05-22 09:31:35'),
(62, '', 'tvv', 'tvv', '', '', '', 136, 136, '', 'tvv', '', '', 0, '42dba618f119cf4e6fb039fce1a846e03e3f23af7005e3a207160b9aeb08133ca95ceea9b5f6d43a73b4ae38041bd6e5b8db6702d3845af475ea137a8ec12472', '', '', 'light', '2019-05-22 02:33:32', '', '2019-05-22 02:33:32', '', '2019-05-22 09:33:33'),
(63, '', 'gvug', 'gvug', '', '', '', 136, 136, '', 'gvug', '', '', 0, '198f03c712e741e263d38b850650c1ece4226c2a8f20206cce1f7ca90db4fb53a28c1c96845cb9f2d20e854ae6cfa9935147269cc49416f7e2c345a8588bc732', '', '', 'light', '2019-05-22 02:36:23', '', '2019-05-22 02:36:23', '', '2019-05-22 09:36:23'),
(64, '', 'hchv', 'hchv', '', '', '', 128, 128, '', 'hchv', '', '', 0, 'bcbe021a6f6678fd1cb582f9b891fe739874a3b1bfd22f50cc61191d8513ee7c4c5b9c24892d0f4f5e8bf48b6ef56bf1c7ab33d6b9a4f81f65fc6ed6e36d0957', '', '', 'light', '2019-05-22 02:41:36', '', '2019-05-22 02:41:36', '', '2019-05-22 09:41:36'),
(65, '', 'ydyydy', 'ydyydy', '', '', '', 102, 102, '', 'ydyydy', '', '', 0, '1f99470ffc1b31ebd57d43b56e0e130a775adae088bb955faf2bfb8db59ca5f643f6487fc016f0f00a85b5e357b6e25615b0f7ddbf81ba31f07faa2514850da5', '', '', 'light', '2019-05-22 02:54:30', '', '2019-05-22 02:54:30', '', '2019-05-22 09:54:32'),
(66, '', '', '', '', '', '', 75, 75, '', '', '6538', '', 0, 'b5629eb8d7485e101b99211de7803ef48f43f2cb7d42000a5a5898437d7c52524c3407c778ce5645808f1143d141027e8ecebc2dc45cc0b353a672f04cd18f5b', '', 'en', 'light', '2019-05-22 03:04:25', '', '2019-05-22 03:04:25', '', '2019-05-22 10:04:25'),
(67, '', 'vhhj', 'vhhj', '', '', '', 128, 128, '', 'vhhj', '', '', 0, '8fd711797131786c5901eaf0c87669feab5175885f1b30c67a1f98df741b48b41dc1121c3974d4382a1f6b1073f9de9bd7e3d6d2363ccc621d0c4e1cd0c508bc', '', '', 'light', '2019-05-22 03:06:48', '', '2019-05-22 03:06:48', '', '2019-05-22 10:06:49'),
(68, '', 'd68f6', 'd68f6', '', '', '', 102, 102, '', 'd68f6', '', '', 0, 'd940077db0d08292860d05644a863a498171feed3699bd4646b4ab9f8d95bb7baf0760014a0e712c94fa230483a1b040d0c0fa4a378ce3b5f952981852cc9ada', '', '', 'light', '2019-05-22 03:08:13', '', '2019-05-22 03:08:13', '', '2019-05-22 10:08:14'),
(69, '', '7r60f', '7r60f', '', '', '', 102, 102, '', '7r60f', '', '', 0, '62ef7ab59b55b6a0755cb867ea5c6845e98f366170982c2d8840ed300b1db4b81c10b45c8f9a491fd4078610c2054915e63e7f35ac5be32847a8f9427dabc9fb', '', '', 'light', '2019-05-22 03:14:13', '', '2019-05-22 03:14:13', '', '2019-05-22 10:14:13'),
(70, '', 'vy8y', 'vy8y', '', '', '', 102, 102, '', 'vy8y', '', '', 0, '85b9753b92503fee5933bb07ec2f5589548055d010bac03a4dc20e277c57535e18a20593ebcb9863dc930f6f0b7047b8816e523e1fcd31b3ab968cf49637fb71', '', '', 'light', '2019-05-22 04:21:36', '', '2019-05-22 04:21:36', '', '2019-05-22 11:21:37'),
(71, '', 'ftfy', 'ftfy', '', '', '', 75, 75, '', 'ftfy', '', '', 0, '82a2e4cccd90e82275cd9179d9fbca0abfa785fca54b2a877cc2176a40c556fbd54abc00dedaebdea990d36fbf382848be4c9d45dffe5c01aa442358e713b8db', '', '', 'light', '2019-05-22 04:27:04', '', '2019-05-22 04:27:04', '', '2019-05-22 11:27:04'),
(72, '', 't8cyg', 't8cyg', '', '', '', 136, 136, '', 't8cyg', '', '', 0, '0df12e6e6af24e40b754b7334340f022be185f3c6827bdaf36c75a824be604345b71d3705f9bcac2589642ecd88a60158b022da1b2fa035859d13d11c1ab3ab6', '', '', 'light', '2019-05-22 04:29:24', '', '2019-05-22 04:29:24', '', '2019-05-22 11:29:24'),
(73, '', 'kyn', 'kyn', '', '', '', 113, 113, '', 'kyn', '', '', 0, '994e204f6adde8c71a55574ebe1872657c4c74728cb9b65235566062173c22f1e92a1ea76b7db4f75de18c8ba1b70b8b9ff459500e45b90afc17b216df295492', '', '', 'light', '2019-05-22 04:36:13', '', '2019-05-22 04:36:13', '', '2019-05-22 11:36:13'),
(74, '', 'ycc', 'ycc', '', '', '', 10, 10, '', 'ycc', '', '', 0, '6eeb65ce1d88884c69bd8d4406c2be9089443d0a7ab00238bedca86b5c4d35a270826fe571550c5270e5387c83fc882c869e9576dfdeaca19bf04f1c7689c9bc', '', '', 'light', '2019-05-22 04:38:13', '', '2019-05-22 04:38:13', '', '2019-05-22 11:38:13'),
(75, '', 'ig gic', 'ig gic', '', '', '', 136, 136, '', 'ig gic', '', '', 0, '1453052e99ca338343bbed519eb45cde8550bd38d7aafbcd71ed082c9af90f0a16bd21a14bb9481a029e85a0551dbb1e7ca4ee58face290d98c0084be584323c', '', '', 'light', '2019-05-22 04:39:15', '', '2019-05-22 04:39:15', '', '2019-05-22 11:39:15'),
(76, '', 'gg x', 'gg x', '', '', '', 102, 102, '', 'gg x', '', '', 0, 'e2099ff0821dd784d1f960a96eef6f8a62cbfcb2420997d222ea0e0542a5aa39f6ef2ba11a7ac57902a823e3751d31666e7fc04fcd05d44602e28c56b7c1dda2', '', '', 'light', '2019-05-22 04:41:56', '', '2019-05-22 04:41:56', '', '2019-05-22 11:41:57'),
(77, '', 'f', 'f', '', '', '', 102, 102, '', 'f', '', '', 0, '62ac374b55615f492ee251f8321ce749a0e00f796e2f987764342947cadd8f17a6d4de49dfb24e24ca137a230379e5e4fd6d1d4286b23c83fe5de07dbb22d242', '', '', '', '2019-05-22 04:45:21', '', '2019-05-22 04:45:21', '', '2019-05-22 11:45:22'),
(78, '', 'gvgh', 'gvgh', '', '', '', 136, 136, '', 'gvgh', '', '', 0, 'fac9fa22221f5d45cf08f6dff2e06cfacec4e1de44a829b7a41e3444697b9517c3f7c0d07f4494d4ce4cd994042155ba567b648ac0e5dd19041665dd81950db9', '', '', '', '2019-05-22 04:57:32', '', '2019-05-22 04:57:32', '', '2019-05-22 11:57:32'),
(79, '', 'hyvbyb', 'hyvbyb', '', '', '', 128, 128, '', 'hyvbyb', '', '', 0, 'ba6dbfa07fc9feea960063e1f02d78f526edcb77e8524f0ad41037b560f1c4900e5a81c8e5d6f91933acb3faa29054187c523169bb7af7540b8bd304fd5a7d72', '', '', '', '2019-05-22 04:59:25', '', '2019-05-22 04:59:25', '', '2019-05-22 11:59:25'),
(80, '', 'te hrh', 'te hrh', '', '', '', 136, 136, '', 'te hrh', '', '', 0, 'a211033a9d3847c29a6eb9bca9ea8ab851907e9cf78002c9ef4f5f2ab663e3c663ebbfaee1f7db1727f9930c59847ea6340b3914d78c9b03309b43d870f9067d', '', '', '', '2019-05-22 05:05:54', '', '2019-05-22 05:05:54', '', '2019-05-22 12:05:55'),
(81, '', 'hccjv', 'hccjv', '', '', '', 136, 136, '', 'hccjv', '', '', 0, '5ffd3b21abed8e3727cd6eb72616c467f8a30ab6ae99d525c5de40b4dd9e14d9ec603407920649c6325fa6c7edaac3fe5d688d3f0e18a31d27bcc20c2a731a64', '', '', '', '2019-05-22 05:37:12', '', '2019-05-22 05:37:12', '', '2019-05-22 12:37:13'),
(82, '', 'r5dy', 'r5dy', '', '', '', 136, 136, '', 'r5dy', '', '', 0, 'ed4536cd23fec35ec37fe0a7d25dbbf61f8694d6f613ff8fb3d4fd7f13e61714aeea1795e6ae35f8c0e5a9d5e66a3e7ecccede54d57c3e3da34a91c3027e6a1f', '', '', '', '2019-05-22 06:21:42', '', '2019-05-22 06:21:42', '', '2019-05-22 13:21:43'),
(83, '', 'hhj', 'hhj', '', '', '', 136, 136, '', 'hhj', '', '', 0, 'daf5c44bfa6bfa5bca51325e6af15358762160bb6072c58baa37249d64cadfdc0dac40229acd3fca9dfffa387761785b55945525934a910fae749634996c3473', '', '', '', '2019-05-22 21:32:01', '', '2019-05-22 21:32:01', '', '2019-05-23 04:32:01');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL,
  `org` varchar(200) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `ad_image` varchar(300) DEFAULT NULL,
  `description` text,
  `banner_image` varchar(300) DEFAULT NULL,
  `constituency_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `org`, `name`, `phone`, `email`, `address`, `website`, `ad_image`, `description`, `banner_image`, `constituency_id`) VALUES
(1, 'InoPot', 'Smart Pots', 2147483647, 'info@inopot.com', '4th Floor, Nila Buildings\r\nTechnopark', 'www.inopot.com', '10l_colors_logo24.jpg', 'A wonderful time saving and water saving solution has been met out. Time-saving in the sense, the frequency of watering is reduced to merely 7% and water saving of almost 95%. This helps us in saving money indirectly, for our time is used more fruitfully. Think of a scenario that the family needs to go on a vacation and helpers cannot be permitted indoors ! Herein comes the relevance of INOPOT ! We could leave our plants in INOPOT unattended for even as long as two weeks !  This gift of freedom leaves us with a lot of relief because our plants are no more a liability to us. We could give more attention to other prioritised stuff and happily relax.', '10l_colors_LOGO.jpg', 133);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state`, `country_id`) VALUES
(1, 'Kerala', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `UserCode` varchar(20) NOT NULL,
  `Password` varchar(250) NOT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Role` varchar(6) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '0',
  `CreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(20) NOT NULL,
  `AmentedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AmentedBy` varchar(20) NOT NULL,
  `LastLogin` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`Id`, `Name`, `UserCode`, `Password`, `Image`, `Role`, `Status`, `CreatedAt`, `CreatedBy`, `AmentedAt`, `AmentedBy`, `LastLogin`) VALUES
(1, '', 'admin', '1234', 'img.jpg', 'Admin', 0, '2018-10-01 00:00:00', 'admin', '2018-10-01 00:00:00', 'admin', '2018-11-30 07:30:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category` (`category`);

--
-- Indexes for table `category_icons`
--
ALTER TABLE `category_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constituency`
--
ALTER TABLE `constituency`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `constiuency` (`constiuency`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_image`
--
ALTER TABLE `news_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_log`
--
ALTER TABLE `news_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `public_user`
--
ALTER TABLE `public_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state` (`state`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `category_icons`
--
ALTER TABLE `category_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=987;

--
-- AUTO_INCREMENT for table `constituency`
--
ALTER TABLE `constituency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `news_image`
--
ALTER TABLE `news_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_log`
--
ALTER TABLE `news_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `public_user`
--
ALTER TABLE `public_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
