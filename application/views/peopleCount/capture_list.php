<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<style>
.td-padding {
	vertical-align: middle!important;
}
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-offset-4 col-md-12">



											<div class="card-block px-0 py-3">
                                            <div class="table-responsive">

																	<table class="table table-hover">
																			<tbody>
																			<?php foreach($list_data as $ldata) { ?>
																					<tr class="unread">

																							<td width="40%"><img class="" style="width:200px;" src="<?= $ldata->image!=''?base_url('assets/ftp_people_count/'.$ldata->image):base_url('assets/images/user/people.jpg') ?>" alt="activity-user"></td>
																							<td class="td-padding">
																									<h6 class="mb-1"><b> Device </b><?=$ldata->device_id ?></h6>
																									<p class="m-0"><b>People Count</b> <?=$ldata->people_count ?></p>
																									<p class="m-0"><b>Co2</b> <?=$ldata->co2 ?></p>
																									<p class="m-0"><b>Humidity </b><?=$ldata->humidity ?></p>
																									<p class="m-0"><b>Temperature</b> <?=$ldata->temperature ?></p>
																							</td>
																							<td  class="td-padding">
																									<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i><?= $ldata->created_time ?></h6>
																							</td>
																							<td  class="td-padding"><a data-toggle="modal" data-target="#<?= $ldata->id ?>" href="#<?= $ldata->id ?>" class="label theme-bg2 text-white f-12" style="border-radius:10px">view</a></td>
																					</tr>
																					<div class="modal fade" id="<?= $ldata->id ?>" role="dialog">
																						<div class="modal-dialog">

																							<!-- Modal content-->
																							<div class="modal-content">
																								<div class="modal-header">
																									<button type="button" class="close" data-dismiss="modal">&times;</button>
																								</div>
																								<div class="modal-body">
																									<p>Some text in the modal.</p>
																									<img class="" style="width:100%;" src="<?= $ldata->image!=''?base_url('assets/ftp_people_count/'.$ldata->image):base_url('assets/images/user/people.jpg') ?>" alt="activity-user">
																								</div>
																							</div>

																						</div>
																					</div>
																					<?php } ?>
																					<!-- <tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-2.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Mathilde Andersen</h6>
																									<p class="m-0">Lorem Ipsum is simply text of…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-red f-10 m-r-15"></i>11 MAY 10:35</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-3.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Karla Sorensen</h6>
																									<p class="m-0">Lorem Ipsum is simply…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>9 MAY 17:38</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-1.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Ida Jorgensen</h6>
																									<p class="m-0">Lorem Ipsum is simply text of…</p>
																							</td>
																							<td>
																									<h6 class="text-muted f-w-300"><i class="fas fa-circle text-c-red f-10 m-r-15"></i>19 MAY 12:56</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-2.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Albert Andersen</h6>
																									<p class="m-0">Lorem Ipsum is simply dummy…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>21 July 12:56</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr> -->
																			</tbody>
																	</table>
															</div>
													</div>




													<button  class="btn btn-primary" id="button">Next</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
  </div>
	<!-- Modal -->

	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
