<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<style>
.add_btn {
	width:90px;
}
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $title ?></h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href=""><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5><?= $title.' - '.$action ?></h5>
																							<a  class="label theme-bg text-white f-12 add_btn" href="<?= base_url($controller.'/add_camera') ?>"><i class="feather icon-edit"></i>Add New</a>

                                        </div>

                                        <!-- [ stiped-table ] start -->
                                        <div class="card-block table-border-style">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Camera Name</th>
                                                            <th>Brand</th>
                                                            <th>Protocol</th>
                                                            <th>Location</th>
                                                            <th>Location Type</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php if($list_data!=null) {
														$no = 1;
													foreach($list_data as $list) {?>
														<tr>
															<td><?= $no?></td>
															<td><?= $list->name ?></td>
															<td><?= $list->brand ?></td>
															<td><?= $list->protocol ?></td>
															<td><?= $list->location ?></td>
															<td><?= $list->location_type ?></td>
															<td><a  class="label theme-bg text-white f-12 save_multi_input hidden_input" href="<?=base_url($controller.'/add_camera/'.$list->id)?>"><i class="feather icon-edit"></i>Edit</a><a href="<?= base_url($controller.'/delete_camera/'.$list->id) ?>" class="label theme-bg2 text-white f-12 hidden_input" onclick="return confirm('Delete this record?')"><i class="feather icon-x"></i></a></td>
														</tr>
													<?php }} ?>
													</tbody>
                                                </table>
                                            </div>
                                        </div>

                                <!-- [ stiped-table ] end -->
                                    </div>
                                </div>
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
