<body>

    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a  class="b-brand">
                    <div class="b-bg">
                        <i class="feather icon-users"></i>
                    </div>
                    <span class="b-title">Crowd Manager</span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="javascript:"><span></span></a>
            </div>


            <div class="navbar-content scroll-div">
                <ul class="nav pcoded-inner-navbar">
                    <!-- <li class="nav-item pcoded-menu-caption">
                        <label>People Count</label>
                    </li>
                    <li class="nav-item </?=  $active=='inout_dash'?'active':'' ?>">
                        <a href="</?= base_url('InOutHome');?>" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-home"></i></span>
												<span class="pcoded-mtext">Dashboard</span>
											</a>
										</li>
                    <li class="nav-item pcoded-hasmenu </?= ($active=='inout_cam_add'|| $active=='inout_cam')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-camera"></i></span>
												<span class="pcoded-mtext">Camera</span>
											</a>
											<ul class="pcoded-submenu" style ="</?= ($active=='inout_cam_add'||$active=='inout_cam')?'display:block':'' ?>">
												<li class="<?= $active=='inout_cam_add'?'active':'' ?>"><a href="<?= base_url('/InOutHome/add_camera');?>" class="">Camera Add</a></li>
												<li class="<?= $active=='inout_cam'?'active':'' ?>"><a href="<?= base_url('/InOutHome/camera');?>" class="">Camera List</a></li>
											</ul>
                    </li>
										<li class="nav-item pcoded-hasmenu </?= ($active=='inout_comm')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-box"></i></span>
												<span class="pcoded-mtext">Components</span>
											</a>
											<ul class="pcoded-submenu" style="</?= ($active=='inout_comm')?'display:block':'' ?>">
												<li class="</?= $active=='inout_comm'?'active':'' ?>"><a href="</?= base_url('/InOutHome/common_settings');?>" class="">Common Settings</a></li>
											</ul>
                    </li> -->
                    <li class="nav-item pcoded-menu-caption">
                        <label>Crowd Manager</label>
                    </li>
										<li class="nav-item <?=  $active=='peopleCount_dash'?'active':'' ?>">
                        <a href="<?= base_url('PeopleCountHome');?>" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-home"></i></span>
												<span class="pcoded-mtext">Dashboard</span>
											</a>
										</li>
                    <li class="nav-item pcoded-hasmenu <?= ($active=='peopleCount_cam_add'|| $active=='peopleCount_cam')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-camera"></i></span>
												<span class="pcoded-mtext">Camera</span>
											</a>
											<ul class="pcoded-submenu" style ="<?= ($active=='peopleCount_cam_add'||$active=='peopleCount_cam')?'display:block':'' ?>">
												<li class="<?= $active=='peopleCount_cam_add'?'active':'' ?>"><a href="<?= base_url('/PeopleCountHome/add_camera');?>" class="">Camera Add</a></li>
												<li class="<?= $active=='peopleCount_cam'?'active':'' ?>"><a href="<?= base_url('/PeopleCountHome/camera');?>" class="">Camera List</a></li>
											</ul>
                    </li>
										<li class="nav-item pcoded-hasmenu <?= ($active=='peopleCount_comm')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-box"></i></span>
												<span class="pcoded-mtext">Components</span>
											</a>
											<ul class="pcoded-submenu" style="<?= ($active=='peopleCount_comm')?'display:block':'' ?>">
												<li class="<?= $active=='peopleCount_comm'?'active':'' ?>"><a href="<?= base_url('/PeopleCountHome/common_settings');?>" class="">Common Settings</a></li>
											</ul>
					</li>
					<li class="nav-item pcoded-hasmenu <?= ($active=='people_count_report')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-box"></i></span>
												<span class="pcoded-mtext">Report</span>
											</a>
											<ul class="pcoded-submenu" style="<?= ($active=='people_count_report')?'display:block':'' ?>">
												<li class="<?= $active=='people_count_report'?'active':'' ?>"><a href="<?= base_url('/PeopleCountHome/select_people_count');?>" class="">People Count Report</a></li>
											</ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->

    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="javascript:"><span></span></a>
            <a href="index.html" class="b-brand">
                   <div class="b-bg">
                       <i class="feather icon-trending-up"></i>
                   </div>
                   <span class="b-title">Datta Able</span>
               </a>
        </div>
        <a class="mobile-menu" id="mobile-header" href="javascript:">
            <i class="feather icon-more-horizontal"></i>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li><a href="javascript:" class="full-screen" onclick="javascript:toggleFullScreen()"><i class="feather icon-maximize"></i></a></li>
                <!-- <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown">Dropdown</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="javascript:">Action</a></li>
                        <li><a class="dropdown-item" href="javascript:">Another action</a></li>
                        <li><a class="dropdown-item" href="javascript:">Something else here</a></li>
                    </ul>
                </li> -->
                <!-- <li class="nav-item">
                    <div class="main-search">
                        <div class="input-group">
                            <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                            <a href="javascript:" class="input-group-append search-close">
                                <i class="feather icon-x input-group-text"></i>
                            </a>
                            <span class="input-group-append search-btn btn btn-primary">
                                <i class="feather icon-search input-group-text"></i>
                            </span>
                        </div>
                    </div>
                </li> -->
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- <li>
                    <div class="dropdown">
                        <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                        <div class="dropdown-menu dropdown-menu-right notification">
                            <div class="noti-head">
                                <h6 class="d-inline-block m-b-0">Notifications</h6>
                                <div class="float-right">
                                    <a href="javascript:" class="m-r-10">mark as read</a>
                                    <a href="javascript:">clear all</a>
                                </div>
                            </div>
                            <ul class="noti-body">
                                <li class="n-title">
                                    <p class="m-b-0">NEW</p>
                                </li>
                                <li class="notification">
                                    <div class="media">
                                        <img class="img-radius" src="<?= base_url('assets/images/user/avatar-1.jpg')?>" alt="Generic placeholder image">
                                        <div class="media-body">
                                            <p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                            <p>New ticket Added</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="n-title">
                                    <p class="m-b-0">EARLIER</p>
                                </li>
                                <li class="notification">
                                    <div class="media">
                                        <img class="img-radius" src="<?= base_url('assets/images/user/avatar-2.jpg')?>" alt="Generic placeholder image">
                                        <div class="media-body">
                                            <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                            <p>Prchace New Theme and make payment</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="notification">
                                    <div class="media">
                                        <img class="img-radius" src="<?= base_url('assets/images/user/avatar-3.jpg')?>" alt="Generic placeholder image">
                                        <div class="media-body">
                                            <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                            <p>currently login</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="noti-footer">
                                <a href="javascript:">show all</a>
                            </div>
                        </div>
                    </div>
                </li> -->
                <li>
                    <div class="dropdown drp-user">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?= base_url('assets/images/user/avatar-1.jpg" class="img-radius" alt="User-Profile-Image')?>">
                                <span><?= $this->session->userdata['logged_in']['UserCode'] ?></span>
                                <a href="<?= base_url('login/logout'); ?>" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>
                            </div>
                            <ul class="pro-body">
                                <!-- <li><a href="javascript:" class="dropdown-item"><i class="feather icon-settings"></i> Settings</a></li>
                                <li><a href="javascript:" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                                <li><a href="message.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li> -->
                                <!-- <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Log Out</a></li> -->
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <!-- [ Header ] end -->
