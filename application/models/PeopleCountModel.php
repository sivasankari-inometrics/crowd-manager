<?php
class PeopleCountModel extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
		$this->device = $this->db->get_where('people_count_settings',array('key_field'=>'default_device'))->row()->key_value;
		$this->today = date('Y-m-d');
	}
  public function menu() {
		return $this->db->get_where('menu',array());
	}


	//********************************************** dashboard module****************************//
	public function get_default_device() {

		return $this->db->get_where('people_count_lastreading',array('device_id'=>$this->device))->row();
	}

	public function get_today_data() {

		$result = $this->db->query("SELECT count(*) as count,max(temperature) as max_temp FROM people_count_readings WHERE date(created_time)='$this->today'")->result();
		$data['capture_count'] = $result[0]->count;
		$data['max_temp'] = $result[0]->max_temp;
		return $data;
	}
	public function get_total_device() {
		$result = $this->db->query("SELECT count(DISTINCT name) as camera_count,count(DISTINCT location) as location_count FROM people_count_device")->result();
		return $result[0];
	}

	public function get_actual_data() {
		return $this->db->get_where('people_count_readings',array('device_id'=>$this->device,'date(created_time)'=>$this->today))->result();
	}
	public function get_threshold_limit() {
		return $this->db->get_where('people_count_settings',array('key_field'=>'threshold_count'))->row()->key_value;

	}
	public function get_threshold_data($limit) {
		$th_limit = $this->get_threshold_limit();
		return $this->db->query("select * from people_count_readings where device_id='$this->device' and date(created_time)='$this->today' and people_count>= $th_limit order by id desc limit $limit ")->result();
	}
	/**
	 * capture list
	 */
	public function capture_list($limit=null) {
		if($limit==null) {
			$limit = '10';
		}
		return $this->db->order_by('id', 'desc')->get_where('people_count_readings',array('device_id'=>$this->device),$limit)->result();
	}
	//********************************************** dashboard module****************************//

	//***********************common settings*************************************************//
	/**
	 * select all data from  people_count_settings
	 */
	public function select_key_data () {
		$data = $this->db->get_where('people_count_settings',array())->result();
		if(count($data)>0) {
			return $data;
		}
	}

	/**
	 * save common settings value
	 * @param keyfield,keyvalue
	 */
	public function save_common_settings($post_data) {

		$data = [
			'key_value'=> $post_data['key_value']
		];
		$this->db->where('key_field',$post_data['key_field']);
		$this->db->update('people_count_settings',$data);
	}
	//*************************************camera module********************************** */

	/**
	 * list for all people_count_settings table fields
	 * table - people_count_settings
	 * @param key_field
	 * @return key_value
	 */
	public function get_settings($field) {
		$data = $this->db->get_where('people_count_settings',array())->result();
		if(count($data)>0) {
			foreach($data as $dt) {
				if($field==$dt->key_field) {
					return $dt->key_value;
				}
			}
		}
	}

	/**
	 * save camera
	 * table - people_count_device
	 */
	public function save_camera($post_data) {
		if($post_data["id"]!=null) {
			$this->db->where('id',$post_data["id"]);
			$query = $this->db->update('people_count_device', $post_data);
		}else {
			$query = $this->db->insert('people_count_device', $post_data);
		}
    return $query ? true : false;
	}
	/**
	 * select camera_data
	 */
	public function select_camera($id=null) {
		if($id!=null) {
			$data = $this->db->get_where('people_count_device',array('id'=>$id))->row();
		}else {
			$data = $this->db->get_where('people_count_device',array())->result();
		}
		if($data!=null) {
				return $data;
		}
	}

	/**
	 * delete camera
	 */
	public function delete_camera($id) {

		$this->db->where('id',$id);
		$this->db->delete('people_count_device');
	}

	/************************report module************************************************ */

	public function 	people_count_report($data) {
		$todate = date('Y-m-d',strtotime($data['to_date']));
		$fromdate = date('Y-m-d',strtotime($data['from_date']));
			$data = $this->db->query("select * from people_count_readings where date(created_time) between '$fromdate' and '$todate'")->result();
				return $data;

	}
}
