<?php
class Api_model extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
	}
	public function people_count_settings($keyfield) {
		return $this->default_device = $this->db->get_where('people_count_settings',array('key_field'=>$keyfield))->row()->key_value;
	}
  public function get_device_data($data) {

		$datas = $data;
		$data['created_time']  = $datas['updated_time'] = date('Y-m-d H:i:s');
		$device_id = $datas['device_id'];
		$this->db->where('device_id',$device_id);
		$this->db->update('people_count_lastreading',$datas);
		$this->db->insert('people_count_readings',$data);
	}

	public function set_device_data($data) {

		if($data['flag']=='1') {
			$this->db->where('key_field','default_device');
			$this->db->update('people_count_settings',array('key_value'=> $data['device_id']));
		}
		$rtn['time_intreval'] = $this->people_count_settings('default_interval');
		$rtn['threshold_value'] = $this->people_count_settings('threshold_count');
		return $rtn;
	}
}
