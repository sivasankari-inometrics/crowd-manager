<?php
class ApiInOut_model extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
	}
	// public function people_count_settings($keyfield) {
	// 	return $this->default_device = $this->db->get_where('inout_settings',array('key_field'=>$keyfield))->row()->key_value;
	// }
  public function get_device_data($data) {

		$today = date('Y-m-d H:i:s');
		$device_id = $data['device_id'];
		$data['created_time'] = date('Y-m-d H:i:s');
		// $this->db->where('device_id',$device_id);
		// $this->db->update('people_count_lastreading',$data);
		$this->db->insert('inout_readings',$data);
	}

	public function set_device_data($data) {

		if($data['flag']=='1') {
			$this->db->where('key_field','default_device');
			$query = $this->db->update('inout_settings',array('key_value'=> $data['device_id']));

			if($query==true) {
				return $query;
			}else{
				return true;
			}
		}
		return true;
	}
}
